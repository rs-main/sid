<?php

use App\Http\Controllers\StudentController;
use App\Models\PrintJob;
use App\Models\Student;
use GraphQL\Exception\QueryError;
use GraphQL\QueryBuilder\QueryBuilder;
use GraphQL\RawObject;

Route::get("test-end-point",[StudentController::class,"testEndPoint"]);

Route::get("/test",function (){

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,
        'https://maps.googleapis.com/maps/api/geocode/json?address=Achimota%20School&key=AIzaSyAXI-1JdxHZTy3nb700Mpp1X21pzqAmU_c');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $json = curl_exec($ch);
    curl_close($ch);
//
     $apiResult = json_decode($json, true);

     return ["data" => $apiResult["results"][0]["formatted_address"], "coordinates" => $apiResult["results"][0]["geometry"]["location"]];
//    print_r($apiResult);
});


Route::get("graph-test",function (){

    $endPoint = "https://staging.api.desafrica.com/v1";
    $client = new \GraphQL\Client(
        $endPoint,
        []  // Replace with array of extra headers to be sent with request for auth or other purposes
    );

    $builder = (new QueryBuilder('getPrintJobs'))
        ->selectField(
            (new QueryBuilder('jobs'))
                ->selectField('schoolCode')
                ->selectField('schoolName')
                ->selectField('submitDate')
                ->selectField((new QueryBuilder("pendingJobs"))
                ->selectField("imei"))
                ->selectField((new QueryBuilder("completedJobs"))
                    ->selectField("imei"))
        );
    // Run query to get results
    try {

        $results = $client->runQuery($builder, true, []);
    }
    catch (QueryError $exception) {

        // Catch query error and desplay error details
        print_r($exception->getErrorDetails());
        exit;
    }

    // Display original response from endpoint
    return json_decode($results->getResponseBody(),true);

//    return $jobs = json_decode($data, true);
   return  $data =  $jobs["data"]["getPrintJobs"]["jobs"];
    PrintJob::insertData($data);
});

Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});

Route::get('/test-event', function () {
//    event(new \App\Events\MyEvent("hello world"));
//    dd('Event Run Successfully.');

    $options = array(
        'cluster' => env('PUSHER_APP_CLUSTER'),
        'encrypted' => true
    );

    $pusher = new Pusher\Pusher(
        env('PUSHER_APP_KEY'),
        env('PUSHER_APP_SECRET'),
        env('PUSHER_APP_ID'),
        $options
    );

    $data['message'] = 'Hello XpertPhp';
    $pusher->trigger('submission-channel', 'App\\Events\\NewSubmissionEvent', $data);

});

Route::get('/echo', function () {
//    return view("echo");
    return view("pusher");
});

Route::get("/capture-student-card/{card_id}",function ($card_id){
    return \App\Models\Student::captureStudentCard($card_id);
});

Route::get("/test-case",function (){
//   $v = \App\Models\School::select("status")->selectRaw("(CASE WHEN (status = 'target') THEN 'target' END) as target")->get();
//   return $v;

//    $count = Student::whereSchoolId(18)->selectRaw("(CASE WHEN (new = 1) THEN new END) as new")->get();

//    return $count;

    return Student::whereHas("school.projectOfficer")->get();
});

