<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\APIController;
use App\Http\Controllers\CardGeneratorController;
use App\Http\Controllers\DataBankController;
use App\Http\Controllers\ManagementController;
use App\Http\Controllers\PermissionsController;
use App\Http\Controllers\PrintingController;
use App\Http\Controllers\PrintJobController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StudentController;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Spatie\Browsershot\Browsershot;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


//Route::get('/all-jobs',[PrintJobController::class,"getPrintJobs"])->name("jobs.data");
Route::get('/all-jobs',[PrintingController::class,"getPrintJobs"])->name("jobs.data");
Route::get('/print-presses',[PrintingController::class,"printPresses"])->name("print.presses");
Route::get('/card-jobs',[CardGeneratorController::class,"getCardGeneratorJobs"])->name("card-jobs.data");
Route::get('/generate-cards/{imei}/{school}',[CardGeneratorController::class,"downloadJob"]);
Route::post('/generate-cards/{imei}/{school}',[CardGeneratorController::class,"generateCards"]);

Route::get("/projects",[ManagementController::class,"projects"]);
Route::get("/project-officers",[ManagementController::class,"projectOfficers"]);
Route::get('/all-projects',[ManagementController::class,"allProjects"])->name("projects");

Route::get('/students',[DataBankController::class,"allStudents"])->name("students.data");




//Route::group(["middleware"=> "auth"],function (){
    Route::get('/all-permissions',[PermissionsController::class,"getAllPermissions"]);
    Route::get('/all-roles',[RoleController::class,"getAllRoles"]);



    Route::get('/all-schools',[APIController::class,"allSchools"])->name("schools.data");
    Route::get('/jobs/{school}/{imei}',[StudentController::class,"index"])->name("job-details.data");
    Route::get('/job/{imei}/{school_code}',[PrintJobController::class,"getJobDetails"])->name("single-job.data");



    Route::post('/download-job/{imei}/{school}',[AdminController::class,"downloadJob"]);


    Route::get("/sync-jobs",   [PrintJobController::class, "syncPrintJobs"]);

    Route::post("/submit-student",[APIController::class,"submitStudent"]);

    Route::post("/setup-school",[APIController::class,"setUpSchool"]);

    Route::post("/login",[APIController::class,"projectOfficerLogin"]);


    Route::post("/assign-project",[APIController::class,"assignProject"]);

    Route::post("/assign-project",[APIController::class,"assignProject"]);

    Route::get("/students-registered/{school_code}/{total_number}",[APIController::class,"getSchoolStudentsRegistered"]);

    Route::get("/students-registered",[APIController::class,"getStudentsRegistered"]);

    Route::get("/students-by-code/{school_code}",[APIController::class,"generateStudentId"]);

    Route::post("/update-student/{id}",[APIController::class,"updateStudent"]);

    Route::get("/image/{card_id}",function ($card_id){
        if (file_exists(public_path()."/$card_id.png")){
            $file_pointer = public_path()."/$card_id.png";
            unlink($file_pointer);
        }

        Browsershot::url("https://sid.desafrica.com/card/$card_id")
            ->windowSize(1050, 660)
            ->save("$card_id.png");

        return response()->json(["data" => "$card_id.png"]);
    });

Route::get("/capture-student-card/{card_id}",function ($card_id){
     Student::captureStudentCard($card_id);
});

Route::get("/download-card-image/{card_id}",function ($card_id){
    return Response::download(public_path()."/$card_id.png");
});

Route::get("/project-officers/{school_code}",[APIController::class, "projectOfficers"]);

