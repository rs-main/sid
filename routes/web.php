<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\CardGeneratorController;
use App\Http\Controllers\ControlCenterController;
use App\Http\Controllers\DataBankController;
use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\ManagementController;
use App\Http\Controllers\PrintingController;
use App\Http\Controllers\PrintJobController;
use App\Http\Controllers\StudentController;
use App\Models\Student;
use Illuminate\Support\Facades\Route;
use Spatie\Browsershot\Browsershot;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::group(['middleware' => ['auth','check_permission']], function () {

//    Route::get('/', [AdminController::class, 'getIndex'])
//        ->name('control-center');

    Route::get('/', [ControlCenterController::class, 'getIndex'])
        ->name('control-center');

    Route::get('/printing', [PrintingController::class, 'getPrinting'])
        ->name('printing');

    Route::get('/job-details/{schoool}/{imei}', [PrintingController::class, 'getPrintDetails'])
        ->name('job-details');

    Route::get('/card-generator', [CardGeneratorController::class, 'getCardGenerator'])
        ->name('card-generator');

    Route::get('/management', [ManagementController::class, 'getManagement'])
        ->name('management');

    Route::post('/api/add-new-project',[ManagementController::class,"postAddNewProject"]);

    Route::post('/api/add-new-project-officer',[ManagementController::class,"postAddNewProjectOfficer"]);

    Route::get('/databank', [DataBankController::class, 'getDataBank'])->name('databank');

    Route::get('/users', [AccountController::class, 'getUsers'])->name('users');
    Route::post('/api/add-new-account',[AccountController::class,"postAddNewAccount"]);
    Route::get('/api/users-data',[AccountController::class,"getAllUsersData"])->name("users.data");
    Route::post('/api/update-user-status/{id}',[AccountController::class,"postChangeUserStatus"]);
    Route::post('/api/update-user/{id}',[AccountController::class,"update"]);
    Route::post('/api/remove-user/{id}',[AccountController::class,"removeUser"]);
    Route::get("/api/activity-log/{id}",[AccountController::class,"getUserLogs"]);

    Route::get('/deliveries', [DeliveryController::class, 'getDeliveries'])->name('deliveries');

    Route::get("/user",function(){
       return \Illuminate\Support\Facades\Auth::id();
    });


});


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
        return view('dashboard');
})->name('dashboard');

Route::get("card/{card_id}",[PrintJobController::class,"getCard"]);

Route::get("download-job/{imei}/{school_id}",[AdminController::class,"downloadJob"]);

Route::get("/image/{card_id}",function ($card_id){
    if (file_exists(public_path()."/$card_id.png")){
        $file_pointer = public_path()."/$card_id.png";
        unlink($file_pointer);
    }

    Browsershot::url("https://sid.desafrica.com/card/$card_id")
        ->windowSize(1040, 700)
        ->save("$card_id.png");

    return response()->json(["data" => "$card_id.png"]);
});

Route::post("/process-image-zips/{school}/{imei}",function ($school,$imei){

    $name_of_zipped = $imei . "_" . $school . ".zip";

    if (file_exists(public_path()."/$name_of_zipped")){
        $file_pointer = public_path()."/$name_of_zipped";
        unlink($file_pointer);
    }
    $request = request();
    $all_card_ids = explode(",",$request->get("card_ids"));

    $zip = new \ZipArchive();
    if ($zip->open(public_path()."/$name_of_zipped", \ZipArchive::CREATE) === TRUE) {
        foreach ($all_card_ids as $card_id) {
            $zip->addFile("$card_id.png");
        }

        $zip->close();
    }

    return response()->json(["data" => "$name_of_zipped", "ids"=>$all_card_ids]);
});


@include 'test_routes.php';
