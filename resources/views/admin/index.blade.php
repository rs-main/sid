

@extends("layouts.master")

@section("scripts")


    <script src="{{asset("js/websocket.js")}}"></script>
    <script src="{{asset("js/google_maps.js")}}"></script>
    <script src="{{asset("js/control_center.js")}}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLxvQxzcnFc8CH2rFmQWthopHhF1xHsYc&callback=initMap&libraries=&v=weekly&region=GH" async>
    </script>

    <script>
       function geoLocateSchoolOnMap( schoolInfo ) {

            if($('#geolocateSwitchControlCenter').prop('checked') == true) {
                google.maps.event.trigger(window.myMapMarkers[schoolInfo.school_code], 'click');
            }
        }

        let schoolTable;
        controlCenterObj.getSchools()
            .then(data => {
                console.log("schools " + data, "Rest Schools")
                let school_counts = @json($schools_collection);
                schoolTable = $('#dataTable-schools').DataTable(
                    {
                        initComplete: function() {
                            $('[data-toggle="officer-name-tooltip"]').tooltip();
                            var datatable_length_text = $('.dataTables_length label').contents().filter(function() {
                                if(this.nodeType === 3) {
                                    if(this.data == ' entries') {
                                        return this.nodeType === 3;
                                    }
                                }
                            }).wrap('<span style="display:none"></style>');

                            var toggleGeolocation = `<div class="custom-control custom-switch ml-3">
                                                                <input type="checkbox" class="custom-control-input" id="geolocateSwitchControlCenter" checked>
                                                                <label class="custom-control-label" for="geolocateSwitchControlCenter">Geolocate</label>
                                                            </div>`;

                            $('#dataTable-schools_filter').parent().append(toggleGeolocation);

                        },
                        responsive: true,
                        data : data.data,
                        columns: [
                            { data: 'school_code' },
                            { data: 'school_name' },
                            { data: 'population',searchable: false },
                            { data: 'sid_status',searchable: false },
                            { data: 'location' ,searchable: false },
                            { data: 'project_officers',searchable: false },

                        ],
                        columnDefs: [
                            {
                                targets: 4,
                                title: 'Location',
                                orderable: false,

                                render: function(data, type, full, meta)
                                {
                                    return `${data}<br><small class="text-muted">Greater Accra</small>`
                                },
                            },
                            {

                                targets: -1,
                                title: 'Assigned P.O.',
                                orderable: false,

                                render: function() {
                                    var img = '<div class="d-flex justify-content-end align-items-center">';
                                    if(data != null) {
                                        $.each(data, function(value){
                                            img += `<img style="border-radius: 50%; height: 40px; width:40px; margin-left: 12px;"
                                            data-toggle="officer-name-tooltip" title="Kofi baboni"  src="img/demo/avatars/avatar-f.png">`;
                                        });
                                    }

                                    img+=`</div>`;

                                    return img;
                                }

                            },
                            {

                                targets: 1,
                                title: 'School Name',
                                orderable: false,

                                render: function(data, type, full, meta)
                                {

                                    return `${data}<br><small class="text-muted">Start date 01-03-2021</small>`

                                },
                            },
                            {

                                targets: 3,
                                title: 'Status',
                                orderable: false,
                                render: function(data, type, full, meta)
                                {
                                    if(data == null)
                                        return ''
                                    else
                                        return `<span class="badge badge-pill project-status ${data} text-white">${commonObj.capitalizeFirstLetter(data)}</span><br><small class="text-muted">01.03.2021</small>`
                                },
                            },
                            {
                                targets: 2,
                                title: 'Population',
                                orderable: false,
                                render: function(data, type, full, meta)
                                {
                                    // let percentage = getSchoolPercentage(full.schoolCode, full.population)
                                    let percentage = (school_counts[full.school_code]/full.population) *100;
                                    let population = full.population;
                                    return `<div class="d-flex">Completed: ${percentage.toFixed(2)}%</div>
                                                            <div class="progress progress-sm mb-3">
                                                                <div class="progress-bar population completed submitted bg-fusion-400" role="progressbar" style="width:${percentage}%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>`
                                },
                            }
                        ]
                    });

                    $('#dataTable-schools tbody').on('click', 'tr td:not(:last-child)', function () {


                        geoLocateSchoolOnMap(schoolTable.row( this ).data() );

                    });
            });

        let channel = pusher.subscribe('submission-channel');
        channel.bind('App\\Events\\NewSubmissionEvent', function(data) {
            schoolTable.draw();
        });

        Dropzone.options.addAppReleaseFile = {
            init: function() {
                this.on("addedfile", function(file) {
                });
            },
            url : "https://staging.api.desafrica.com/sid-app-upload",
            success : function(data, res) {
                $(controlCenterClassObj.jsId.addAppReleaseFile).val(res);
            }
        };




        $(document).ready(function()
        {
            $('.project-officer-img').tooltip('show');

            $('.js-thead-colors a').on('click', function()
            {
                var theadColor = $(this).attr("data-bg");
                console.log(theadColor);
                $('#dt-basic-example thead').removeClassPrefix('bg-').addClass(theadColor);
            });

            $('.js-tbody-colors a').on('click', function()
            {
                var theadColor = $(this).attr("data-bg");
                console.log(theadColor);
                $('#dt-basic-example').removeClassPrefix('bg-').addClass(theadColor);
            });

            runDatePicker();

        });

        /**DATE PICKER INPUT**/

        var controls = {
            leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
            rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
        }

        var runDatePicker = function() {
            $('#DatePickerAddAppRelease').datepicker(
                {
                    orientation: "top left",
                    todayHighlight: true,
                    templates: controls
                }).on('changeDate', function(e) {

                $(controlCenterClassObj.jsId.addAppReleaseDate).val(e.format());
            });
        }

        /* TAB 1: UPDATING CHART */
        var data = [],
            totalPoints = 200;
        var getRandomData = function()
        {
            if (data.length > 0)
                data = data.slice(1);

            // do a random walk
            while (data.length < totalPoints)
            {
                var prev = data.length > 0 ? data[data.length - 1] : 50;
                var y = prev + Math.random() * 10 - 5;
                if (y < 0)
                    y = 0;
                if (y > 100)
                    y = 100;
                data.push(y);
            }

            // zip the generated y values with the x values
            var res = [];
            for (var i = 0; i < data.length; ++i)
                res.push([i, data[i]])
            return res;
        }
        // setup control widget
        var updateInterval = 1500;
        $("#updating-chart").val(updateInterval).change(function()
        {

            var v = $(this).val();
            if (v && !isNaN(+v))
            {
                updateInterval = +v;
                $(this).val("" + updateInterval);
            }

        });
        // setup plot
        var options = {
            colors: ['#6FB3E6'],
            series:
                {
                    lines:
                        {
                            show: true,
                            lineWidth: 0.5,
                            fill: 0.9,
                            fillColor:
                                {
                                    colors: [
                                        {
                                            opacity: 0.9,
                                        },
                                        {
                                            opacity: 0
                                        }]
                                },
                        },

                    shadowSize: 0 // Drawing is faster without shadows
                },
            grid:
                {
                    borderColor: 'rgba(0,0,0,0.05)',
                    borderWidth: 1,
                    labelMargin: 5
                },
            xaxis:
                {
                    color: '#F0F0F0',
                    tickColor: 'rgba(0,0,0,0.05)',
                    font:
                        {
                            size: 10,
                            color: '#999'
                        }
                },
            yaxis:
                {
                    min: 0,
                    max: 100,
                    color: '#F0F0F0',
                    tickColor: 'rgba(0,0,0,0.05)',
                    font:
                        {
                            size: 10,
                            color: '#999'
                        }
                }
        };
        var plot = $.plot($("#updating-chart"), [getRandomData()], options);
        /* live switch */
        $('input[type="checkbox"]#start_interval').click(function()
        {
            if ($(this).prop('checked'))
            {
                $on = true;
                updateInterval = 1500;
                update();
            }
            else
            {
                clearInterval(updateInterval);
                $on = false;
            }
        });
        var update = function()
        {
            if ($on == true)
            {
                plot.setData([getRandomData()]);
                plot.draw();
                setTimeout(update, updateInterval);
            }
            else
            {
                clearInterval(updateInterval)
            }
        }

        // fetch(`/api/sync-jobs`).then(function (){
        //     console.log("jobs synced successfully!")
        // });

    </script>

@endsection

@section("content")

    <main id="js-page-content" role="main" class="page-content">

        <ol class="breadcrumb page-breadcrumb">
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
        </ol>

        <div class="subheader">
            <h1 class="subheader-title">
                <i class='subheader-icon fal fa-globe'></i> Control Center
                <!--<small>
                    Insert page description or punch line
                </small>
                -->
            </h1>
            <!--
            Right content on content header
            A nice area to add graphs or buttons
            <div class="subheader-block">
                Right Subheader Block
            </div>
             -->
        </div>

        <!-- Your main content goes below here: -->
        <div class="row">
            <div class="col-xl-12">
                <div id="panel-1" class="panel">
                    <div class="panel-hdr">
                        <h2>
                            Progress Map
                        </h2>

                    </div>
                    <div class="panel-container show">
                        <div class="panel-content row">
                            <div class="col-md-9">
                                <div class="d-flex justify-content-center align-items-center google-maps-container" id="google-maps-container"></div>
                            </div>
                            <div class="col-md-3">
                                <div class="google-maps-legends">
                                    <h2>Key</h2>

                                    <div class="d-flex justify-content-center align-items-center">
                                        <div class="legend-dot d-inline-block target"></div>
                                        <div class="d-inline-block flex-grow-1 ml-3">
                                            <div class="d-flex">
                                                Target
                                                <span id="TargetSpan" class="d-inline-block ml-auto">0</span>
                                            </div>
                                            <div class="progress progress-sm mb-3">
                                                <div id="TargetProgress" class="progress-bar target bg-fusion-400" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-center align-items-center">
                                        <div class="legend-dot started d-inline-block"></div>
                                        <div class="d-inline-block flex-grow-1 ml-3">
                                            <div class="d-flex">
                                                Started
                                                <span id="StartedSpan" class="d-inline-block ml-auto">0</span>
                                            </div>
                                            <div class="progress progress-sm mb-3">
                                                <div id="StartedProgress" class="progress-bar started bg-fusion-400" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0"></div>
                                            </div>
                                        </div>
                                    </div>




                                    <div class="d-flex justify-content-center align-items-center">
                                        <div class="legend-dot submitted d-inline-block"></div>
                                        <div class="d-inline-block flex-grow-1 ml-3">
                                            <div class="d-flex">
                                                Submitted
                                                <span id="SubmittedSpan" class="d-inline-block ml-auto">0</span>
                                            </div>
                                            <div class="progress progress-sm mb-3">
                                                <div id="SubmittedProgress" class="progress-bar submitted bg-fusion-400" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-center align-items-center">
                                        <div class="legend-dot printed d-inline-block"></div>
                                        <div class="d-inline-block flex-grow-1 ml-3">
                                            <div class="d-flex">
                                                Printed
                                                <span id="PrintedSpan" class="d-inline-block ml-auto">0</span>
                                            </div>
                                            <div class="progress progress-sm mb-3">
                                                <div id="PrintedProgress" class="progress-bar printed bg-fusion-400" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-center align-items-center">
                                        <div class="legend-dot delivered d-inline-block"></div>
                                        <div class="d-inline-block flex-grow-1 ml-3">
                                            <div class="d-flex">
                                                Delivered
                                                <span id="DeliveredSpan" class="d-inline-block ml-auto">0</span>
                                            </div>
                                            <div class="progress progress-sm mb-3">
                                                <div id="DeliveredProgress" class="progress-bar delivered bg-fusion-400" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="solid">
                                <div class="project-completion-chats row">
                                    <div class="col-6">
                                        <div class="mb-3">Overall <br>Completion</div>
                                        <div id="OverallCompletion" class="js-easy-pie-chart overall-completion color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="23" data-piesize="50" data-linewidth="5" data-linecap="butt" data-scalelength="0">
                                            <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-lg">
                                                <span class="js-percent d-block text-dark"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="mb-3">Active <br> Officers</div>
                                        <div id="ActiveOfficers" class="js-easy-pie-chart active-officers color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="75" data-piesize="50" data-linewidth="5" data-linecap="butt" data-scalelength="0">

                                            <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-lg">
                                                <span class="js-percent d-block text-dark"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div id="panel-1" class="panel">
                    <div class="panel-hdr">
                        <h2>
                            Schools
                        </h2>

                    </div>
                    <div class="panel-container show">
                        <div class="panel-content row">

                            <div class="col-12">
                                <table id="dataTable-schools" class="table table-bordered table-hover table-striped w-100">
                                    <thead>
                                    <tr>
                                        <th>School Code</th>
                                        <th>School Name</th>
                                        <th>Population</th>
                                        <th>Status</th>
                                        <th>Location</th>
                                        <th>Assigned P.O.</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr></tr>
                                    </tbody>

                                    <tfoot>
                                    <tr>
                                        <th>School Code</th>
                                        <th>School Name</th>
                                        <th>Population</th>
                                        <th>Status</th>
                                        <th>Location</th>
                                        <th>Assigned P.O.</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-7">
                <div id="panel-1" class="panel">
                    <div class="panel-hdr">
                        <h2>
                            Server stats
                        </h2>

                    </div>
                    <div class="panel-container show">
                        <div class="panel-content">
                            <div class="position-relative">
                                <div class="custom-control custom-switch position-absolute pos-top pos-left ml-5 mt-3 z-index-cloud">
                                    <input type="checkbox" class="custom-control-input" id="start_interval">
                                    <label class="custom-control-label" for="start_interval">Live Update</label>
                                </div>
                                <div id="updating-chart" style="height:242px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div id="panel-1" class="panel">
                    <div class="panel-hdr">
                        <h2>
                            Mobile App release
                        </h2>

                    </div>
                    <div class="panel-container show">
                        <div class="panel-content">
                            <form action="/upload" id="add-app-release-file" class="dropzone needsclick" style="min-height: 7rem; background-color: white;">
                                <div class="dz-message needsclick">
                                    <span class="fs-sm text-muted">Drop app here in .apk</span>
                                </div>
                                <input type="hidden" value="" id="AddAppReleaseFile">
                            </form>
                            <form>
                                <div class="form-group row mt-3">
                                    <label class="col-form-label col-12 col-lg-4 form-label text-lg-left">Publish date</label>
                                    <div class="col-12 col-lg-8 demo-v-spacing-sm">
                                        <div class="input-group">
                                            <input type="text" class="form-control " placeholder="" id="DatePickerAddAppRelease">
                                            <input type="hidden" value="" id="AddAppReleaseDate">

                                            <div class="input-group-append">
                                                                <span class="input-group-text fs-xl">
                                                                    <i class="fal fa-calendar-check"></i>
                                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label whitespace-nowrap col-md-12 col-lg-4 form-label text-lg-left">Version name</label>
                                    <div class="col-md-12 col-lg-8">
                                        <div class="input-group">
                                            <input type="text" class="form-control " placeholder="" id="AddAppVersionName">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label whitespace-nowrap col-md-12 col-lg-4 form-label text-lg-left">App URL</label>
                                    <div class="col-md-12 col-lg-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon3">http://</span>
                                            </div>
                                            <input type="text" class="form-control" id="AddAppDownloadUrl" aria-describedby="basic-addon3">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary m-auto d-flex w-50 justify-content-center" id="AddAppReleaseBtn" onclick="controlCenterObj.addAppRelease(event)">Schedule Release</button>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
