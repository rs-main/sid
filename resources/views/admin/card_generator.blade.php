@extends("layouts.master")

@section("scripts")
    @include("partials.card_generator.scripts")
@endsection

@section("content")

    <main id="js-page-content" role="main" class="page-content">

        <ol class="breadcrumb page-breadcrumb">
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
        </ol>

        <div class="subheader">
            <h1 class="subheader-title">
                <i class='subheader-icon fal fa-credit-card'></i> Card generator
                <!--<small>
                    Insert page description or punch line
                </small>
                -->
            </h1>
            <!--
            Right content on content header
            A nice area to add graphs or buttons
            <div class="subheader-block">
                Right Subheader Block
            </div>
             -->
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div id="panel-1" class="panel">
                    <div class="panel-container show">
                        <div class="panel-content row">

                            <div class="col-12">


                                <table id="dataTable-card-generator" class="table table-bordered table-hover table-striped w-100">
                                    <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Job</th>
                                        <th>Project name</th>
                                        <th>Institution</th>
                                        <th>Project officer</th>
                                        <th>Submitted on</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
{{--                                    <tr>--}}
{{--                                        <td>1</td>--}}
{{--                                        <td>Job 1</td>--}}
{{--                                        <td>GES SHS 2020 e-card</td>--}}
{{--                                        <td>Accra high School</td>--}}
{{--                                        <td>Kofi baboni <br><small class="text-muted">Device ID#</small></td>--}}
{{--                                        <td>01-03-2021</td>--}}
{{--                                        <td>Generated</td>--}}
{{--                                        <td></td>--}}
{{--                                    </tr>--}}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Job</th>
                                        <th>Project name</th>
                                        <th>Institution</th>
                                        <th>Project officer</th>
                                        <th>Submitted on</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-8">
                <div id="panel-1" class="panel">
                    <div class="panel-hdr">
                        <h2>
                            Card Template
                        </h2>

                    </div>
                    <div class="panel-container show">
                        <div class="panel-content row">
                            <div class="col-12">

                                <div class="row">
                                    <div class="col-lg-8">

                                        <div class="position-relative">
                                            <div class="position-relative">
                                                <figure>
                                                    <img src="img/BG-3-Security-card.png" class="w-100" style="border: 1px solid rgba(0, 0, 0, 0.07);">
                                                </figure>

                                                <div class="position-absolute" id="card-generator-school-crest">
                                                    <div class="text-center" style="background-color: #f1f1f1;border: 1px solid #cccccc;">School <br>crest<br> (img)</div>
                                                </div>

                                                <div class="position-absolute" id="card-generator-student-name">
                                                    <span>STUDENT FULL NAME</span>
                                                </div>
                                            </div>
                                        <div id="card-generator-overlay" class="w-100" ></div>

                                        

                                        <div class="position-absolute" id="card-generator-school-name">
                                            <span>SCHOOL NAME</span>
                                        </div>
                                        <div class="position-absolute" id="card-generator-student-img">
                                            <div class="text-center d-flex align-items-center justify-content-center" style="background-color: #f1f1f1;border: 1px solid #cccccc;">Student <br>Photo</div>
                                        </div>
                                        
                                        <div class="position-absolute" id="card-generator-guardian-contact-no">
                                            <span>PHONE NO.</span>
                                        </div>
                                        <div class="position-absolute" id="card-generator-issue-date">
                                            <span>ISSUE DATE</span>
                                        </div>
                                        <div class="position-absolute" id="card-generator-id-no">
                                            <span>ID NO.</span>
                                        </div>
                                        <div class="position-absolute" id="card-generator-sex">
                                            <span>SEX</span>
                                        </div>
                                        <div class="position-absolute" id="card-generator-expiry-date">
                                            <span>EXPIRY DATE</span>
                                        </div>
                                        <div class="position-absolute" id="card-generator-qr-code">
                                            <div class="d-flex align-items-center text-center" style="background-color: #f1f1f1;border: 1px solid #cccccc;">QR <br>code<br> (img)</div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="w-100 card-generator-expected-data d-flex flex-column h-100">


                                            <div style="border: 1px solid rgba(0, 0, 0, 0.07);">
                                                <div class="py-1 pl-3" ><strong>Expected Data</strong></div>
                                            </div>

                                            <div style="border: 1px solid rgba(0, 0, 0, 0.07);">
                                                <div class="py-1  pl-3">School name</div>
                                                <div class="py-1  pl-3">School Crest</div>
                                                <div class="py-1  pl-3">Student Full name</div>
                                                <div class="py-1  pl-3">Date of Birth</div>
                                                <div class="py-1  pl-3">School year</div>
                                                <div class="py-1  pl-3 ">ID Number</div>
                                                <div class="py-1  pl-3">QR Code</div>
                                                <div class="py-1  pl-3">Photo</div>
                                            </div>

                                        </div>



                                    </div>

                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div id="panel-1" class="panel">
                    <div class="panel-hdr">
                        <h2>
                            Card Background Artwork preview
                        </h2>

                    </div>
                    <div class="panel-container show">
                        <div class="panel-content">
                            <div  class="mb-3"><strong>Front</strong></div>
                            <figure>
                                <img src="img/SID_card-Sample_sec_1.png" class="w-100" style="border: 1px solid rgba(0, 0, 0, 0.07); border-radius: 12px;">
{{--                                <iframe--}}
{{--                                    src="/app/1800101060071.pdf"--}}
{{--                                    frameBorder="0"--}}
{{--                                    scrolling="auto"--}}
{{--                                    height="100%"--}}
{{--                                    width="100%"--}}
{{--                                ></iframe>--}}
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </main>


@endsection
