@extends("layouts.master")

@section("scripts")
@include("partials.databank.scripts")
@endsection

@section("content")
    <main id="js-page-content" role="main" class="page-content">

        <ol class="breadcrumb page-breadcrumb">
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
        </ol>

        <div class="subheader">
            <h1 class="subheader-title">
                <i class='subheader-icon fal fa-shopping-bag'></i> Data bank
                <!--<small>
                    Insert page description or punch line
                </small>
                -->
            </h1>
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div id="panel-1" class="panel">
                    <div class="panel-container show">
                        <div class="panel-content row">

                            <div class="col-12">
                                <table id="dataTable-data-bank" class="table table-bordered table-hover table-striped w-100">
                                    <thead>
                                    <tr>
                                        <th>Person name</th>
                                        <th>ID No.</th>
                                        <th>Project name</th>
                                        <th>Institution name</th>
                                        <th>Gender</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
{{--                                    <tr>--}}
{{--                                        <td>Kofi wayo</td>--}}
{{--                                        <td>--}}
{{--                                            <div id="sid-card-tooltip" role="tooltip"><img src="img/SID_card_Sample.png" style="width: 350px; height: auto;"></div>--}}
{{--                                            <a href="" id="sid-card-link" class="underline" aria-describedby="tooltip">012020231308</a>--}}
{{--                                        </td>--}}
{{--                                        <td>GES SHS 2020 e-card</td>--}}
{{--                                        <td>Accra high School</td>--}}
{{--                                        <td>M</td>--}}
{{--                                        <td></td>--}}
{{--                                    </tr>--}}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Person name</th>
                                        <th>ID No.</th>
                                        <th>Project name</th>
                                        <th>Institution name</th>
                                        <th>Gender</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </main>
@endsection
