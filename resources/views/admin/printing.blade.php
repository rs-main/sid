@extends("layouts.master")

@section("scripts")
<script src="js/printing_center.js"></script>
    <script>
        /* demo scripts for change table color */
        /* change background */


        $(document).ready(function()
        {
        $('#PrintPanelGoBackBtn').on('click', function() {
        $('#PrintPanelPageSubheader').fadeIn(500);
        $('#PrintPanelPageBreadcrumb').fadeOut(500);
        $('#PrintPanelPageBreadcrumbDate').fadeIn(500);
        $('#PrintPanelHTML').fadeIn(500);
        $('#JobPillPanelHTML').fadeOut(500);

    });

            $.fn.dataTable.ext.errMode = 'none';
            let printing_center = $('#dataTable-printing-center').DataTable(
                {
                    initComplete: function() {
                        var datatable_length_text = $('.dataTables_length label').contents().filter(function() {
                                if(this.nodeType === 3) {
                                   if(this.data == ' entries') {
                                        return this.nodeType === 3;
                                   }
                                }
                            }).wrap('<span style="display:none"></style>');

                        // $(".job-pill").on("click",function (){
                        //     let imei = $(this).data("imei");
                        //     let code = $(this).data("school-code");
                        //     location.href = `/job-details/${code}/${imei}`;
                        // });

                        $("#dataTable-printing-center").on("click", ".job-pill", function(){
                            // your code goes here

                            let imei = $(this).data("imei");
                            let code = $(this).data("school-code");
                            location.href = `/job-details/${code}/${imei}`;
                        });

                    },
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    createdRow: function( row, data, dataIndex ) {
                        $( row ).find('td:eq(2)')
                        .attr('ondragover' , "printCenterObj.onDragOver(event)")
                        .attr('ondrop' , "printCenterObj.updateJobStatusToCompleted(event)")

                        $( row ).find('td:eq(1)')
                        .attr('ondragover' , "printCenterObj.onDragOver(event)")
                        .attr('ondrop' , "printCenterObj.updateJobStatusToPending(event)")
                    },
                    ajax: "{{ route('jobs.data') }}",
                    columns: [
                        {data: 'school_name', name: 'school_name'},
                        {data: 'print_pending', name: 'print_pending'},
                        {data: 'print_completed', name: 'print_completed'},
                        {data: 'press', name: 'press'},
                        {data: 'quality_officer', name: 'quality_officer'},
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            title:'School Name',

                            render: function(data, type, full, meta)
                            {
                                return `${data}<br><small class="text-muted">

                                        </small>`;
                                // return `<!--${data}<br><small class="text-muted">Submit date 01-03-2021</small>-->`;
                            },
                        },
                        {
                            targets: 1,
                            title:'Print Pending',

                            render: function(data, type, full, meta)
                            {
                                let pending_count = 0;
                                let pending = `<div class="d-grid grid-col-3 print-jobs-pending" id="PrintJobsPending">`;
                                full.print_jobs.forEach(function (item){

                                    if (item.pending === 1){
                                        pending_count++

                                        let elem = `
                                                <div class="badge-pill-wrapper pb-1" draggable="true" ondragstart="printCenterObj.onDragStart(event)" ondragend="" data-job-id="1">
                                                        <span data-imei=${item.imei} data-school-code=${full.school_code} class="badge badge-pill project-status job-pill started text-white pill-pending"
                                                         data-innerText="Job 3"> Job ${pending_count}</span>
                                              </div>


                                        `
                                        pending+=elem;
                                    }
                                })
                                return `${pending}</div>`
                            },
                        },

                        {
                            targets: 2,
                            title:'Print Completed',
                            className: "dragdropzone",
                            render: function(data, type, full, meta)
                            {
                                let completed_count = 0;
                                let completed =  `<div class="d-grid grid-col-3 print-jobs-completed" id="PrintJobsCompleted"
                                                 ondrop="printCenterObj.updateJobStatusToCompleted(event)">`;
                                full.print_jobs.forEach(function (item){

                                    if (item.completed === 1){
                                        completed_count++

                                        let elem = `
                                            <div class="badge-pill-wrapper pb-1" draggable="true"
                                                ondragstart="printCenterObj.onDragStart(event)" ondragend="" data-job-id="1">
                                                        <span data-imei=${item.imei} data-school-code=${full.school_code} class="badge badge-pill delivered project-status job-pill started text-white"
                                                         data-innerText="Job 3"> Job ${completed_count}</span>
                                            </div>

                                        `
                                        completed+=elem;
                                    }
                                })
                                return `${completed}</div`
                            },
                        },

                        {
                            targets: 3,
                            title:'Press',

                            render: function(data, type, full, meta)
                            {
                                return ` <td>Mr. Baboni<br>
                                                <small class="text-muted">Greater Accra</small>
                                            </td>`;
                            },
                        },

                        {
                            targets: 4,
                            title:'Assigned Q.O.',

                            render: function(data, type, full, meta)
                            {
                                return ` <td>Mr. Baboni<br>
                                                <small class="text-muted">Greater Accra</small>
                                            </td>`;
                            },
                        }
                    ],
                });

            let press_datatable =$('#press-datatable').DataTable(
                {
                    processing: true,
                    searching : false,
                    paging : false,
                    info : false,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('print.presses') }}",
                    columns: [
                        {data: 'press', name: 'press'},
                        {data: 'downloaded', name: 'print_pending'},
                        {data: 'printed', name: 'print_completed'},
                        {data: 'press', name: 'press'},
                        {data: 'quality_officer', name: 'quality_officer'},
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            title:'Press',

                            render: function(data, type, full, meta)
                            {
                                return `${data}<br><small class="text-muted">${full.officer_phone}</small>`
                            },
                        },

                        {
                            targets: 1,
                            title:'Downloaded',

                            render: function(data, type, full, meta)
                            {
                                return `${data}<br><small class="text-muted">${full.jobs} Jobs</small>`
                            },
                        },

                        {
                            targets: 2,
                            title:'Printed',

                            render: function(data, type, full, meta)
                            {
                                return `${full.report.printed}<br><small class="text-muted">${full.jobs} Jobs</small>`
                            },
                        },

                        {
                            targets: 3,
                            title:'Delivered',

                            render: function(data, type, full, meta)
                            {
                                return `${full.report.delivered}<br><small class="text-muted">${full.jobs} Jobs</small>`
                            },
                        },

                        {
                            targets: 4,
                            title:'Assigned Q.0.',

                            render: function(data, type, full, meta)
                            {
                                return `${full.officer}<br><small class="text-muted"></small>`
                            },
                        },


                    ],
                });

            let channel = pusher.subscribe('submission-channel');
            channel.bind('App\\Events\\NewSubmissionEvent', function(data) {
                printing_center.draw();
                press_datatable.draw();
            });

            // setTimeout(function (){

                // $(".job-pill").on("click",function (){
                //     let imei = $(this).data("imei");
                //     let code = $(this).data("school-code");
                //     location.href = `/job-details/${code}/${imei}`;
                // });

            // },2000);

            $('.js-thead-colors a').on('click', function()
            {
                var theadColor = $(this).attr("data-bg");
                console.log(theadColor);
                $('#dt-basic-example thead').removeClassPrefix('bg-').addClass(theadColor);
            });

            $('.js-tbody-colors a').on('click', function()
            {
                var theadColor = $(this).attr("data-bg");
                console.log(theadColor);
                $('#dt-basic-example').removeClassPrefix('bg-').addClass(theadColor);
            });

            runDatePicker();

        });

        /**DATE PICKER INPUT**/

        var controls = {
            leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
            rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
        }

        var runDatePicker = function() {
            $('#datepicker-4-1').datepicker(
                {
                    orientation: "top left",
                    todayHighlight: true,
                    templates: controls
                });
        }


    </script>

@endsection

@section("content")

    <main id="js-page-content" role="main" class="page-content">

        <ol class="breadcrumb page-breadcrumb" id="PrintPanelPageBreadcrumbDate">
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
        </ol>

        <div class="subheader" id="PrintPanelPageSubheader">
            <h1 class="subheader-title">
                <i class='subheader-icon fal fa-print'></i> Printing Center
            </h1>

{{--            <h1 class="subheader-title float-right">--}}
{{--                <i class='subheader-icon fal fa-print'></i> Printing Center--}}
{{--            </h1>--}}
        </div>


        <div class="demo-v-spacing mb-5 print-panel-page-breadcrumb"  id="PrintPanelPageBreadcrumb"  style="display: none;">

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="printing.html">Printing panel</a>
                </li>

                <li class="breadcrumb-item active">Job data table</li>
            </ol>

        </div>


        <!-- Your main content goes below here: -->
        <div id="PrintPanelHTML">
            <div class="row">
                <div class="col-xl-12">
                    <div id="panel-1" class="panel">
                        <div class="panel-hdr">
                            <h2>
                                Press management
                            </h2>

                        </div>
                        <div class="panel-container show">
                            <div class="panel-content row">

                                <div class="col-12">
                                    <table id="dataTable-printing-center" class="table table-bordered table-hover table-striped w-100">
                                        <thead>
                                        <tr>
                                            <th>School Name</th>
                                            <th>Print pending</th>
                                            <th>Print completed</th>
                                            <th>Press</th>
                                            <th>Assigned Q.O.</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>School Name</th>
                                            <th>Print pending</th>
                                            <th>Print complete</th>
                                            <th>Press</th>
                                            <th>Assigned Q.O.</th>
                                        </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8">
                    <div id="panel-1" class="panel">
                        <div class="panel-hdr">
                            <h2>
                                Press report
                            </h2>

                        </div>
                        <div class="panel-container show">
                            <div class="panel-content row">
                                <div class="col-12">
                                    <table id="press-datatable" class="table table-bordered table-hover table-striped w-100">
                                        <thead>
                                        <tr>
                                            <th>Press</th>
                                            <th>Downloaded</th>
                                            <th>Printed</th>
                                            <th>Delivered</th>
                                            <th>Assigned Q.O.</th>
                                        </tr>
                                        </thead>
                                        <tbody>
{{--                                        <tr>--}}
{{--                                            <td>Lema Press<br><small class="text-muted">0244112233</small></td>--}}
{{--                                            <td>125,000<br><small class="text-muted">500 Jobs</small></td>--}}
{{--                                            <td>125,000<br><small class="text-muted">500 Jobs</small></td>--}}
{{--                                            <td>125,000<br><small class="text-muted">500 Jobs</small></td>--}}
{{--                                            <td>Kofi baboni<br><small class="text-muted">0244112233</small></td>--}}
{{--                                        </tr>--}}

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <table id="dataTable-printing-center" class="mb-0 table table-bordered table-hover table-striped w-100">
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center flex-wrap">
                                        <div class="mr-3 js-easy-pie-chart percentage-submitted color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="75" data-piesize="50" data-linewidth="5" data-linecap="butt" data-scalelength="0">
                                            <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-lg">
                                                <span class="js-percent d-block text-dark"></span>
                                            </div>
                                        </div>
                                        <div>% of total <br>Submitted</div>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center  flex-wrap">
                                        <div class="mr-3 js-easy-pie-chart percentage-downloaded color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="36" data-piesize="50" data-linewidth="5" data-linecap="butt" data-scalelength="0">
                                            <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-lg">
                                                <span class="js-percent d-block text-dark"></span>
                                            </div>
                                        </div>
                                        <div>% of total <br>Downloaded</div>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center  flex-wrap">
                                        <div class="mr-3 js-easy-pie-chart percentage-printed color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="23" data-piesize="50" data-linewidth="5" data-linecap="butt" data-scalelength="0">
                                            <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-lg">
                                                <span class="js-percent d-block text-dark"></span>
                                            </div>
                                        </div>
                                        <div>% of total <br>Printed</div>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center  flex-wrap">
                                        <div class="mr-3 js-easy-pie-chart percentage-delivered color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="79" data-piesize="50" data-linewidth="5" data-linecap="butt" data-scalelength="0">
                                            <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-lg">
                                                <span class="js-percent d-block text-dark"></span>
                                            </div>
                                        </div>
                                        <div>% of total <br>Delivered</div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div id="panel-1" class="panel">
                        <div class="panel-hdr">
                            <h2>
                                Card Artwork preview
                            </h2>

                        </div>
                        <div class="panel-container show">
                            <div class="panel-content">

                                <figure>
                                    <img src="img/SID_card-Sample_sec_1.png" class="w-100">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row" id="JobPillPanelHTML"  style="display: none;">
            <div class="col-xl-12">
                <div id="panel-1" class="panel">

                    <div class="panel-container show">
                        <div class="panel-content row">

                            <div class="col-12">
                                <button class="btn btn-outline-primary mb-3" id="PrintPanelGoBackBtn">
                                    <span class="icon"><i class="fal fa-arrow-left"></i></span>
                                    Go Back
                                </button>
                                <table id="dataTable-job" class="table table-bordered table-hover table-striped w-100">
                                    <thead class="dataTable-project-officers-table-head">
                                    <tr>
                                        <th>S/N</th>
                                        <th>Card ID No.</th>
                                        <th>Full name</th>
                                        <th>School name</th>
                                        <th>Attribute</th>
                                        <th>Photo</th>
                                        <th>QR</th>
                                        <th>Bio</th>
                                        <th>G.Card</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Card ID No.</th>
                                        <th>Full name</th>
                                        <th>School name</th>
                                        <th>Attribute</th>
                                        <th>Photo</th>
                                        <th>QR</th>
                                        <th>Bio</th>
                                        <th>G.Card</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
