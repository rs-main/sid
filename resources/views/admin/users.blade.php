@extends("layouts.master")

@section("scripts")
    @include("partials.users.scripts")
@endsection

@section("content")

    <main id="js-page-content" role="main" class="page-content">

        <ol class="breadcrumb page-breadcrumb">
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
        </ol>

        <div class="subheader">
            <h1 class="subheader-title">
                <i class='subheader-icon fal fa-id-card'></i> Administrative Accounts
            </h1>

        </div>

        <div class="row d-flex align-items-center pb-4">
{{--            <div class=" mb-3">Add new</div>--}}
            <a data-toggle="modal" id="add-account" data-target="#addAccountModal" class="col-lg col-md-3 col-sm-3 mb-3">
                <button class="btn btn-primary">
                    <span class="icon">
                        <i class="fal fa-plus"></i>
                    </span>
                    Add Account
                </button>
            </a>

{{--            <a data-toggle="modal" data-target="#addSchoolModal" class="col-lg col-md-3 col-sm-3 mb-3">--}}
{{--                <button class="btn btn-primary w-100"><span class="icon"><i class="fal fa-plus"></i></span>Role</button></a>--}}
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div id="panel-1" class="panel">
                    <div class="panel-container show">
                        <div class="panel-content" id="panelHtml">

                            <div style="">
                                <table id="dataTable-users" class="table table-bordered table-hover table-striped w-100">
                                    <thead class="dataTable-project-officers-table-head">
                                    <tr>
                                        <th>S/N</th>
                                        <th>Account Name</th>
                                        <th>Role</th>
                                        <th>Access</th>
                                        <th>Email & Phone</th>
                                        <th>Theme</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Account Name</th>
                                        <th>Role</th>
                                        <th>Access</th>
                                        <th>Email & Phone</th>
                                        <th>Theme</th>
                                        <th>Status</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>

                            @include("partials.users.add_account_modal")

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
