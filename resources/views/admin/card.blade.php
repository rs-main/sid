<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">
        <title></title>

        <style>
            body{
                width: 1012px !important;
                /*height:608px;*/
                /*margin: auto;*/
                margin-left: 0;
                margin-top: -40px;
            }

            .position-absolute{
                position: absolute;
                font-family: 'Roboto', sans-serif;
                font-weight: 500;
                font-size: 40px;
                bottom: 0;
                right:0;
                margin-top: -40px;
            }

            #name_of_school{
                text-align: center;
                top: 185px;
                margin: auto;
                width: 100%;
                position: relative;
                font-family: 'Roboto', sans-serif;
                font-weight: 500;
                font-size: 45px;
                bottom: 0;
                right:0;
            }

            .left-input{}

            #name_of_student{
                left: 375px;
                top: 280px;
            }

            #guardians_contact{
                /*left: 830px;*/
                left: 375px;
                top: 375px;
            }

            #sex{
                /*left: 1122px;*/
                left: 670px;
                top: 375px;
            }

            #issue-date{
                /*left: 830px;*/
                left: 372px;
                top: 468px;
            }

            #expiry-date{
                left: 570px;
                top: 469px;
            }

            #card-id{
                left: 373px;
                top: 564px;
            }

            #qr-code{
                position: absolute;
                font-weight: 500;
                font-size: 34px;
                bottom: 0;
                right:0;
                left: 857px !important;
                top: 447px;
            }

            #school_logo{
                top: 37px;
                width: 100px;
                left: 40px;
                padding-top: 22px;
            }

            #photo{
                top: 293px;
                /*top: 273px;*/
                left: 69px;
            }

        </style>
    </head>

<body>
{{--     @foreach($students as $student)--}}
         <img id="photo" class="position-absolute" src="{{isset($student->profile_img_path) ? "/images/".$student->profile_img_path : ""}}" alt="student's image" width="23.5%"/>
{{--        <img id="photo" class="position-absolute" src="{{asset("img/fred.jpg")}}" width="270" height="320" alt="student's image"/>--}}
        <img id="school_logo" class="position-absolute" src="{{asset("img/school_crests/".optional($student)->school_code.".png")}}" alt="School image"/>
        <p id="name_of_school" >{{strtoupper(optional($student)->school_name)}}</p>
        <p id="name_of_student" class="position-absolute" >{{strtoupper(optional($student)->first_name)}}</p>
        <p id="guardians_contact" class="position-absolute">{{optional($student)->guardian_contact_number}}</p>
        <p id="sex" class="position-absolute">{{optional($student)->gender === "Male" ? "M" : "F"}}</p>
        <p id="issue-date" class="position-absolute">10/2020</p>
        <p id="expiry-date" class="position-absolute">10/{{\App\Models\Student::getStudentExpiry(optional($student)->class)}}</p>
        <p id="card-id" class="position-absolute">{{optional($student)->card_id}}</p>


        @php
            $first_name = $student? $student->first_name:"";
            $last_name = $student? $student->last_name:"";
            $school_name = $student? $student->school_name:"";
            $card_id = $student? $student->card_id:"";
            $qr_code_string = $first_name."\n". $school_name."\n".$card_id;
        @endphp

        <p id="qr-code">
            {!! '<img src="data:image/png;base64,' . DNS2D::getBarcodePNG($qr_code_string, 'QRCODE') . '" alt="barcode" width="157px" height="161px"    />' !!}
        </p>

        <div style=" text-align: center">
            <img src="{{asset("/card_layout.png")}}"  alt="student card"/>
        </div>

{{--     @endforeach--}}

</body>

</html>
