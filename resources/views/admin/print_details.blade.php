@extends("layouts.master")

@section("scripts")
<script src="{{asset("js/printing_center.js")}}"></script>
    <script>

        $(document).ready(function() {
            $('#PrintPanelGoBackBtn').on('click', function () {
                window.history.back();
            });

            $('.js-thead-colors a').on('click', function () {
                var theadColor = $(this).attr("data-bg");
                console.log(theadColor);
                $('#dt-basic-example thead').removeClassPrefix('bg-').addClass(theadColor);
            });

            $('.js-tbody-colors a').on('click', function () {
                var theadColor = $(this).attr("data-bg");
                console.log(theadColor);
                $('#dt-basic-example').removeClassPrefix('bg-').addClass(theadColor);
            });

            let school_code = "{{$code}}"
            let imei        = "{{$imei}}";

            $.fn.dataTable.ext.errMode = 'none';
            let table = $('#dataTable-job').DataTable(
                {
                    responsive: true,
                    initComplete: function() {
                        getJobDetails();
                    },
                    "dom" : "<'row align-items-end '<'col-lg-6'<'#DataReceivedForJobName.col-lg-12'>><'col-lg-6 d-flex justify-content-end'f>>",
                    processing: true,
                    serverSide: true,
                    ajax: `/api/jobs/${school_code}/${imei}`,
                    columns: [
                        {data: 'serial_number', name: 'serial_number'},
                        {data: 'card_id', name: 'card_id',orderable:false},
                        {data: 'full_name', name: 'full_name'},
                        {data: 'school_name', name: 'school_name'},
                        {data: 'class', name: 'class'},
                        {data: 'photo', name: 'photo'},
                        {data: 'qr_code', name: 'qr_code'},
                        {data: 'bio', name: 'bio'},
                        {data: 'action', name: 'action'}
                    ],
                    columnDefs: [
                        {
                            targets: -1,
                            render: function(data, type, full, meta) {
                                let elem =  `\n\t\t\t\t\t\t
                                    <div class='d-flex demo'>\n\t\t\t\t\t\t\t
                                    <a href="/${full.card_id}.png" target="_blank" class='btn btn-sm btn-outline-success btn-icon btn-inline-block mr-1'
                                    title='View'><i class=\"fal fa-eye\"></i></a>\n\t\t\t\t\t\t\t<a href='javascript:void(0);'
                                    class='btn btn-sm btn-outline-warning btn-icon btn-inline-block mr-1 edit-student' title='Edit'>
                                    <i class=\"fal fa-edit btn-white-icon\"></i></a>\n\t\t\t\t\t\t\t
                                    <div class='dropdown d-inline-block'>\n\t\t\t\t\t\t\t\t`;

                                return full.generated === 1 ? elem : "<i>Not generated</i>"
                            }
                        },

                        {
                            "searchable": false,
                            "orderable": false,
                            "targets": 0
                        } ,

                        {
                            targets: 5,
                            render: function(data, type, full, meta) {
                                console.log(full, "details")
                                return `<a href=/images/${full.profile_img_path} class="underline">/url</a>`;
                            }
                        },

                        {
                            targets: 6,
                            render: function(data, type, full, meta) {
                                return `<a target="_blank" href=${data} class="underline">/url</a>`;
                            }
                        },

                        {
                            targets: 7,
                            render: function(data, type, full, meta) {
                                return `yes`;
                            }
                        },
                    ],
                });

            table.on( 'order.dt search.dt', function () {
                table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();

            function getJobDetails(){
                fetch(`/api/job/${imei}/${school_code}`)
                    .then((resp) => resp.json())
                    .then(function (data){
                    $('#DataReceivedForJobName').html(`
                 <div><strong>Data received for Job</strong></div>
                        <div class="row">

                                <div class="col-lg-6">
                                    <div class="webkit-box">Date received: ${data.data.school.submit_date}</div>
                                    <div class="webkit-box">Serial start: ${data.serials[0].start}</div>
                                    <div class="webkit-box">Serial End:  ${data.serials[0].end}</div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="webkit-box">Institution: ${data.data.school.school_name}</div>
                                    <div class="webkit-box">Project officer: <a href="#" class="underline" data-toggle="tooltip"
                                        title="<img src='/img/DES_Logo.png'>" html="true">

                                            ${ (data.data.school.project_officer) ? data.data.school.project_officer.full_name :""}
                                    </a></div>
                                    <div class="webkit-box">Device ID: ${data.imei}</div>

                                </div>
                            </div>
               `)
                })
            }

            $(document).off("click").on("click",".edit-student",function() {
                $("#edit-student-modal").modal();
                let table_data = table.row($(this).parents('tr')).data();

                $("#fullname").val(table_data.first_name)
                $("#guardian_contact_number").val(table_data.guardian_contact_number)
                $("#card_id").val(table_data.card_id)
                $("#sex").val(table_data.gender)
                const dobDate = moment(new Date(table_data.dob), 'DD-MM-YYYY').format('YYYY-MM-DD')
                $("#dob").val(dobDate)
                $("#school_name").val(table_data.school_name);

                $("#update-student-form").off("submit").on("submit",function (e){
                    e.preventDefault();
                    let formData = new FormData(this);

                    let $_this = $(this);

                    const print_submit_button = $("#print-submit-button");

                    print_submit_button.prop("disabled", true);
                    print_submit_button.text("Loading...")

                    $.ajax({
                        url: `/api/update-student/${table_data.id}`,
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        statusCode: {
                            200: function (){
                                toastr.success('Student Updated Successfully');
                                print_submit_button.removeAttr("disabled")
                                print_submit_button.text("Update Student ID")
                                setTimeout(function (){
                                    toastr.success('Card Regenerated Successfully!');
                                },3500);
                            },
                            500: function () {
                                toastr.error('Student Update failed!');
                                print_submit_button.removeAttr("disabled")

                            },
                            422: function (data) {
                                printErrorMsg(data.responseJSON.errors);
                                print_submit_button.removeAttr("disabled")

                            }
                        },
                    }).then(function (){
                        $("#edit-student-modal").modal("hide");
                        table.draw();
                        $_this.trigger("reset");
                    })
                });

            });
            const schools = $("#school_name");

            function loadSchools(){
                fetch('/api/all-schools')
                    .then(response => {
                        return response.json();
                    })
                    .then(data => {
                        console.log(data);
                        let options = "<option value=''>Select School</option>";

                        data.data.forEach(function (item){
                            options+= `<option value=${item.id}> ${item.school_name} </option>`;
                        })
                        schools.html(options);
                    });
            }

            loadSchools();
        });


    </script>

@endsection

@section("content")

    <main id="js-page-content" role="main" class="page-content">

        <!-- <ol class="breadcrumb page-breadcrumb" id="PrintPanelPageBreadcrumbDate">
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
        </ol> -->


        <div class="demo-v-spacing mb-3 print-panel-page-breadcrumb"  id="PrintPanelPageBreadcrumb">

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('printing') }}">Printing panel</a>
                </li>

                <li class="breadcrumb-item active">Job data table</li>
            </ol>

        </div>

        <div class="row" id="JobPillPanelHTML" >
            <div class="col-xl-12">
                <div id="panel-1" class="panel">

                    <div class="panel-container show">
                        <div class="panel-content row">

                            <div class="col-12">
                                <button class="d-flex align-items-center btn btn-outline-primary mb-3" id="PrintPanelGoBackBtn">
                                    <span class="icon d-flex align-items-center"><i class="fal fa-angle-left" style="font-size: 22px;"></i></span>
                                    Go Back
                                </button>
                                <table id="dataTable-job" class="table table-bordered table-hover table-striped w-100">
                                    <thead class="dataTable-project-officers-table-head">
                                    <tr>
                                        <th>S/N</th>
                                        <th>Card ID No.</th>
                                        <th>Full name</th>
                                        <th>School name</th>
                                        <th>Attribute</th>
                                        <th>Photo</th>
                                        <th>QR</th>
                                        <th>Bio</th>
                                        <th>G.Card</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Card ID No.</th>
                                        <th>Full name</th>
                                        <th>School name</th>
                                        <th>Attribute</th>
                                        <th>Photo</th>
                                        <th>QR</th>
                                        <th>Bio</th>
                                        <th>G.Card</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

{{--        {!! '<img src="data:image/png;base64,' . DNS2D::getBarcodePNG('frederick', 'QRCODE') . '" alt="barcode" width=100px   />' !!}--}}

        @include("partials.printing.printing_modals")
    </main>

@endsection
