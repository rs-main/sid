@extends("layouts.master")

@section("scripts")
    <script src="js/websocket.js"></script>
    <script src="js/google_maps.js"></script>
    <script src="js/control_center.js"></script>
    <script src="js/delivery_center.js"></script>

<!-- Async script executes immediately and must be after any DOM elements used in callback. -->
        <script
{{--            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIwzALxUPNbatRBj3Xi1Uhp0fFzwWNBkE&callback=initMap&libraries=&v=weekly&region=GH"--}}
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLxvQxzcnFc8CH2rFmQWthopHhF1xHsYc&callback=initMap&libraries=&v=weekly&region=GH"
            async
            defer
        ></script>
    <script>
                function geoLocateSchoolOnMap( schoolInfo ) {
                    if($('#geolocateSwitch').prop('checked') == true)
                    google.maps.event.trigger(window.myMapMarkers[schoolInfo.school_code], 'click');
               }


        Dropzone.options.addAppReleaseFile = {
            init: function() {
                this.on("addedfile", function(file) {

                });
            },
            url : "https://staging.api.desafrica.com/sid-app-upload",
            success : function(data, res) {

                $(controlCenterClassObj.jsId.addAppReleaseFile).val(res);
            }

        };

        $(document).ready(function()
        {
            let table;

            controlCenterObj.getSchools()
            .then(data => {

                //console.log(data.data.getSchools.schools, 'schools');

                 table = $('#dataTable-delivery-institutions').DataTable(
                    {
                        initComplete: function() {
                            var datatable_length_text = $('.dataTables_length label').contents().filter(function() {
                                if(this.nodeType === 3) {
                                   if(this.data == ' entries') {
                                        return this.nodeType === 3;
                                   }
                                }
                            }).wrap('<span style="display:none"></style>');

                            var dataTableSelectorProjects = `<div class="d-flex col-auto mb-3">
                                                    <select id="dataTableSelector" class="custom-select form-control" onchange="projectManagementObj.showHideDiv($(this).val())">
                                                        <option selected="" value="#ProjectsDiv">Project name</option>
                                                        <option value="#SchoolsDiv">Schools</option>
                                                        <option value="#PrintPressDiv">Print press</option>
                                                        <option value="#ProjectOfficersDiv">Project Officers</option>
                                                        <option value="#QualityOfficersDiv">Quality Officers</option>

                                                    </select>
                                                </div>`;

                                                var dataTableSelectorRegions = `<div class="col-auto d-flex mb-3">
                                                    <select id="dataTableSelector" class="custom-select form-control" onchange="projectManagementObj.showHideDiv($(this).val())">
                                                        <option selected="" value="#ProjectsDiv">All Regions</option>
                                                        <option value="#SchoolsDiv">Schools</option>
                                                        <option value="#PrintPressDiv">Print press</option>
                                                        <option value="#ProjectOfficersDiv">Project Officers</option>
                                                        <option value="#QualityOfficersDiv">Quality Officers</option>

                                                    </select>
                                                </div>`;

                                                var toggleGeolocation = `<div class="custom-control custom-switch ml-3">
                                                                <input type="checkbox" class="custom-control-input" id="geolocateSwitch" checked>
                                                                <label class="custom-control-label" for="geolocateSwitch">Geolocate</label>
                                                            </div>`;

                                $($('#dataTable-delivery-institutions_wrapper > div.row > div')[1]).removeClass(['col-sm-12', 'col-md-6']).addClass(['col-auto' , 'mb-3', 'ml-auto']);
                                $($('#dataTable-delivery-institutions_wrapper > div.row > div')[0]).removeClass(['col-sm-12', 'col-md-6']).addClass(['col-auto' , 'mb-3']);
                                $($('#dataTable-delivery-institutions_wrapper > div.row')[0]).prepend(dataTableSelectorRegions);
                                $($('#dataTable-delivery-institutions_wrapper > div.row')[0]).prepend(dataTableSelectorProjects);
                                $('#dataTable-delivery-institutions_filter').parent().append(toggleGeolocation);


                        },
                        responsive: true,

                        data : data.data,
                        columns: [
                            { data: 'school_code' },
                            { data: 'school_name' },
                            { data: 'population',searchable: false },
                            { data: 'sid_status',searchable: false },
                            { data: 'location' ,searchable: false },
                            { data: null}

                        ],
                        columnDefs: [
                            {
                                targets: 4,
                                title: 'Contact',
                                orderable: false,

                                render: function(data, type, full, meta)
                                {

                                    return `${data}<br><small class="text-muted">Tel: 0201122663</small>`

                                },
                            },
                            {

                                targets: -1,
                                title: 'Action',
                                orderable: false,

                                render: function(data, type, full, meta) {

                                    var deliveryStatusBtn = '';
                                    var deliveryStatus = 'pending';

                                    if(deliveryStatus == 'pending') {
                                        deliveryStatusBtn = `<button type="button" class="btn btn-primary text-white text-nowrap">Pending</button>
                                                    <button type="button" class="btn btn-primary text-white dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>`;
                                    }

                                    if(deliveryStatus == 'in-transit') {
                                        deliveryStatusBtn = `<button type="button" class="btn btn-warning text-white text-nowrap">In-transit</button>
                                                    <button type="button" class="btn btn-warning text-white dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>`;
                                    }

                                    if(deliveryStatus == 'delivered') {
                                        deliveryStatusBtn = `<button type="button" class="btn btn-success text-white text-nowrap ">Delivered</button>
                                                    <button type="button" class="btn btn-success text-white dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>`;
                                    }



                                    return `<div class="btn-group">
                                                    ${deliveryStatusBtn}
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="javascript:void(0)" onclick="deliveryCenterObj.updateDeliveryStatus({updateFrom: '${deliveryStatus}', updateTo: 'pending',  event: event})">Pending</a>
                                                        <a class="dropdown-item" href="javascript:void(0)" onclick="deliveryCenterObj.updateDeliveryStatus({updateFrom: '${deliveryStatus}', updateTo: 'in-transit',  event: event})">In-transit</a>
                                                        <a class="dropdown-item" href="javascript:void(0)" onclick="deliveryCenterObj.updateDeliveryStatus({updateFrom: '${deliveryStatus}', updateTo: 'delivered',  event: event})">Delivered</a>
                                                    </div>
                                                </div>`;
                                }

                            },
                            {

                                targets: 1,
                                title: 'School Name',
                                orderable: false,

                                render: function(data, type, full, meta)
                                {

                                    return `${data}<br><small class="text-muted">GPS# 00103</small>`

                                },
                            },
                            {

                                targets: 3,
                                title: 'Status',
                                orderable: false,
                                render: function(data, type, full, meta)
                                {
                                    if(data == null)
                                        return ''
                                    else
                                        return `<span class="badge badge-pill project-status ${data} text-white">${commonObj.capitalizeFirstLetter(data)}</span><br><small class="text-muted">01.03.2021</small>`
                                },
                            },
                            {
                                targets: 2,
                                title: 'Population',
                                orderable: false,
                                render: function(data, type, full, meta)
                                {

                                    return data;
                                },
                            }
                        ]
                    });

                    $('#dataTable-delivery-institutions tbody').on('click', 'tr td:not(:last-child)', function () {
                        geoLocateSchoolOnMap(table.row( this ).data() );
                        console.log(table.row(this).data(),"Control Center Delivery");
                    });
            });

            let channel = pusher.subscribe('submission-channel');
            channel.bind('App\\Events\\NewSubmissionEvent', function(data) {
                table.draw();
            });

            $('.project-officer-img').tooltip('show');

            $('.js-thead-colors a').on('click', function()
            {
                var theadColor = $(this).attr("data-bg");
                console.log(theadColor);
                $('#dt-basic-example thead').removeClassPrefix('bg-').addClass(theadColor);
            });

            $('.js-tbody-colors a').on('click', function()
            {
                var theadColor = $(this).attr("data-bg");
                console.log(theadColor);
                $('#dt-basic-example').removeClassPrefix('bg-').addClass(theadColor);
            });

            runDatePicker();

        });

        /**DATE PICKER INPUT**/

        var controls = {
            leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
            rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
        }

        var runDatePicker = function() {
            $('#DatePickerAddAppRelease').datepicker(
                {
                    orientation: "top left",
                    todayHighlight: true,
                    templates: controls
                }).on('changeDate', function(e) {

                $(controlCenterClassObj.jsId.addAppReleaseDate).val(e.format());
            });
        }

        /* TAB 1: UPDATING CHART */
        var data = [],
            totalPoints = 200;
        var getRandomData = function()
        {
            if (data.length > 0)
                data = data.slice(1);

            // do a random walk
            while (data.length < totalPoints)
            {
                var prev = data.length > 0 ? data[data.length - 1] : 50;
                var y = prev + Math.random() * 10 - 5;
                if (y < 0)
                    y = 0;
                if (y > 100)
                    y = 100;
                data.push(y);
            }

            // zip the generated y values with the x values
            var res = [];
            for (var i = 0; i < data.length; ++i)
                res.push([i, data[i]])
            return res;
        }
        // setup control widget
        var updateInterval = 1500;
        $("#updating-chart").val(updateInterval).change(function()
        {

            var v = $(this).val();
            if (v && !isNaN(+v))
            {
                updateInterval = +v;
                $(this).val("" + updateInterval);
            }

        });
        // setup plot
        var options = {
            colors: ['#6FB3E6'],
            series:
                {
                    lines:
                        {
                            show: true,
                            lineWidth: 0.5,
                            fill: 0.9,
                            fillColor:
                                {
                                    colors: [
                                        {
                                            opacity: 0.9,
                                        },
                                        {
                                            opacity: 0
                                        }]
                                },
                        },

                    shadowSize: 0 // Drawing is faster without shadows
                },
            grid:
                {
                    borderColor: 'rgba(0,0,0,0.05)',
                    borderWidth: 1,
                    labelMargin: 5
                },
            xaxis:
                {
                    color: '#F0F0F0',
                    tickColor: 'rgba(0,0,0,0.05)',
                    font:
                        {
                            size: 10,
                            color: '#999'
                        }
                },
            yaxis:
                {
                    min: 0,
                    max: 100,
                    color: '#F0F0F0',
                    tickColor: 'rgba(0,0,0,0.05)',
                    font:
                        {
                            size: 10,
                            color: '#999'
                        }
                }
        };
        var plot = $.plot($("#updating-chart"), [getRandomData()], options);
        /* live switch */
        $('input[type="checkbox"]#start_interval').click(function()
        {
            if ($(this).prop('checked'))
            {
                $on = true;
                updateInterval = 1500;
                update();
            }
            else
            {
                clearInterval(updateInterval);
                $on = false;
            }
        });
        var update = function()
        {
            if ($on == true)
            {
                plot.setData([getRandomData()]);
                plot.draw();
                setTimeout(update, updateInterval);

            }
            else
            {
                clearInterval(updateInterval)
            }

        }

    </script>

@endsection

@section("content")

    <main id="js-page-content" role="main" class="page-content">

        <ol class="breadcrumb page-breadcrumb">
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
        </ol>

        <div class="subheader">
            <h1 class="subheader-title">
                <i class='subheader-icon fal fa-globe'></i> Control Center for Deliveries
                <!--<small>
                    Insert page description or punch line
                </small>
                -->
            </h1>
            <!--
            Right content on content header
            A nice area to add graphs or buttons
            <div class="subheader-block">
                Right Subheader Block
            </div>
             -->
        </div>
        <!--
        <div class="alert alert-primary">
            <div class="d-flex flex-start w-100">
                <div class="mr-2 hidden-md-down">
                    <span class="icon-stack icon-stack-lg">
                        <i class="base base-6 icon-stack-3x opacity-100 color-primary-500"></i>
                        <i class="base base-10 icon-stack-2x opacity-100 color-primary-300 fa-flip-vertical"></i>
                        <i class="ni ni-blog-read icon-stack-1x opacity-100 color-white"></i>
                    </span>
                </div>
                <!--
                <div class="d-flex flex-fill">
                    <div class="flex-fill">
                        <span class="h5">Pro Tip!</span>
                        <p>
                            If you don't know where to start, this is a good page to start your application. It comes with the basics to get you started. Contains a good inline documentation and waypoints to guide you with your project. Use this area of the page as an attention grabber. Users are likely to respond or pay attention when you involve a color icon along with your information (as displayed here on the left).
                        </p>
                        <p class="m-0">
                            Follow a slogal with a useful link or call to action <a href="#" target="_blank">Call to action >></a>
                        </p>
                    </div>
                </div>

            </div>
        </div>
        -->
        <!-- Your main content goes below here: -->
        <div class="row">
            <div class="col-xl-12">
                <div id="panel-1" class="panel">
                    <div class="panel-hdr">
                        <h2>
                            Progress Map
                        </h2>

                    </div>
                    <div class="panel-container show">
                        <div class="panel-content row">
                            <div class="col-md-9">
                                <div class="d-flex justify-content-center align-items-center google-maps-container" id="google-maps-container"></div>
                            </div>
                            <div class="col-md-3">
                                <div class="google-maps-legends">
                                    <h2>Key</h2>

                                    <div class="d-flex justify-content-center align-items-center">
                                        <div class="legend-dot d-inline-block target"></div>
                                        <div class="d-inline-block flex-grow-1 ml-3">
                                            <div class="d-flex">
                                                Target
                                                <span id="TargetSpan" class="d-inline-block ml-auto">0</span>
                                            </div>
                                            <div class="progress progress-sm mb-3">
                                                <div id="TargetProgress" class="progress-bar target bg-fusion-400" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-center align-items-center">
                                        <div class="legend-dot started d-inline-block"></div>
                                        <div class="d-inline-block flex-grow-1 ml-3">
                                            <div class="d-flex">
                                                Started
                                                <span id="StartedSpan" class="d-inline-block ml-auto">0</span>
                                            </div>
                                            <div class="progress progress-sm mb-3">
                                                <div id="StartedProgress" class="progress-bar started bg-fusion-400" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0"></div>
                                            </div>
                                        </div>
                                    </div>




                                    <div class="d-flex justify-content-center align-items-center">
                                        <div class="legend-dot submitted d-inline-block"></div>
                                        <div class="d-inline-block flex-grow-1 ml-3">
                                            <div class="d-flex">
                                                Submitted
                                                <span id="SubmittedSpan" class="d-inline-block ml-auto">0</span>
                                            </div>
                                            <div class="progress progress-sm mb-3">
                                                <div id="SubmittedProgress" class="progress-bar submitted bg-fusion-400" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-center align-items-center">
                                        <div class="legend-dot printed d-inline-block"></div>
                                        <div class="d-inline-block flex-grow-1 ml-3">
                                            <div class="d-flex">
                                                Printed
                                                <span id="PrintedSpan" class="d-inline-block ml-auto">0</span>
                                            </div>
                                            <div class="progress progress-sm mb-3">
                                                <div id="PrintedProgress" class="progress-bar printed bg-fusion-400" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-center align-items-center">
                                        <div class="legend-dot delivered d-inline-block"></div>
                                        <div class="d-inline-block flex-grow-1 ml-3">
                                            <div class="d-flex">
                                                Delivered
                                                <span id="DeliveredSpan" class="d-inline-block ml-auto">0</span>
                                            </div>
                                            <div class="progress progress-sm mb-3">
                                                <div id="DeliveredProgress" class="progress-bar delivered bg-fusion-400" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="solid">
                                <div class="project-completion-chats row">
                                    <div class="col-6">
                                        <div class="mb-3">Overall <br>Completion</div>
                                        <div id="OverallCompletion" class="js-easy-pie-chart overall-completion color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="23" data-piesize="50" data-linewidth="5" data-linecap="butt" data-scalelength="0">
                                            <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-lg">
                                                <span class="js-percent d-block text-dark"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="mb-3">Active <br> Officers</div>
                                        <div id="ActiveOfficers" class="js-easy-pie-chart active-officers color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="75" data-piesize="50" data-linewidth="5" data-linecap="butt" data-scalelength="0">

                                            <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-lg">
                                                <span class="js-percent d-block text-dark"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div id="panel-1" class="panel">
                    <div class="panel-hdr">
                        <h2>
                            Delivery Institutions
                        </h2>

                    </div>
                    <div class="panel-container show">
                        <div class="panel-content row">

                            <div class="col-12">
                                <table id="dataTable-delivery-institutions" class="table table-bordered table-hover table-striped w-100">
                                    <thead>
                                    <tr>
                                        <th>School ID</th>
                                        <th>School Name</th>
                                        <th>Population</th>
                                        <th>Status</th>
                                        <th>Contact</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>


                                    </tbody>

                                    <tfoot>
                                    <tr>
                                        <th>School ID</th>
                                        <th>School Name</th>
                                        <th>Population</th>
                                        <th>Status</th>
                                        <th>Contact</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
