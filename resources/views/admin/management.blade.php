@extends("layouts.master")

@section("scripts")
{{--    <script src="js/project_management.js"></script>--}}
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyBLxvQxzcnFc8CH2rFmQWthopHhF1xHsYc"></script>
    <script>

        const print_error_msg       = $(".print-error-msg");

        function initialize() {
            var autocompleteInput = document.getElementsByClassName('google-autocomplete');

            for (i = 0; i < autocompleteInput.length; i++) {
                autocomplete = new google.maps.places.Autocomplete(autocompleteInput[i]);
                autocomplete.addListener("place_changed", getLatLng);
            }
        }

        function getLatLng() {

            const place = autocomplete.getPlace();

            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();

            projectManagementClassObj.jsData.location.lat = lat;
            projectManagementClassObj.jsData.location.lng = lng;

        }

        google.maps.event.addDomListener(window, 'load', initialize);

        Dropzone.options.dropzoneCardFrontImage = {
            init: function() {
                this.on("addedfile", function(file) {
                    projectManagementClassObj.jsData.bgImg = file;
                });
            },

        };

        $(document).ready(function()
        {
            window.dataTables = {};
            window.dataTables.projectOfficers = false;
            window.dataTables.qualityOfficers = false;
            window.dataTables.printPress = false;
            window.dataTables.schools = false;


            $('#DatePickerNewProjectEndDate').datepicker(
                {
                    orientation: "top left",
                    todayHighlight: true,
                    templates: controls,
                    autoclose: true,

                }).on('changeDate', function(e) {
                   $(projectManagementClassObj.jsId.newProjectDurationEndDate).val(e.format());
                });

            $('#DatePickerNewProjectStartDate').datepicker(
                {
                    orientation: "top left",
                    todayHighlight: true,
                    templates: controls,
                    autoclose: true,

                }).on('changeDate', function(e) {
                   $(projectManagementClassObj.jsId.newProjectDurationStartDate).val(e.format());
                });


            $('#DatePickerAssignProjectOfficerStartDate').datepicker(
                {
                    orientation: "top left",
                    todayHighlight: true,
                    templates: controls,
                    autoclose: true,

                }).on('changeDate', function(e) {
                $(projectManagementClassObj.jsId.assignProjectOfficerStartDate).val(e.format());
            });

            $('#DatePickerAssignProjectOfficerEndDate').datepicker(
                {
                    orientation: "top left",
                    todayHighlight: true,
                    templates: controls,
                    autoclose: true,

                }).on('changeDate', function(e) {
                $(projectManagementClassObj.jsId.assignProjectOfficerEndDate).val(e.format());
            });


            $('#addProjectModal').on('show.bs.modal', function (e) {
                //commonObj.getInstitutionType();
                var objData = [
                    {
                        id: 399,
                        name: "optin1"
                    },
                    {
                        id: 399,
                        name: "optin2"
                    },

                ]

                commonObj.createSelectOptions(projectManagementClassObj.jsId.newInstitutionType ,objData);
                commonObj.createSelectOptions(projectManagementClassObj.jsId.newCardSize, objData);
            });

            $('#addProjectOfficerModal').on('show.bs.modal', function (e) {
                    commonObj.getProjects().then(data => {
                        //console.log(data.data.getProjects.projects)
                        commonObj.createSelectOptions(projectManagementClassObj.jsId.newProjectOfficerAssignedProject , data.data.getProjects.projects);
                    });
                });

            $(function()
            {
                $('.select2').select2();

                $(".select2-placeholder-multiple").select2(
                    {
                        placeholder: "Select State"
                    });
                $(".js-hide-search").select2(
                    {
                        minimumResultsForSearch: 1 / 0
                    });
                $(".js-max-length").select2(
                    {
                        maximumSelectionLength: 2,
                        placeholder: "Select maximum 2 items"
                    });
                $(".select2-placeholder").select2(
                    {
                        placeholder: "Select a state",
                        allowClear: true
                    });

                $(".js-select2-icons").select2(
                    {
                        minimumResultsForSearch: 1 / 0,
                        templateResult: icon,
                        templateSelection: icon,
                        escapeMarkup: function(elm)
                        {
                            return elm
                        }
                    });

                function icon(elm)
                {
                    elm.element;
                    return elm.id ? "<i class='" + $(elm.element).data("icon") + " mr-2'></i>" + elm.text : elm.text
                }

            });

            projectManagementObj.getProjects()
                .then(data => {

                    //console.log(data, "projects");

                    if(data.data.getProjects == null) {
                        data.data.getProjects = {};

                        data.data.getProjects.projects = {}
                    }

                    $('#dataTable-projects').dataTable(
                        {
                            initComplete: function() {
                                window.dataTables.projects = true;

                                var datatable_length_text = $('.dataTables_length label').contents().filter(function() {
                                if(this.nodeType === 3) {
                                   if(this.data == ' entries') {
                                        return this.nodeType === 3;
                                   }
                                }
                            }).wrap('<span style="display:none"></style>');

                                var dataTableSelectorProjects = `<div class="col-md-4 d-flex mb-3">
                                                    <label class="form-label d-flex align-self-center whitespace-nowrap d-inline pr-3">Table for</label>
                                                    <select id="dataTableSelector" class="custom-select form-control" onchange="projectManagementObj.showHideDiv($(this).val())">
                                                        <option selected="" value="#ProjectsDiv">Projects</option>
                                                        <option value="#SchoolsDiv">Schools</option>
                                                        <option value="#PrintPressDiv">Print press</option>
                                                        <option value="#ProjectOfficersDiv">Project Officers</option>
                                                        <option value="#QualityOfficersDiv">Quality Officers</option>

                                                    </select>
                                                </div>`;

                                $($('#dataTable-projects_wrapper > div.row > div')[1]).removeClass('col-md-6').addClass('col-md-3  mb-3');
                                $($('#dataTable-projects_wrapper > div.row > div')[0]).removeClass('col-md-6').addClass('col-md-5  mb-3');
                                $($('#dataTable-projects_wrapper > div.row')[0]).prepend(dataTableSelectorProjects);
                            },
                            responsive: true,
                            data : data.data.getProjects.projects,
                            columns: [
                                { data: 'name' },
                                { data: 'id' },
                                { data: 'startDate' },
                                { data: null }
                            ],
                            columnDefs: [
                                {
                                    targets: 0,
                                    title: 'Project Name',
                                    responsive: true,
                                    orderable: false,
                                    render: function(data, type, full, meta)
                                    {
                                        return `${data}`;
                                    },
                                },
                                {
                                    targets: -1,
                                    title: 'Actions',
                                    responsive: true,
                                    orderable: false,
                                    render: function(data, type, full, meta)
                                    {

                                        return "\n\t\t\t\t\t\t<div class='d-flex demo'>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-outline-success btn-inline-block mr-1' title='Delete Record'><i class=\"fal fa-eye\"></i></a>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-outline-warning btn-inline-block mr-1' title='Edit'><i class=\"fal fa-edit btn-white-icon\"></i></a>\n\t\t\t\t\t\t\t<div class='dropdown d-inline-block'>\n\t\t\t\t\t\t\t\t<a href='#' class='btn btn-sm btn-outline-danger' data-toggle='dropdown' aria-expanded='true' title='More options'><i class=\"fal fa-times\"></i></a>\n\t\t\t\t\t\t\t\t<div class='dropdown-menu'>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Change Status</a>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Generate Report</a>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>";
                                    },
                                }],
                        });


                }).then(data => {
            });

            $('.js-thead-colors a').on('click', function()
            {
                var theadColor = $(this).attr("data-bg");
                console.log(theadColor);
                $('#dt-basic-example thead').removeClassPrefix('bg-').addClass(theadColor);
            });

            $('.js-tbody-colors a').on('click', function()
            {
                var theadColor = $(this).attr("data-bg");
                console.log(theadColor);
                $('#dt-basic-example').removeClassPrefix('bg-').addClass(theadColor);
            });
        });


        $("#addNewProjectForm").on("submit",function (e){
            e.preventDefault();
            // let form_data = $(this).serialize();
            let formData = new FormData(this);
            let $_this = $(this);

            console.log(formData)

            $.ajax({
                url: '/api/add-new-project',
                type: "POST",
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                statusCode: {
                    200: function (){
                        toastr.success('Project Added Successfully');
                        print_error_msg.css("display","none");
                    },
                    500: function () {
                        toastr.error('Project creating failed!');
                    },
                    422: function (data) {
                        printErrorMsg(data.responseJSON.errors);
                    }
                },
            }).then(function (){
                $("#addNewProjectForm").modal("hide");
                table.draw();
                loadProjects();
                $_this.trigger("reset");
            })
        });

        function printErrorMsg (msg) {
            print_error_msg.find("ul").html('');
            print_error_msg.css('display','block');
            $.each( msg, function( key, value ) {
                console.log("error key " + key)
                print_error_msg.find("ul").append('<li>'+value+'</li>');
                $("."+key).text(value);
            });
        }

        function loadOfficers(){
            fetch('/api/project-officers')
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    console.log(data);
                    let options = "<option value=''>Select Officer</option>";

                    data.data.forEach(function (item){
                        options+= `<option value=${item.id}> ${item.name} </option>`;
                    })
                    $("#officers").html(options);

                });
        }

        function loadProjects(){
            fetch('/api/projects')
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    console.log(data);
                    let options = "<option value=''>Select Project</option>";

                    data.data.forEach(function (item){
                        options+= `<option value=${item.id}> ${item.name} </option>`;
                    })
                    $("#projects").html(options);

                });
        }

        $("#assignProject").on("submit",function (e){
            e.preventDefault();
            // let form_data = $(this).serialize();
            let formData = new FormData(this);
            let $_this = $(this);

            console.log(formData)

            $.ajax({
                url: `/api/add-new-project-officer`,
                type: "POST",
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                statusCode: {
                    200: function (){
                        toastr.success('Project Officer Added Successfully');
                        print_error_msg.css("display","none");
                    },
                    500: function () {
                        toastr.error('Failed!');
                    },
                    422: function (data) {
                        printErrorMsg(data.responseJSON.errors);
                        toastr.warning('Fill all required fields!');

                    }
                },
            }).then(function (){
                $("#addProjectOfficerModal").modal("hide");
                // table.draw();
                $_this.trigger("reset");
            })
        });

        $.fn.dataTable.ext.errMode = 'none';
        const table = $('#dataTable-projects').DataTable({
                initComplete: function() {
                    var datatable_length_text = $('.dataTables_length label').contents().filter(function() {
                        if(this.nodeType === 3) {
                            if(this.data == ' entries') {
                                return this.nodeType === 3;
                            }
                        }
                    }).wrap('<span style="display:none"></style>');
                },
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: "{{ route('projects') }}",
                columns: [
                    {data: 'project_name', name: 'project_name',orderable:false},
                    {data: 'project_no', name: 'project_no'},
                    {data: 'officers_assigned', name: 'officers_assigned'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions'},
                ],
                columnDefs: [
                    {
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        render: function (data, type, full, meta) {
                            return "\n\t\t\t\t\t\t<div class='d-flex demo'>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-outline-success btn-icon btn-inline-block mr-1' title='Delete Record'><i class=\"fal fa-eye\"></i></a>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-outline-warning btn-icon btn-inline-block mr-1' title='Edit'><i class=\"fal fa-edit btn-white-icon\"></i></a>\n\t\t\t\t\t\t\t<div class='dropdown d-inline-block'>\n\t\t\t\t\t\t\t\t<a href='#' class='btn btn-sm btn-outline-danger btn-icon' data-toggle='dropdown' aria-expanded='true' title='More options'><i class=\"fal fa-times\"></i></a>\n\t\t\t\t\t\t\t\t<div class='dropdown-menu'>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Change Status</a>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Generate Report</a>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>";
                        },
                    }
              ]
            });

        loadOfficers();
        loadProjects();

    </script>

@endsection

@section("content")

    <main id="js-page-content" role="main" class="page-content">

        <ol class="breadcrumb page-breadcrumb">
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
        </ol>

        <div class="subheader">
            <h1 class="subheader-title">
                <i class='subheader-icon fal fa-id-card'></i> Management - <span class="text-muted" id="management-page-header">Projects</span>
                <!-- <small>
                    Insert page description or punch line
                </small>
                -->
            </h1>

        </div>

        <div class="row d-flex align-items-center pb-4">
            <div class="col-auto mb-3">Add new</div>
            <a data-toggle="modal" data-target="#addProjectModal" class="col-lg col-md-3 col-sm-3 mb-3"><button class="btn btn-primary w-100"><span class="icon"><i class="fal fa-plus"></i></span>Project</button></a>
            <a data-toggle="modal" data-target="#addSchoolModal" class="col-lg col-md-3 col-sm-3 mb-3"><button class="btn btn-primary w-100"><span class="icon"><i class="fal fa-plus"></i></span>School</button></a>
            <a data-toggle="modal" data-target="#addPrintPressModal" class="col-lg col-md-3 col-sm-3 mb-3"><button class="btn btn-primary w-100"><span class="icon"><i class="fal fa-plus"></i></span>Print Press</button></a>
            <a data-toggle="modal" data-target="#addProjectOfficerModal" class="col-lg col-md-3 col-sm-3 mb-3"><button class="btn btn-primary w-100"><span class="icon"><i class="fal fa-plus"></i></span>Project Officer</button></a>
            <div class="col-lg col-md-3 col-sm-3 mb-3"><button class="btn btn-primary w-100"><span class="icon"><i class="fal fa-plus"></i></span>Print Officer</button></div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div id="panel-1" class="panel">
                    <div class="panel-container show">
                        <div class="panel-content" id="panelHtml">

                            <div id="ProjectsDiv" style="">
                                <table id="dataTable-projects" class="table table-bordered table-hover table-striped w-100">
                                    <thead>
                                    <tr>
                                        <th>Project Name</th>
                                        <th>Project No.</th>
                                        <th>Officers Assigned</th>
                                        <th>Date Created</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Project Name</th>
                                        <th>Project No.</th>
                                        <th>Officers Assigned</th>
                                        <th>Date Created</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <!-- Add Project Modal -->
                            <div class="modal fade" id="addProjectModal" tabindex="-1" role="dialog"
                                 aria-labelledby="addSchoolModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Add new project</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-body">

                                            <div class="alert alert-danger print-error-msg" style="display:none">
                                                <ul></ul>
                                            </div>

                                            <form id="addNewProjectForm" enctype="multipart/form-data" method="POST">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Project name</label>
                                                    <input type="text" class="form-control" id="NewProjectName" name="name" aria-describedby="nameHelp"
                                                           placeholder="Project Name">
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Card title</label>
                                                    <input type="text" class="form-control" name="card_title" id="NewCardTitle" placeholder="Card title">
                                                </div>

                                                <div class="form-group w-50">
                                                    <label for="exampleInputPassword1" class="d-block">Institution type</label>
                                                    <select class="custom-select" aria-label="Disabled select example" id="NewInstitutionType" name="institution_type">
                                                        <option selected>Schools</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Field</label>
                                                    <div class="d-block">
                                                        <div class="d-flex">

                                                            <div class="form-check mr-3 col">
                                                                <input class="form-check-input" type="checkbox" value="First Name" id="NewFirstName" name="field[]">
                                                                <label class="form-check-label" for="flexCheckDefault">
                                                                    First name
                                                                </label>
                                                            </div>

                                                            <div class="form-check mr-3 col">
                                                                <input class="form-check-input" type="checkbox" value="Second Name" id="NewSecondName" name="field[]">
                                                                <label class="form-check-label" for="flexCheckDefault">
                                                                    Second name
                                                                </label>
                                                            </div>

                                                            <div class="form-check mr-3 col">
                                                                <input class="form-check-input" type="checkbox" value="Middle Name" id="NewMiddleName" name="field[]">
                                                                <label class="form-check-label" for="flexCheckDefault">
                                                                    Middle name
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <br>

                                                        <div class="d-flex">

                                                            <div class="form-check mr-3 col">
                                                                <input class="form-check-input" type="checkbox" value="Issue Date" id="NewIssueDate" name="field[]">
                                                                <label class="form-check-label" for="flexCheckDefault">
                                                                    Issue date
                                                                </label>
                                                            </div>

                                                            <div class="form-check mr-3 col">
                                                                <input class="form-check-input" type="checkbox" value="ID No." id="NewIdNo" name="field[]" >
                                                                <label class="form-check-label" for="flexCheckDefault">
                                                                    ID No.
                                                                </label>
                                                            </div>


                                                            <div class="form-check mr-3 col">
                                                                <input class="form-check-input" type="checkbox" value="QR Code" id="NewQrCode" name="field[]">
                                                                <label class="form-check-label" for="flexCheckDefault">
                                                                    QR Code
                                                                </label>
                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <label>Project Duration</label>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="input-group">
                                                                <input type="datetime-local" class="form-control " placeholder="Start date" id="DatePickerNewProjectStartDate"
                                                                       name="project_start_date">
                                                                <div class="input-group-append">
                                                                                    <span class="input-group-text fs-xl">
                                                                                        <i class="fal fa-calendar-alt"></i>
                                                                                    </span>
                                                                </div>
                                                                <input type="hidden" id="NewProjectDurationStartDate">
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="input-group">
                                                                <input type="datetime-local" class="form-control " placeholder="End date" id="DatePickerNewProjectEndDate"
                                                                       name="project_end_date">
                                                                <div class="input-group-append">
                                                                                    <span class="input-group-text fs-xl">
                                                                                        <i class="fal fa-calendar-alt"></i>
                                                                                    </span>
                                                                </div>
                                                                <input type="hidden" id="NewProjectDurationEndDate">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group w-50">
                                                    <label for="exampleInputPassword1" class="d-block">Card size</label>
                                                    <select class="custom-select" aria-label="Disabled select example" id="NewProjectCardSize" name="card_size">
                                                        <option selected value="5x3">5 x 3 inches</option>
                                                        <option value="4x1">4 x 1 inches</option>
                                                        <option value="3x1">3 x 1 inches</option>
                                                        <option value="2x1">2 x 1 inches</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Card Background image upload</label>
                                                    <input type="file" class="form-control" name="image" id="image">
                                                </div>

{{--                                                <div class="form-group mt-5">--}}
{{--                                                    <label>Card Background image upload</label>--}}

{{--                                                    <div class="dropzone needsclick" id="new-project-card-background-image" style="min-height: 7rem; background-color: white;">--}}
{{--                                                        <div class="dz-message needsclick">--}}
{{--                                                            <span class="fs-sm text-muted">Drop card artwork here</span>--}}
{{--                                                        </div>--}}
{{--                                                        <input type="hidden" id="NewProjectCardBackImg">--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary">Create project</button>
                                                    <button type="button" class="btn btn-secondary ml-auto" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </form>


{{--                                            <div class="form-group mt-5">--}}
{{--                                                <label>Card Background image upload</label>--}}

{{--                                                <div class="dropzone needsclick" id="new-project-card-background-image" style="min-height: 7rem; background-color: white;">--}}
{{--                                                    <div class="dz-message needsclick">--}}
{{--                                                        <span class="fs-sm text-muted">Drop card artwork here</span>--}}
{{--                                                    </div>--}}
{{--                                                    <input type="hidden" id="NewProjectCardBackImg">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

                                        </div>
{{--                                        <div class="modal-footer">--}}
{{--                                            <button type="submit" class="btn btn-primary">Create project</button>--}}
{{--                                            <button type="button" class="btn btn-secondary ml-auto" data-dismiss="modal">Cancel</button>--}}
{{--                                        </div>--}}
                                    </div>
                                </div>
                            </div>

                            <div id="SchoolsDiv" style="display: none;">
                                <table id="dataTable-schools" class="table table-bordered table-hover table-striped w-100">
                                    <thead class="dataTable-schools-table-head">
                                    <tr>
                                        <th>School Name</th>
                                        <th>Population Count</th>
                                        <th>Assign Officer</th>
                                        <th>Start date</th>
                                        <th>Completed</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>

                                        </td>

                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>School Name</th>
                                        <th>Population Count</th>
                                        <th>Assign Officer</th>
                                        <th>Start date</th>
                                        <th>Completed</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <!-- Add School Modal -->
                            <div class="modal fade" id="addSchoolModal" tabindex="-1" role="dialog" aria-labelledby="addSchoolModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Add new School</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Institution name</label>
                                                    <input type="text" class="form-control" id="NewSchoolName" aria-describedby="newSchoolName" placeholder="">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Population</label>
                                                    <input type="text" class="form-control" id="NewSchoolPopulation" placeholder="">
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="button" class="btn btn-primary">Create School</button>
                                            <button type="button" class="btn btn-secondary ml-auto" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Assign Project Officer Modal -->
                            <div class="modal fade" id="assignProjectOfficerModal" tabindex="-1" role="dialog" aria-labelledby="assignProjectOfficerModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Assign Project Officer</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="mb-3" id="AssignProjectOfficerSchoolNameHTML"></div>
                                            <form>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1" class="d-block">Officer name</label>
                                                    <select class="custom-select" aria-label="Disabled select example" id="AssignProjectOfficerNameSelect">
                                                        <option selected></option>
                                                    </select>
                                                </div>
                                                <input type="hidden" value="" id="AssignProjectOfficerSchoolCode">
                                            </form>
                                        </div>
                                        <div class="modal-footer mb-3">

                                            <button type="button" class="btn btn-primary m-auto" data-dismiss="modal" onclick="projectManagementObj.assignInstitutionToPO()">Assign</button>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div id="ProjectOfficersDiv" style="display: none;">

                                <table id="dataTable-project-officers" class="table table-bordered table-hover table-striped" style="width: 100%;">
                                    <thead class="dataTable-project-officers-table-head">
                                    <tr>
                                        <th>Project Officer name</th>
                                        <th>Assign Project ID</th>
                                        <th>Assign Schools</th>
                                        <th>Start date</th>
                                        <th>Inst. Engaged</th>
                                        <th>Total Submits</th>
                                        <th>Task Duration</th>
                                        <th>Date Added</th>
                                        <th>ID Type</th>
                                        <th>ID No.</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>

                                        </td>

                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Project Officer name</th>
                                        <th>Assign Project ID</th>
                                        <th>Assign Schools</th>
                                        <th>Start date</th>
                                        <th>Inst. Engaged</th>
                                        <th>Total Submits</th>
                                        <th>Task Duration</th>
                                        <th>Date Added</th>
                                        <th>ID Type</th>
                                        <th>ID No.</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>


                            <!-- Add Task to Project Officer Name Modal -->
                            <div class="modal fade" id="addTaskToProjectOfficerNameModal" role="dialog" aria-labelledby="assignProjectOfficerModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Add task to 'Project Officer name'</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <form>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1" class="d-block">Institution name</label>
                                                    <select class="select2" id="AssignProjectOfficerInstSelect" aria-label="select example">
                                                        <option selected>type name or institution code</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1" class="d-block">Task Duration</label>
                                                    <div class="d-flex">
                                                        <div class="input-group mr-3">
                                                            <input type="text" class="form-control datepicker" placeholder="Start date" id="DatePickerAssignProjectOfficerStartDate">
                                                            <input type="hidden"  value="" id="AssignProjectOfficerStartDate">
                                                            <div class="input-group-append">
                                                                                    <span class="input-group-text fs-xl">
                                                                                        <i class="fal fa-calendar-alt"></i>
                                                                                    </span>
                                                            </div>
                                                        </div>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control datepicker" placeholder="End date" id="DatePickerAssignProjectOfficerEndDate">
                                                            <input type="hidden"  value="" id="AssignProjectOfficerEndDate">
                                                            <div class="input-group-append">
                                                                                    <span class="input-group-text fs-xl">
                                                                                        <i class="fal fa-calendar-alt"></i>
                                                                                    </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <input type="hidden" value="" id="AssignProjectOfficerId">


                                            </form>
                                        </div>
                                        <div class="modal-footer mb-3">

                                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="projectManagementObj.addTaskToProjectOfficerName()">Assign Task</button>
                                            <button type="button" class="btn btn-secondary ml-auto" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div id="PrintPressDiv" style="display: none;">
                                <table id="dataTable-print-press" class="table table-bordered table-hover table-striped w-100">
                                    <thead>
                                    <tr>
                                        <th>Press Name</th>
                                        <th>Assigned Officer</th>
                                        <th>Date Added</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Press Name</th>
                                        <th>Assigned Officer</th>
                                        <th>Date Added</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <!-- Add new Press Modal -->
                            <div class="modal fade" id="addPrintPressModal" tabindex="-1" role="dialog" aria-labelledby="addPrintPressModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Add new Press</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Press name</label>
                                                    <input type="text" class="form-control" id="NewPressName" aria-describedby="emailHelp" placeholder="">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">GPS code</label>
                                                    <input type="text" class="form-control" id="NewPressGpsCode" placeholder="Ghana post GPS">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Location</label>
                                                    <input type="text" class="form-control google-autocomplete" placeholder="type location">
                                                </div>






                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Contact Name</label>
                                                    <input type="text" class="form-control" id="NewPressContactName" placeholder="">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Contact No.</label>
                                                    <input type="text" class="form-control" id="NewPressContactNo" placeholder="">
                                                </div>





                                            </form>

                                        </div>
                                        <div class="modal-footer">

                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-primary ml-auto" data-dismiss="modal" onclick="projectManagementObj.addPress()">Create Press</button>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <!-- Assign Print Officer Modal -->
                            <div class="modal fade" id="assignPrintOfficerModal" tabindex="-1" role="dialog" aria-labelledby="assignProjectOfficerModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Assign Print Officer</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="mb-3" id="AssignPrintOfficerPressNameHTML"></div>
                                            <form>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1" class="d-block">Officer name</label>
                                                    <select class="select2 form-control custom-select" aria-label="select example" id="AssignPrintOfficerNameSelect">
                                                        <option selected>type Name or ID</option>
                                                    </select>
                                                </div>
                                                <input type="hidden" id="AssignPrintOfficerPressId">
                                            </form>
                                        </div>
                                        <div class="modal-footer mb-3">

                                            <button type="button" class="btn btn-primary m-auto" data-dismiss="modal" onclick="projectManagementObj.assignOfficerToPress()">Assign</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Add Project Officer Modal -->
                            <div class="modal fade" id="addProjectOfficerModal" tabindex="-1" role="dialog" aria-labelledby="addProjectOfficerModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
{{--                                            <h5 class="modal-title" id="exampleModalLabel">Add new Project Officer</h5>--}}
                                            <h5 class="modal-title" id="exampleModalLabel">Add Project Officer</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <div id="errors" class="alert alert-danger print-error-msg" style="display:none">
                                                <ul></ul>
                                            </div>

                                            <form id="assignProject" method="POST" enctype="multipart/form-data">

                                                <div class="form-group">
                                                    <label for="fullname">Name</label>
                                                    <input type="text" class="form-control" name="name" id="fullname" required placeholder="Name">
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Email</label>
                                                    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Email" name="email">
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Phone</label>
                                                    <input type="tel" class="form-control" id="phone" aria-describedby="phoneHelp" placeholder="Phone" name="phone">
                                                </div>

                                                <div class="form-group">
                                                    <label for="password">Password</label>
                                                    <input type="password" class="form-control" id="password" name="password" placeholder="Type password">
                                                </div>


                                                <div class="form-group">
                                                    <label for="projects">Assign Project</label>
                                                    <select class="form-control" name="project_id" id="projects">
                                                        <option>Select Project</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="project_location">Assigned Project Location</label>
                                                    <input type="text" class="form-control google-autocomplete" id="NewProjectOfficerAssignedProjectLocation"
                                                           name="project_location" placeholder="type location">
                                                </div>

                                                <div class="form-group">
                                                    <label for="address">Officer's Residential Address</label>
                                                    <input type="text" class="form-control" id="NewProjectOfficerResidentialAddress" placeholder="Ghana Post or Street Address" name="address">
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1" class="d-block">Officer's Personal ID</label>
                                                    <div class="d-flex align-items-center">
                                                        <div class="input-group">
                                                            <select class="form-control mr-1" id="NewProjectOfficerIDType" name="id_type">
                                                                <option>ID Type</option>
                                                                <option value="Voters ID">Voters ID</option>
                                                                <option value="Driver License">Driver License</option>
                                                                <option value="Passport">Passport</option>
                                                                <option value="SSNIT">SSNIT</option>
                                                            </select>
                                                        </div>


                                                        <input type="text" class="form-control mr-1" id="NewProjectOfficerIDNo" placeholder="ID No." name="id_no">


                                                        <div class="input-group">

                                                            <input type="date" class="form-control" placeholder="Expiry Date" id="DatePickerNewProjectOfficerIDExpiryDate" name="expiry_date">
{{--                                                            <input type="text" class="form-control" placeholder="Expiry Date" id="DatePickerNewProjectOfficerIDExpiryDate" name="expiry_date">--}}
                                                            <input type="hidden" value="" id="NewProjectOfficerIDExpiryDate">

                                                            <div class="input-group-append">
                                                                            <span class="input-group-text fs-xl">
                                                                                <i class="fal fa-calendar-check"></i>
                                                                            </span>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="modal-footer">

                                                    <button type="submit" class="btn btn-primary">Add Project Officer</button>
                                                    <button type="button" class="btn btn-secondary ml-auto" data-dismiss="modal">Cancel</button>
                                                </div>

                                            </form>
{{--                                            <form action="/upload" id="project-officer-id-card" class="dropzone needsclick mt-5" style="min-height: 7rem; background-color: white;">--}}
{{--                                                <div class="dz-message needsclick">--}}
{{--                                                    <span class="fs-sm text-muted">Drag & drop or Browse for photo</span>--}}
{{--                                                </div>--}}
{{--                                                <input type="hidden" value="" id="NewProjectOfficerIdCardImg">--}}
{{--                                            </form>--}}
                                        </div>
{{--                                        <div class="modal-footer">--}}

{{--                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Assign Project</button>--}}
{{--                                            <button type="button" class="btn btn-secondary ml-auto" data-dismiss="modal">Cancel</button>--}}
{{--                                        </div>--}}
                                    </div>
                                </div>
                            </div>


                            <div id="QualityOfficersDiv" style="display: none;">
                                    <table id="dataTable-quality-officers" class="table table-bordered table-hover table-striped" style="width: 100%;">
                                        <thead class="dataTable-project-officers-table-head">
                                        <tr>
                                            <th>Project Officer name</th>
                                            <th>Assign Project ID</th>
                                            <th>Assign Schools</th>
                                            <th>Start date</th>
                                            <th>Inst. Engaged</th>
                                            <th>Total Submits</th>
                                            <th>Task Duration</th>
                                            <th>Date Added</th>
                                            <th>ID Type</th>
                                            <th>ID No.</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>

                                            </td>

                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Project Officer name</th>
                                            <th>Assign Project ID</th>
                                            <th>Assign Schools</th>
                                            <th>Start date</th>
                                            <th>Inst. Engaged</th>
                                            <th>Total Submits</th>
                                            <th>Task Duration</th>
                                            <th>Date Added</th>
                                            <th>ID Type</th>
                                            <th>ID No.</th>
                                            <th>Actions</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
