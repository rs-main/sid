<script>

    $("#add-new-project-form").on("submit",function (e){
        e.preventDefault();
        // let form_data = $(this).serialize();
        let formData = new FormData(this);
        let $_this = $(this);

        console.log(formData)

        $.ajax({
            url: '/api/add-new-account',
            type: "POST",
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            statusCode: {
                200: function (){
                    toastr.success('Account Added Successfully');
                    print_error_msg.css("display","none");
                },
                500: function () {
                    toastr.error('Account creating failed!');
                },
                422: function (data) {
                    printErrorMsg(data.responseJSON.errors);
                }
            },
        }).then(function (){
            $("#addAccountModal").modal("hide");
            table.draw();
            $_this.trigger("reset");
        })
    });

</script>
