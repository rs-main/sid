
<div class="modal fade" id="edit-student-modal" tabindex="-1" role="dialog" aria-labelledby="editSchoolModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account-label">Edit Student Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <br>
            <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
            </div>
            <form method="post" id="update-student-form" enctype="multipart/form-data">

                <div class="modal-body">
                    <div class="form-group">
                        <label for="fullname">Name</label>
                        <input type="text" class="form-control" name="name" id="fullname" required placeholder="Name">
                    </div>

                    <div class="form-group">
                        <label for="school_name">School Name</label>
                        <input type="text" class="form-control" name="school_id" id="school_name" readonly disabled>
{{--                        <select class="form-control" name="school_id" id="school_name" readonly></select>--}}
                    </div>

                    <div class="form-group">
                        <label for="guardian_contact_number">Guardian Contact No.</label>
                        <input type="tel" class="form-control" id="guardian_contact_number" placeholder="Guardian Contact" name="guardian_contact_number">
                    </div>

                    <div class="form-group">
                        <label for="card_id">ID No.</label>
                        <input type="text" class="form-control" id="card_id" placeholder="ID No." name="card_id" readonly disabled>
                    </div>

                    <div class="form-group">
                        <label for="card_id">Date of Birth</label>
                        <input type="date" class="form-control" id="dob" placeholder="Date of Birth" name="dob">
                    </div>

                    <div class="form-group">
                        <label for="card_id">Sex</label>
                        <select class="form-control" name="sex">
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>




                </div>

                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary ml-auto" id="print-submit-button">Update Student ID</button>

                </div>
            </form>
        </div>

    </div>

</div>
