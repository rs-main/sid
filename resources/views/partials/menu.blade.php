<ul id="js-nav-menu" class="nav-menu">
    @can('control-center')
        <li class="{{ strpos($routeName, 'control-center') !== false ? ' active' : '' }}">
            <a href="{{route("control-center")}}" title="Blank Project" data-filter-tags="blank page">
                <i class="fal fa-globe"></i>
                <span class="nav-link-text" data-i18n="nav.blankpage">Control Center</span>
            </a>
        </li>
    @endcan

    @can('print')
        <li class="{{ strpos($routeName, 'printing') !== false ? ' active' : '' }}">
            <a href="{{route("printing")}}" title="Blank Project" data-filter-tags="blank page">
                <i class="fal fa-print"></i>
                <span class="nav-link-text" data-i18n="nav.blankpage">Printing</span>
            </a>
        </li>
    @endcan

    @can('card-generator')
        <li class="{{ strpos($routeName, 'card-generator') !== false ? ' active' : '' }}">
            <a href="{{route("card-generator")}}" title="Blank Project" data-filter-tags="blank page">
                <i class="fal fa-credit-card"></i>
                <span class="nav-link-text" data-i18n="nav.blankpage">Card Generator</span>
            </a>
        </li>

    @endcan

    @can('management')

        <li class="{{ strpos($routeName, 'management') !== false ? ' active' : '' }}">
            <a href="{{route("management")}}" title="Blank Project" data-filter-tags="blank page">
                <i class="fal fa-id-card"></i>
                <span class="nav-link-text" data-i18n="nav.blankpage">Management</span>
            </a>
        </li>
    @endcan

    @can('databank')

        <li class="{{ strpos($routeName, 'databank') !== false ? ' active' : '' }}">
            <a href="{{route("databank")}}" title="Blank Project" data-filter-tags="blank page">
                <i class="fal fa-shopping-bag"></i>
                <span class="nav-link-text" data-i18n="nav.blankpage">Data Bank</span>
            </a>
        </li>

    @endcan

        @can('control-center-delivery')

            <li class="{{ strpos($routeName, 'deliveries') !== false ? ' active' : '' }}">
                <a href="{{route("deliveries")}}" title="Blank Project" data-filter-tags="blank page">
                    <i class="fal fa-mail-bulk"></i>
                    <span class="nav-link-text" data-i18n="nav.blankpage">Deliveries</span>
                </a>
            </li>

        @endcan

    @can('all')

        <li class="{{ strpos($routeName, 'users') !== false ? ' active' : '' }}">
            <a href="{{route("users")}}" title="Users" data-filter-tags="blank page">
                <i class="fal fa-user"></i>
                <span class="nav-link-text" data-i18n="nav.blankpage">Accounts</span>
            </a>
        </li>

    @endcan

</ul>
