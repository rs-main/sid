<script>
    const button = document.querySelector('#sid-card-link');
    const tooltip = document.querySelector('#sid-card-tooltip');

    const popperInstance = Popper.createPopper(button, tooltip, {
        modifiers: [
            {
                name: 'offset',
                options: {
                    offset: [0, 8],
                },
            },
        ],
    });

    function show() {
        // Make the tooltip visible
        tooltip.setAttribute('data-show', '');

        // Enable the event listeners
        popperInstance.setOptions({
            modifiers: [{ name: 'eventListeners', enabled: true }],
        });

        // Update its position
        popperInstance.update();
    }

    function hide() {
        // Hide the tooltip
        tooltip.removeAttribute('data-show');

        // Disable the event listeners
        popperInstance.setOptions({
            modifiers: [{ name: 'eventListeners', enabled: false }],
        });
    }

    const showEvents = ['mouseenter', 'focus'];
    const hideEvents = ['mouseleave', 'blur'];

    showEvents.forEach(event => {
        button.addEventListener(event, show);
    });

    hideEvents.forEach(event => {
        button.addEventListener(event, hide);
    });

    $.fn.dataTable.ext.errMode = 'none';


</script>
<script>
    /* demo scripts for change table color */
    /* change background */
    $(function() {
        $('[data-toggle="show-sid-card-tooltip"]').tooltip({
            html: true,

        });
    });

    function ImagetoPrint(source="/")
    {
        return "<html><head><scri"+"pt>function step1(){\n" +
            "setTimeout('step2()', 10);}\n" +
            "function step2(){window.print();window.close()}\n" +
            "</scri" + "pt></head><body onload='step1()'>\n" +
            "<img src='" + source + "' /></body></html>";
        // "<img src='" +window.location.origin+'/'+ source + "' /></body></html>";
    }

    function PrintImage(source)
    {
        var Pagelink = "about:blank";
        var pwa = window.open(Pagelink, "_new");
        pwa.document.open();
        pwa.document.write(ImagetoPrint(source));
        pwa.document.close();
    }

    $(document).ready(function()
    {


        let databank_datatable = $('#dataTable-data-bank').DataTable(

            {

                initComplete: function () {
                    var datatable_length_text = $('.dataTables_length label').contents().filter(function() {
                                if(this.nodeType === 3) {
                                   if(this.data == ' entries') {
                                        return this.nodeType === 3;
                                   }
                                }
                            }).wrap('<span style="display:none"></style>');

                    var tableFiltersHTML = `   <div class="pt-4 d-flex align-items-center w-auto flex-wrap">
                                            <div class="col-auto mb-3">Filter by</div>
                                            <div class="col-auto mb-3 flex-grow-1">
                                                <select class="custom-select form-control">
                                                    <option>Year</option>
                                                  </select>
                                            </div>
                                            <div class="col-auto mb-3 flex-grow-1">
                                                <select class="form-control custom-select">
                                                    <option>Project name</option>
                                                  </select>
                                            </div>
                                            <div class="col-auto mb-3 flex-grow-1">
                                                <select class="form-control custom-select">
                                                    <option>Institution name</option>
                                                  </select>
                                            </div>
                                            <div class="col-auto mb-3 flex-grow-1">
                                                <select class="form-control custom-select">
                                                    <option>Gender</option>
                                                  </select>
                                            </div>

                                        </div>`;

                    $('#dataTable-data-bank_wrapper >  .row.mb-3').append(tableFiltersHTML);

                    $(".print-image").on("click",function (){
                        const url = $(this).data("url");
                        location.href = `/api/download-card-image/${url}`;
                    });
                },
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: `/api/students`,
                columns: [
                    {data: 'full_name', name: 'full_name'},
                    {data: 'card_id', name: 'card_id',orderable:false},
                    {data: 'project_name', name: 'project_name'},
                    {data: 'school_name', name: 'school_name'},
                    {data: 'gender', name: 'gender'},
                    {data: 'action', name: 'action'}
                ],
                columnDefs: [
                    {
                        targets : [0,4],
                        orderable : false,
                    },
                    {
                        targets: -1,
                        title: 'Admin Controls',
                        orderable: false,
                        render: function(data, type, full, meta)
                        {
                            const base_url = window.location.origin
                            let url = `/${full.card_id}.png`;
                            let card_id = `${full.card_id}`;
                            let elem = `\n\t\t\t\t\t\t<div class='d-flex demo'>\n\t\t\t\t\t\t\t<a href=${url} target="_blank" class='btn btn-sm btn-outline-success btn-inline-block mr-1' title='View'>
                                <i class=\"fal fa-eye\"></i></a>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' data-url="${card_id}" class='btn btn-sm btn-outline-primary btn-inline-block mr-1 print-image'
                                 title='Print'><i class=\"fal fa-print\"></i></a>\n\t\t\t\t\t\t\t<div class='dropdown d-inline-block'>\n\t\t\t\t\t\t\t\t
<!--                                <a href='#' class='btn btn-sm btn-outline-danger' data-toggle='dropdown' aria-expanded='true' title='More options'><i class=\"fal fa-times\"></i></a>\n\t\t\t\t\t\t\t\t<div class='dropdown-menu'>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Change Status</a>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Generate Report</a>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>-->`;
                            return full.generated ? elem : "<i>Not generated</i>"
                        },
                    },

                    {
                        targets: 1,
                        orderable: false,
                        render: function(data, type, full, meta)
                        {
                            return ` <div id="sid-card-tooltip" role="tooltip"><img src="/${data}.png" style="width: 350px; height: auto;"></div>
                                            <a id="sid-card-link" class="underline" aria-describedby="tooltip">${data}</a>`;
                        },
                    },

                    {
                        targets: 2,
                        orderable: false,
                        render: function(data, type, full, meta)
                        {
                            return `GES SHS 2020 e-card`;
                        },
                    },

                    {
                        targets: 3,
                        orderable: false,
                        /*	The `data` parameter refers to the data for the cell (defined by the
                            `data` option, which defaults to the column being worked with, in this case `data: 16`.*/
                        render: function(data, type, full, meta)
                        {
                            var badge = {
                                1:
                                    {
                                        'title': 'Started',
                                        'class': 'project-status started text-white'
                                    },
                                2:
                                    {
                                        'title': 'Delivered',
                                        'class': 'badge-success'
                                    },
                                3:
                                    {
                                        'title': 'Canceled',
                                        'class': 'badge-secondary'
                                    },
                                4:
                                    {
                                        'title': 'Attempt #1',
                                        'class': 'bg-danger-100 text-white'
                                    },
                                5:
                                    {
                                        'title': 'Attempt #2',
                                        'class': 'bg-danger-300 text-white'
                                    },
                                6:
                                    {
                                        'title': 'Failed',
                                        'class': 'badge-danger'
                                    },
                                7:
                                    {
                                        'title': 'Attention!',
                                        'class': 'badge-primary'
                                    },
                                8:
                                    {
                                        'title': 'In Progress',
                                        'class': 'badge-success'
                                    },
                            };
                            if (typeof badge[data] === 'undefined')
                            {
                                return data;
                            }
                            return '<span class="badge ' + badge[data].class + ' badge-pill">' + badge[data].title + '</span><br>';
                        },
                    }],
            });

        let channel = pusher.subscribe('submission-channel');
        channel.bind('App\\Events\\NewSubmissionEvent', function(data) {
            databank_datatable.draw();
        });

    });
</script>
