<div class="modal fade" id="addAccountModal" tabindex="-1" role="dialog" aria-labelledby="addSchoolModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account-label">Add New Account</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <br>
            <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
            </div>
            <form method="post" id="add-new-account-form" enctype="multipart/form-data">

                <div class="modal-body">
                    <div class="form-group">
                        <label for="fullname">Name</label>
                        <input type="text" class="form-control" name="name" id="fullname" required placeholder="Name">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Email" name="email">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Phone</label>
                        <input type="tel" class="form-control" id="phone" aria-describedby="phoneHelp" placeholder="Phone" name="phone">
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Type password">
                    </div>

                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" class="form-control" id="image" name="image">
                    </div>

                    <div class="form-group">
                        <label for="roles">Roles</label>
                        <select class="form-control role" name="role" id="roles">
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="password">Access Expiry Time</label>
                        <input type="datetime-local" class="form-control" id="password" name="access_expiry_time">
                    </div>

{{--                    <div class="form-group">--}}
{{--                        <label for="permissions">Permissions</label>--}}
{{--                        <select class="form-control" name="permission[]" multiple id="permissions" readonly style="width: 100%; height: 100%;">--}}
{{--                        </select>--}}
{{--                    </div>--}}

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="submit-button">Add New Account</button>
                    <button type="button" class="btn btn-secondary ml-auto" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>

    </div>

</div>

<div class="modal fade" id="editAccountModal" tabindex="-1" role="dialog" aria-labelledby="addSchoolModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account-label">Update Account</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <br>
            <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
            </div>
            <form method="post" id="update-account-form" enctype="multipart/form-data">

                <div class="modal-body">
                    <div class="form-group">
                        <label for="fullname">Name</label>
                        <input type="text" class="form-control" name="name" id="edit-name" required placeholder="Name">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control" id="edit-email" aria-describedby="emailHelp" placeholder="Email" name="email">
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="edit-password" name="password" placeholder="Type password">
                    </div>

                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" class="form-control" id="image" name="image">
                    </div>

                    <div class="form-group">
                        <label for="roles">Roles</label>
                        <select class="form-control role" name="role" id="roles">
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="password">Access Expiry Time</label>
                        <input type="datetime-local" class="form-control" id="password" name="access_expiry_time">
                    </div>

                    {{--                    <div class="form-group">--}}
                    {{--                        <label for="permissions">Permissions</label>--}}
                    {{--                        <select class="form-control" name="permission[]" multiple id="permissions" readonly style="width: 100%; height: 100%;">--}}
                    {{--                        </select>--}}
                    {{--                    </div>--}}

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="submit-button">Update Account</button>
                    <button type="button" class="btn btn-secondary ml-auto" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>

    </div>

</div>

<div class="modal fade" id="logModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account-label">Logs</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <br>
            <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
            </div>

                <div class="modal-body">

                    <table id="dataTable-logs" class="table table-bordered table-hover table-striped w-100">
                        <thead>
                        <tr>
                            <th>Action</th>
{{--                            <th>Subject</th>--}}
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Action</th>
{{--                            <th>Subject</th>--}}
                            <th>Date</th>
                        </tr>
                        </tfoot>
                    </table>

                </div>

        </div>

    </div>

</div>
