<script>
    const permissions           = $("#permissions");
    const roles                 = $(".role");
    const print_error_msg       = $(".print-error-msg");

    permissions.select2();

    function loadPermissions(){
        fetch('/api/all-permissions')
            .then(response => {
                return response.json();
            })
            .then(data => {
                console.log(data);
                let options = "<option value=''>Select Permission(s)</option>";

                data.data.forEach(function (item){
                    options+= `<option value=${item.id}> ${item.name} </option>`;
                })
                permissions.html(options);
            });
    }

    function loadRoles(){
        fetch('/api/all-roles')
            .then(response => {
                return response.json();
            }).then(data => {
            console.log(data);
            let options = "<option value=''>Select Role</option>";

            data.data.forEach(function (item){
                options+= `<option value=${item.id}> ${item.name} </option>`;
            })
            roles.html(options);
        });
    }

    function printErrorMsg (msg) {
        print_error_msg.find("ul").html('');
        print_error_msg.css('display','block');
        $.each( msg, function( key, value ) {
            console.log("error key " + key)
            print_error_msg.find("ul").append('<li>'+value+'</li>');
            $("."+key).text(value);
        });
    }

    $("#add-account").on("click",function (){
        $("#addAccountModal").modal();
    })

    $("#add-new-account-form").on("submit",function (e){
        e.preventDefault();
        // let form_data = $(this).serialize();
        let formData = new FormData(this);
        let $_this = $(this);

        console.log(formData)

        $.ajax({
            url: '/api/add-new-account',
            type: "POST",
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            statusCode: {
                200: function (){
                    toastr.success('Account Added Successfully');
                    print_error_msg.css("display","none");
                },
                500: function () {
                    toastr.error('Account creating failed!');
                },
                422: function (data) {
                    printErrorMsg(data.responseJSON.errors);
                }
            },
        }).then(function (){
            $("#addAccountModal").modal("hide");
            table.draw();
            $_this.trigger("reset");
        })
    });

    $.fn.dataTable.ext.errMode = 'none';
    const table = $('#dataTable-users')
        .on( 'error.dt', function ( e, settings, techNote, message ) {
            toastr.warning("It seems you have overstayed your welcome. redirecting...")
            setTimeout(function (){
                // location.href="/"
            },2500);
            console.log( 'An error has been reported by DataTables: ', message );
        }).DataTable({
        initComplete: function() {
            var datatable_length_text = $('.dataTables_length label').contents().filter(function() {
                                if(this.nodeType === 3) {
                                   if(this.data == ' entries') {
                                        return this.nodeType === 3;
                                   }
                                }
                            }).wrap('<span style="display:none"></style>');
        },
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: "{{ route('users.data') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name',orderable:false},
            {data: 'role', name: 'role'},
            {data: 'access', name: 'access'},
            {data: 'email', name: 'email'},
            {data: 'theme', name: 'theme'},
            {data: 'status', name: 'status'},
        ],
        columnDefs: [

            {
                targets: 2,
                title: 'Role',
                orderable: false,
                render: function(data, type, full, meta)
                {

                    if (full.access_expiry != null) {
                        return `${data} <br><small class="text-muted">Expires ${full.access_expiry}</small>`
                    }else{
                        return `${data}`
                    }
                },
            },
            {
                targets: 3,
                title: 'Access',
                orderable: false,
                render: function(data, type, full, meta)
                {
                    console.log(data)
                    let permissions = `<div class="d-grid grid-col-3">`;
                    let all_access = "";
                    data.forEach(function (item){
                        let elem = `<div class="badge-pill-wrapper pb-1">
                                <span class="badge badge-pill project-status job-pill started text-white">${item}</span>
                            </div>`

                        if (item === "All"){
                            all_access+=elem;
                        }

                        permissions+=elem;
                    })

                    if (all_access !== ""){
                        return `${all_access}`
                    }
                    return `${permissions}</div>`
                },
            },

            {
                targets: 6,
                title: 'Status',
                orderable: false,
                render: function(data, type, full, meta)
                {
                    console.log("***********")
                    console.log(full)
                    let status_mark_element = data === 0 ? `<i class=\"fal fa-check\"></i>`:`<i class=\"fal fa-ban\"></i>`;

                    return "\n\t\t\t\t\t\t<div class='d-flex flex-nowrap'>\n\t\t\t\t\t\t\t" +
                        `<a href='javascript:void(0);' class='btn btn-sm btn-success mr-1 mt-1 change-status' data-id='${full.id}' title='change status'>${status_mark_element}</a>\n\t\t\t\t\t\t\t` +
                        "<a href='javascript:void(0);' class='btn btn-sm btn-primary mr-1 mt-1 log' title='Log'>Log</a>\n\t\t\t\t\t\t\t\t" +
                        "<a href='javascript:void(0);' class='btn btn-sm text-white btn-warning mr-1 mt-1  edit-user' title='edit'><i class=\"fal fa-pencil\"></i></a>\n\t\t\t\t\t\t\t" +
                        "<a href='javascript:void(0);' class='btn btn-sm btn-danger mr-1 mt-1  remove-user' title='remove user'><i class=\"fal fa-times\"></i></a>\n\t\t\t\t\t\t\t";
                    // "<a href='#' class='btn btn-sm btn- btn-icon' data-toggle='dropdown' aria-expanded='true' title='More options'><i class=\"fal fa-times\"></i></a>";
                },
            },

        ]
    });
    // });

    $(document).off("click").on("click",".log",function(){
        $("#logModal").modal();
        let table_data = table.row( $(this).parents('tr') ).data();

        let logsTable = $('#dataTable-logs').DataTable({
            processing: true,
            serverSide: true,
            ajax: `/api/activity-log/${table_data.id}`,
            columns: [
                {
                    data: 'action', name: 'action',orderable:false
                },
                {
                    data: 'created_at', name: 'created_at'},
            ]
        });

        // logsTable.draw();
        // $('#dataTable-logs').data.reload();
        logsTable.destroy();
    })

    $(document).on("click",".edit-user",function(){
        let table_data = table.row( $(this).parents('tr') ).data();

        $("#edit-name").val(table_data.name)
        $("#edit-email").val(table_data.email)
        $(".role").val(table_data.role_id)
        $("#editAccountModal").modal();

        console.log("%%%%%%%%%")
        console.log(table.row( $(this).parents('tr') ).data());

        $("#update-account-form").off("submit").on("submit",function (e){
            e.preventDefault();
            // let form_data = $(this).serialize();
            let formData = new FormData(this);

            let $_this = $(this);

            $.ajax({
                url: `/api/update-user/${table_data.id}`,
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                statusCode: {
                    200: function (){
                        toastr.success('Account Updated Successfully');
                    },
                    500: function () {
                        toastr.error('Account Update failed!');
                    },
                    422: function (data) {
                        printErrorMsg(data.responseJSON.errors);
                    }
                },
            }).then(function (){
                $("#editAccountModal").modal("hide");
                // toastr.success('Account Added Successfully');
                table.draw();
                $_this.trigger("reset");
                print_error_msg.css("display","none");
            })
        });

    })

    $(document).on("click",".remove-user",function(){
        let table_data = table.row( $(this).parents('tr') ).data();

        if (window.confirm("Do you really want to remove account?")) {
            $.ajax({
                        url: `/api/remove-user/${table_data.id}`,
                        type: "POST",
                        cache: false,
                        processData: false,
                        statusCode: {
                            200: function (){
                                toastr.success('Account Removed Successfully!');
                            },
                            500: function () {
                                toastr.error('Account Removal failed!');
                            }
                        },
                    }).then(function (){
                        table.draw();
                    })
        }
    })

    $(document).on("click",".change-status",function(){
        let id = $(this).data("id")
        $.post(`/api/update-user-status/${id}`,function (data){
            toastr.success("Successfully updated an account!")
            table.draw();
        });
    })

    loadPermissions();
    loadRoles();

</script>
