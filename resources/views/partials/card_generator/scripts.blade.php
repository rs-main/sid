<script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script>

<script>
    const button = document.querySelector('#sid-card-link');
    const tooltip = document.querySelector('#sid-card-tooltip');


    const popperInstance = Popper.createPopper(button, tooltip, {
        modifiers: [
            {
                name: 'offset',
                options: {
                    offset: [0, 8],
                },
            },
        ],
    });

    function show() {
        // Make the tooltip visible
        tooltip.setAttribute('data-show', '');

        // Enable the event listeners
        popperInstance.setOptions({
            modifiers: [{ name: 'eventListeners', enabled: true }],
        });

        // Update its position
        popperInstance.update();
    }

    function hide() {
        // Hide the tooltip
        tooltip.removeAttribute('data-show');

        // Disable the event listeners
        popperInstance.setOptions({
            modifiers: [{ name: 'eventListeners', enabled: false }],
        });
    }

    const showEvents = ['mouseenter', 'focus'];
    const hideEvents = ['mouseleave', 'blur'];

    showEvents.forEach(event => {
        button.addEventListener(event, show);
    });

    hideEvents.forEach(event => {
        button.addEventListener(event, hide);
    });

</script>

<script>
    /* demo scripts for change table color */
    /* change background */
    $(function() {
        $('[data-toggle="show-sid-card-tooltip"]').tooltip({
            html: true,

        });
    });


    $(document).ready(function()
    {
        $.fn.dataTable.ext.errMode = 'none';

        let card_generator_table =
            $('#dataTable-card-generator').DataTable({
                initComplete: function () {
                    var datatable_length_text = $('.dataTables_length label').contents().filter(function() {
                                if(this.nodeType === 3) {
                                   if(this.data == ' entries') {
                                        return this.nodeType === 3;
                                   }
                                }
                            }).wrap('<span style="display:none"></style>');

                    var tableFiltersHTML = `   <div class="pt-4 d-flex align-items-center w-auto flex-wrap">
                                            <div class="col-auto mb-3">Filter by</div>
                                            <div class="col-auto mb-3 flex-grow-1">
                                                <select class="form-control custom-select">
                                                    <option>Project name</option>
                                                  </select>
                                            </div>
                                            <div class="col-auto mb-3 flex-grow-1">
                                                <select class="form-control custom-select">
                                                    <option>Institution name</option>
                                                  </select>
                                            </div>
                                            <div class="col-auto mb-3 flex-grow-1">
                                                <select class="form-control custom-select">
                                                    <option>Project officer</option>
                                                  </select>
                                            </div>
                                            <div class="col-auto mb-3 flex-grow-1">
                                                <select class="form-control custom-select">
                                                    <option>Category (Class)</option>
                                                  </select>
                                            </div>

                                        </div>`;

                    $('#dataTable-card-generator_wrapper >  .row.mb-3').append(tableFiltersHTML);
                },
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('card-jobs.data') }}",
                columns: [
                    {data: 'serial_number', name: 'serial_number'},
                    {data: 'job', name: 'job'},
                    {data: 'project_name', name: 'project_name'},
                    {data: 'institution', name: 'institution'},
                    {data: 'project_officer', name: 'project_officer'},
                    {data: 'submitted_on', name: 'submitted_on'},
                    {data: 'status', name: 'status'},
                    {data: 'actions', name: 'actions'},
                ],
                columnDefs: [
                    {
                        targets : [0, 1, 2, 3, 4, 6],
                        orderable : false,
                    },
                    {
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        render: function(data, type, full, meta)
                        {
                            let generate_status = full.generated ? "Regenerate" : "Generate";
                            let button_element = `class='btn btn-outline-primary btn-inline-block mr-1 generate-ids' data-school=${full.school_id} data-imei=${full.imei} title='Generate'>${generate_status}</a>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' `;
                            let print_element =`class='btn btn-outline-success btn-inline-block mr-1 text-nowrap print-ids' title='Print'  data-school=${full.school_id} data-imei=${full.imei}>To Print</a>\n\t\t\t\t\t\t\t<div class='dropdown d-inline-block'>\n\t\t\t\t\t\t\t\t`;
                            let print_button  = full.generated ? print_element : "";

                            return "\n\t\t\t\t\t\t<div class='d-flex demo'>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' " +
                                `${button_element} ` +
                                `${print_button}`
                        },
                    },

                    {
                        targets: 4,
                        title: 'Project Officer',
                        orderable: false,
                        render: function(data, type, full, meta)
                        {
                            return `${data} <br><small class="text-muted">Device ID: ${full.imei}</small>`;
                        },
                    },

                ],
            });

        $('.js-thead-colors a').on('click', function()
        {
            var theadColor = $(this).attr("data-bg");
            console.log(theadColor);
            $('#dt-basic-example thead').removeClassPrefix('bg-').addClass(theadColor);
        });

        $('.js-tbody-colors a').on('click', function()
        {
            var theadColor = $(this).attr("data-bg");
            console.log(theadColor);
            $('#dt-basic-example').removeClassPrefix('bg-').addClass(theadColor);
        });

        $(document).on("click",".generate-ids",function(){
            let table_data = card_generator_table.row( $(this).parents('tr') ).data();
            const $_this = $(this);
            $_this.text("pending...")
            toastr.success("Generating cards...")

            $.post(`/api/generate-cards/${table_data.imei}/${table_data.school_id}`,function (data){
                toastr.success("Successfully generated images.")
                console.log(data.data);
                $_this.text("Generated")
                $_this.attr("disabled",true);
                card_generator_table.draw();

            }).fail(function(data){
                $_this.text("Download")
                $_this.attr("disabled",true);
                console.log(((data)))
            });
        });

        $(document).on("click",".print-ids",function(){
            let table_data = card_generator_table.row( $(this).parents('tr') ).data();
            const $_this = $(this);
            toastr.success("Zipping images..")

            $.post(`/api/download-job/${table_data.imei}/${table_data.school_id}`,function (data){
                // toastr.success("Successfully zipped images. Downloading...")
                toastr.success(`${data.message}`)
                console.log(data.data);
                setTimeout(function (){
                    location.href=`/${data.data}`;
                },2000)
            })
        });

        let channel = pusher.subscribe('submission-channel');
        channel.bind('App\\Events\\NewSubmissionEvent', function(data) {
            card_generator_table.draw();
        });
    });

</script>


<script>


</script>
