<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->uuid("id")->index()->primary();
            $table->string("name")->nullable();
            $table->string("card_title")->nullable();
            $table->string("institution_type")->nullable();
            $table->json("fields")->nullable();
            $table->date("project_start_date")->nullable();
            $table->date("project_end_date")->nullable();
            $table->string("card_size")->nullable();
            $table->string("image_path")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
