<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string("student_id");
            $table->string("card_id");
            $table->string("first_name");
            $table->string("last_name");
            $table->bigInteger("school_id")->unsigned();
            $table->string("class");
            $table->boolean("bio")->default(false);
            $table->string("qr_code")->nullable();
            $table->string("photo")->nullable();
            $table->string("imei");
            $table->string("project_officer_name")->nullable();
            $table->string("date")->nullable();
            $table->string("serial_start")->nullable();
            $table->string("serial_end")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
