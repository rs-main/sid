<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectOfficersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_officers', function (Blueprint $table) {
            $table->uuid("id")->index()->primary();
            $table->string("full_name")->nullable();
            $table->uuid("project_id")->index();
            $table->string("project_location")->nullable();
            $table->string("phone_number")->nullable();
            $table->bigInteger("user_id")->index();
            $table->string("address")->nullable();
            $table->string("id_type")->nullable();
            $table->string("id_no")->nullable();
            $table->date("expiry_date")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_officers');
    }
}
