<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrintPressReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('print_press_reports', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("print_press_id")->unsigned();
            $table->integer("jobs");
            $table->integer("downloaded");
            $table->integer("printed");
            $table->integer("delivered");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('print_press_reports');
    }
}
