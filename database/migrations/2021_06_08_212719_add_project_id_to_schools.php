<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProjectIdToSchools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools', function (Blueprint $table) {
//            $table->dateTime("submit_date")->change();
            $table->uuid("project_id")->index()->nullable()->after("submit_date");
            $table->uuid("project_officer_id")->index()->nullable()->after("project_id");
            $table->uuid("contact_person_id")->index();
            $table->dropColumn(["quality_officer","press"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn(["project_id","project_officer_id","contact_person_id"]);
            $table->string("quality_officer")->nullable();
            $table->string("press")->nullable();

        });
    }
}
