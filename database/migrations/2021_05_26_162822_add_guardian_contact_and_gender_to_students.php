<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGuardianContactAndGenderToStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string("guardian_contact_number")->nullable()->after("bio");
            $table->string("gender",6)->nullable()->after("guardian_contact_number");
            $table->string("issue_date")->nullable()->after("guardian_contact_number");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn(["guardian_contact_number","gender","issue_date"]);
        });
    }
}
