<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProjectIdAndProjectOfficerIdAndClassIdToStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string("status")->nullable();
            $table->string("dob")->nullable()->after("last_name");
            $table->uuid("project_officer_id")->index()->after("project_officer_name");
            $table->uuid("project_id")->index()->after("project_officer_id");
            $table->uuid("class_id")->index()->after("project_id");
            $table->string("bio_right_thumb_img_path")->nullable();
            $table->string("bio_left_thumb_img_path")->nullable();
            $table->string("bio_right_fore_img_path")->nullable();
            $table->string("bio_left_fore_img_path")->nullable();
            $table->string("profile_img_path")->nullable();
            $table->string("card_img_path")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn("project_officer_id");
            $table->dropColumn("dob");
            $table->dropColumn("project_id");
            $table->dropColumn("class_id");
            $table->dropColumn("bio_right_thumb_img_path");
            $table->dropColumn("bio_left_thumb_img_path");
            $table->dropColumn("bio_right_fore_img_path");
            $table->dropColumn("bio_left_fore_img_path");
            $table->dropColumn("profile_img_path");
            $table->dropColumn("card_img_path");
        });
    }
}
