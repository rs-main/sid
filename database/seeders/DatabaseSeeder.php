<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleTableSeeder::class,
            PermissionsTableSeeder::class,
            AddPressInformationSeeder::class
        ]);

        $this->assignPermissionsToRole();
    }

    public function assignRolePermissions(\Spatie\Permission\Models\Role $role,array $permissions){

        $role->syncPermissions($permissions);
    }

    private function assignPermissionsToRole(): void
    {
        foreach (Role::all() as $role) {
            switch ($role->name) {
                case "Super Admin":
                    $role = \Spatie\Permission\Models\Role::find($role->id);
                    $perm_ids = Permission::whereIn("name", ["all", "control-center", "databank", "print", "management", "card-generator", "control-center-delivery"])
                        ->pluck("id")->toArray();
                    $this->assignRolePermissions($role, $perm_ids);
                    break;

                case "Admin":
                    $role = \Spatie\Permission\Models\Role::find($role->id);
                    $perm_ids = Permission::whereIn("name", ["control-center", "databank", "print", "management", "card-generator"])->pluck("id")->toArray();
                    $this->assignRolePermissions($role, $perm_ids);
                    break;

                case "Print Manager":
                    $role = \Spatie\Permission\Models\Role::find($role->id);
                    $perm_ids = Permission::whereIn("name", ["control-center", "databank", "print", "management", "card-generator"])->pluck("id")->toArray();
                    $this->assignRolePermissions($role, $perm_ids);
                    break;

                case "Data Manager":
                    $role = \Spatie\Permission\Models\Role::find($role->id);
                    $perm_ids = Permission::whereIn("name", ["control-center", "management", "card-generator"])->pluck("id")->toArray();
                    $this->assignRolePermissions($role, $perm_ids);
                    break;

                case "Project Owner":
                    $role = \Spatie\Permission\Models\Role::find($role->id);
                    $perm_ids = Permission::whereIn("name", ["control-center"])->pluck("id")->toArray();
                    $this->assignRolePermissions($role, $perm_ids);
                    break;

                case "Delivery":
                    $role = \Spatie\Permission\Models\Role::find($role->id);
                    $perm_ids = Permission::whereIn("name", ["control-center-delivery"])->pluck("id")->toArray();
                    $this->assignRolePermissions($role, $perm_ids);
                    break;

            }

        }
    }
}
