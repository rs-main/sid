<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table("roles")->truncate();

        $roles = ["Super Admin","Admin","Print Manager","Data Manager","Project Owner","Delivery", "Project Officer"];

        foreach ($roles as $role){
            $role = Role::create([
                "name" => $role,
                "guard_name" => "web"
            ]);

//            $role->syncPermissions($request->input('permission'));
        }
    }
}
