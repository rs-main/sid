<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_modules = ["all","control-center","databank","print",
            "management","card-generator","control-center-delivery"];

        foreach ($permission_modules as $permission_module){
            switch ($permission_module){

                case "all":
                    $other_name = "All";
                    $this->storePermission($permission_module, $other_name);
                    break;

                case "control-center":
                    $other_name = "Control Ctr.";
                    $this->storePermission($permission_module, $other_name);
                    break;

                case "databank":
                    $other_name = "Data B.";
                    $this->storePermission($permission_module, $other_name);
                    break;

                case "print":
                    $other_name = "Print";
                    $this->storePermission($permission_module, $other_name);
                    break;

                case "management":
                    $other_name = "Management";
                    $this->storePermission($permission_module, $other_name);
                    break;

                case "card-generator":
                    $other_name = "Card G.";
                    $this->storePermission($permission_module, $other_name);
                    break;

                case "control-center-delivery":
                    $other_name = "Control Ctr.(Delivery)";
                    $this->storePermission($permission_module, $other_name);
                    break;
            }

        }
    }

    private function storePermission($permission_module,$other_name){
        Permission::create([
            "name" => $permission_module,
            "other_name" => $other_name
        ]);
    }
}
