<?php

namespace Database\Seeders;

use App\Models\PrintPress;
use App\Models\PrintPressReport;
use Illuminate\Database\Seeder;

class AddPressInformationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $print_press = new PrintPress();
        $print_press->name = "R.S.Press";
        $print_press->location = "Greater Accra";
        $print_press->officer = "Mr. Muni";
        $print_press->officer_phone = "0244112233";
        if($id = $print_press->save()){
            $report = new PrintPressReport();
            $report->jobs = 0;
            $report->downloaded= 0;
            $report->printed= 0;
            $report->delivered= 0;
            $report->print_press_id=$id;
            $report->save();
        }
    }
}
