<?php


namespace App\Exceptions;


use Spatie\Permission\Exceptions\UnauthorizedException;

class ExpiredUserException extends UnauthorizedException
{
    public static function userExpired(): self
    {
        return new static(403, 'User access has expired!', null, []);
    }

    public static function userActive(): self
    {
        return new static(403, 'Account is suspended for now!', null, []);
    }
}
