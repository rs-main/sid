<?php

namespace App\Console\Commands;

use App\Models\ContactPerson;
use App\Models\PrintJob;
use App\Models\School;
use App\Models\Student;
use Illuminate\Console\Command;

class ResetPrintJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset:jobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resets jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        PrintJob::truncate();
        Student::truncate();
        foreach (School::whereStatus("started")->orWhere("status","submitted")->pluck("school_code") as $item){
            $url = "https://dextraclass.com/api/sid/schools/update-school-details-by-code";
            \Http::withToken("MnkxMGVaaUZJQktCdVM1U0V0Y2daMFlKQWV6UU42M3hjak9PbTlJSkJJOHBsb3h3MTBIZjgzMg==")
                ->post($url, ["school_code" => $item, "sid_status" => "target"]);
        }
        School::orWhere("status","started")->orWhere("status","submitted")->update(["status" => "target"]);
        ContactPerson::truncate();
//        School::truncate();
    }
}
