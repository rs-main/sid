<?php

namespace App\Console\Commands;

use App\Jobs\InitializePrintJobsDataJob;
use App\Jobs\InitializeStudentsDataJob;
use Illuminate\Console\Command;

class InitializePrintJobsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:print-jobs';

    /**
     * The console command inserts print jobs.
     *
     * @var string
     */
    protected $description = 'Inserts print jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        InitializePrintJobsDataJob::dispatch();
//        InitializeStudentsDataJob::dispatch()->delay(now()->addSeconds(20));
        InitializePrintJobsDataJob::dispatch();
        InitializeStudentsDataJob::dispatch()->delay(now()->addSeconds(60));
    }
}
