<?php

namespace App\Console\Commands;

use App\Jobs\InitializeStudentsDataJob;
use Illuminate\Console\Command;

class initializeStudentsDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initialize:students';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize students';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        InitializeStudentsDataJob::dispatch();
    }
}
