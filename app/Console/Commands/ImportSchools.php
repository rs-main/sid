<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;
use Webpatser\Uuid\Uuid;

class ImportSchools extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:schools';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->comment("Started....  ".PHP_EOL);
        $i = 0;
        $filename = storage_path()."/schools_and_codes.xlsx";
        $collection = new Collection();
        (new FastExcel)->sheet(1)->import($filename, function ($line) use($collection,$i) {
            $collection->push($line);
            DB::table("schools")->insert([
                "school_name" => $line["SCHOOL"],
                "school_code"         => $line["SCHOOL CODE"],
            ]);

            $count = $i++;
            $this->comment("info => " .$count." " . implode(" , ",$line) . PHP_EOL);
        });

        $this->comment("From Collection " . implode($collection[0]));
    }
}
