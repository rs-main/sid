<?php

namespace App\Listeners;

use App\Events\StudentRegistered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StudentRegisteredListener
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  StudentRegistered  $event
     * @return void
     */
    public function handle($event)
    {

        $token = "MnkxMGVaaUZJQktCdVM1U0V0Y2daMFlKQWV6UU42M3hjak9PbTlJSkJJOHBsb3h3MTBIZjgzMg==";
        \Http::withToken($token)->post("https://dextraclass.com/api/sid/schools/students/add-new-student",
            [
                "school_code"=> $event->student_sample_object["sample_object"]["info"]["school_code"],
                "school"     => $event->student_sample_object["sample_object"]["info"]["school"],
                "region"     => $event->student_sample_object["sample_object"]["info"]["region"],
                "index_number_jhs"     => "",
                "student_name"     => $event->student_id_object->first_name,
                "year_group"     =>   "",
                "program"     =>   "",
                "date_of_birth" => $event->student_id_object->dob,
                "residential_status" => "",
                "student_mobile_number" => $event->student_id_object->guardian_contact_number,
                "shs_number" => $event->student_id_object->student_id,
                "other_id"   => ""
            ]);
    }
}
