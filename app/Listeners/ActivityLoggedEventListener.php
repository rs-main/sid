<?php

namespace App\Listeners;

use App\Events\ActivityLoggedEvent;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class ActivityLoggedEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ActivityLoggedEvent $event
     * @return void
     */
    public function handle(ActivityLoggedEvent $event)
    {
        activity()
            ->causedBy(User::find(Auth::id()))
            ->performedOn(User::find(Auth::id()))
            ->log($event->logMessage);
    }
}
