<?php

namespace App\Http\Controllers;

use App\Http\Resources\RoleResource;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class RoleController extends Controller
{

    public function getAllRoles(): AnonymousResourceCollection
    {
        $roles = Role::all();
        return RoleResource::collection($roles);
    }
}
