<?php

namespace App\Http\Controllers;

use App\Models\PrintJob;
use App\Models\ProjectOfficer;
use App\Models\School;
use App\Models\Student;
use App\Traits\ApiResponse;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class CardGeneratorController extends Controller
{
    use ApiResponse;
    use ConsumeExternalService;

    public function __construct()
    {
        $this->middleware('permission:card-generator', ['only' => ['getCardGenerator']]);
    }

    public function getCardGenerator(){
        return view("admin.card_generator");
    }

    public function getCardGeneratorJobs(){
        $jobs = PrintJob::with("school")->orderByDesc("created_at")->get();

        return DataTables::of($jobs)

            ->addColumn('serial_number',function ($row){
                $text = 1;
                return $text;

            })->addColumn('job',function ($row){
                $text = "Job";
                return $text;

            })->addColumn('project_name',function ($row){
                $text = "GES SHS 2020 e-card";
                return $text;

            })->addColumn('institution',function ($row){
                $text = $row->school? $row->school->school_name : "";
                return $text;

            })->addColumn('project_officer',function ($row){
                $project_officer_id = School::whereId($row->school_id)->pluck("project_officer_id");
                $officer = ProjectOfficer::whereId($project_officer_id)->first();
                return $officer?  $officer->full_name:"";

            })->addColumn('submitted_on',function ($row){
                $text = $row->created_at;
                return $text;

            })->addColumn('status',function ($row){
                $text = "Pending";
                return $text;

            })->addColumn('actions',function ($row){
                $text = "on";
                return $text;

            })->make(true);

    }

    public function downloadJob($imei, $school_id)
    {
        $print_job = PrintJob::whereImei($imei)->whereSchoolId($school_id)->first();

        if ($print_job && $print_job->download_url == null){
            return  $this->successResponse(["message" => "Generate cards first!","status"=>"failed"], 200);
        }

        return $this->successResponse(["message" => "Successfully generated cards",
            "data"=> $print_job->download_url,"status"=> "success"], 200);
    }

    public function generateCards($imei, $school_id)
    {
        if (School::find($school_id) && School::find($school_id)->generated){
            return  $this->successResponse(["message" => "Generate cards first!",
                "data"=> [], "ids" => []
            ], 200);
        }
        $print_job = PrintJob::whereImei($imei)->whereSchoolId($school_id)->first();

        $builder = Student::whereImei($imei)->whereSchoolId($school_id);
        $student_card_ids = $builder->pluck("card_id")->toArray();
        $api_response = $this->processImageZips($school_id,$imei, $student_card_ids);

        DB::beginTransaction();
        $print_job->update([
            "generated" => true,
            "download_url" => $api_response["data"]
        ]);

        $builder->update([
            "generated" => true
        ]);

        DB::commit();

        return $this->successResponse(["message" => "Successfully generated cards!",
            "data"=> $api_response["data"], "ids" => $api_response["ids"]
        ], 200);
    }

    /**
     * @param $school
     * @param $imei
     */
    private function processImageZips($school, $imei,$card_ids)
    {
        $name_of_zipped = $imei . "_" . $school . ".zip";

        if (file_exists(public_path()."/$name_of_zipped")){
            $file_pointer = public_path()."/$name_of_zipped";
            unlink($file_pointer);
        }

        $all_card_ids = $card_ids;

        $zip = new \ZipArchive();
        if ($zip->open(public_path()."/$name_of_zipped", \ZipArchive::CREATE) === TRUE) {
            foreach ($all_card_ids as $card_id) {
                $zip->addFile($card_id.".png");
//                $zip->addFile(public_path()."/$card_id.png");
            }

            $zip->close();
        }

        return ["data" => "$name_of_zipped", "ids"=>$all_card_ids];
    }


}
