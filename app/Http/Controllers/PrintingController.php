<?php

namespace App\Http\Controllers;

use App\Models\PrintJob;
use App\Models\PrintPress;
use App\Models\School;
use App\Traits\ApiResponse;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PrintingController extends Controller
{
    use ApiResponse;
    use ConsumeExternalService;

    public function getPrinting(){
        return view("admin.printing");
    }

    public function getPrintDetails($code, $imei){
        return view("admin.print_details",compact('code','imei'));
    }

    public function getPrintJobs(){

        $schools = School::with("printJobs")->where("status","started")
            ->orWhere("status","submitted")->orWhere("status","delivered")
            ->orderByDesc("created_at")->get();

        return DataTables::of($schools)
            ->addColumn('school_name',function ($row){
                $text = $row->school_name;
                return $text;

            })->addColumn('print_pending',function ($row){
                $text = $row["print_jobs"];
                return $text;

            })->addColumn('print_completed',function ($row){
                $text = $row["print_jobs"];
                return $text;

            })->make(true);
    }

    public function printPresses(){
        $presses = PrintPress::with("report")
            ->orderByDesc("created_at")->get();

        return DataTables::of($presses)

            ->addColumn('press',function ($row){
                $text = $row->name;
                return $text;

            })->addColumn('downloaded',function ($row){
                $text = PrintJob::whereGenerated(true)->count();
                return $text;

            })
            ->addColumn('printed',function ($row){
                $text = $row->printed;
                return $text;
            })

            ->addColumn('delivered',function ($row){
                $text = $row->delivered;
                return $text;
            })

            ->addColumn('officer',function ($row){
                $text = $row->officer;
                return $text;
            })

            ->addColumn('officer_number',function ($row){
                $text = $row->officer_number;
                return $text;
            })

            ->addColumn('jobs',function ($row){
                $text = PrintJob::count();
                return $text;
            })

            ->make(true);
    }

}
