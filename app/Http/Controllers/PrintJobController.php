<?php

namespace App\Http\Controllers;

use App\Models\PrintJob;
use App\Models\ProjectOfficer;
use App\Models\School;
use App\Models\Student;
use App\Traits\ApiResponse;
use App\Traits\ConsumeExternalService;
use Yajra\DataTables\DataTables;

class PrintJobController extends Controller
{
    use ConsumeExternalService;
    use ApiResponse;

    public function __construct()
    {

    }


    public function getJobDetails($imei,$school_code){
        $studentIds = Student::whereImei($imei)->whereSchoolId(School::GetId($school_code))
            ->select(\DB::raw("MIN(student_id) AS start, MAX(student_id) AS end"))->get();

        $job = PrintJob::whereHas("school",function ($q) use($school_code){
            return $q->whereSchoolCode($school_code);
        })->with(["school" => function($school){
               return $school->with("projectOfficer:id,full_name")
                   ->selectRaw("id,school_code,submit_date,school_name,project_officer_id")->first();
        }])->whereImei($imei)->selectRaw("print_jobs.school_id,imei,print_jobs.created_at")->first();

        return $this->successResponse(["data" => $job, "serials" => $studentIds,"imei" => $imei]);
    }

    public function syncPrintJobs(){
//          InitializePrintJobsDataJob::dispatch();
//          InitializeStudentsDataJob::dispatch()->delay(now()->addSeconds(10));
    }

    public function getCard($card_id){
        $student = Student::whereStudentId($card_id)->leftJoin("schools",
            "students.school_id","=","schools.id")->first();
        return view("admin.card",compact("student"));
    }
}
