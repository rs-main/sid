<?php

namespace App\Http\Controllers;

use App\Models\School;
use App\Models\Student;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class DataBankController extends Controller
{

    public function getDataBank(){
        return view("admin.databank");
    }

    public function allStudents(){
        $count = 0;
        $students = Student::orderByDesc("created_at")->get();
        return DataTables::of($students)
            ->addIndexColumn('serial_number',function ($row) use ($count) {
                return $row;
            })
            ->addColumn('full_name',function ($row){
                $fullName = $row->first_name;
                return $fullName;
            })

            ->addColumn('card_id',function ($row){
                $text = $row->card_id;
                return $text;
            })

            ->addColumn('project_name',function ($row){
                $text = "Project";
                return $text;
            })


            ->addColumn('school_name',function ($row){
                $school = School::find($row->school_id);
                return $school->school_name;

            })

            ->addColumn('action',function ($row){
                return " Action";

            })->addColumn('gender',function ($row){
                return $row->gender;

            })

            ->addColumn('bio',function ($row){
                $text = $row->bio ? "yes" : "no";
                return $text;

            })->make(true);
    }

}
