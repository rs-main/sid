<?php

namespace App\Http\Controllers;

use App\Models\School;
use App\Models\Student;
use Illuminate\Http\Request;

class ControlCenterController extends Controller
{

    //

    public function getIndex(){
        $schools_collection = [];

        foreach (School::pluck("school_code") as $school_code) {
            $school = School::whereSchoolCode($school_code)->first();
            $school_id = $school ? $school->id : null;
            $student_count = Student::whereSchoolId($school_id)->whereNotNull("status")->count();
            $schools_collection[$school_code] = $student_count;
        }

        return view("admin.index",compact("schools_collection"));
    }

    // TODO
    public function getServerStats(){

    }

    // TODO
    public function scheduleAppRelease(){

    }
}
