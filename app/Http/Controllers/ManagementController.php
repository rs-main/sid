<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectOfficer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Webpatser\Uuid\Uuid;
use Yajra\DataTables\DataTables;

class ManagementController extends Controller
{

    public function getManagement(){
        return view("admin.management");
    }

    public function projects(){
        $projects = Project::selectRaw("id,name")->get();
        return response()->json(["data" => $projects]);
    }


    public function postAddNewProject(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'card_title' => 'required',
            'project_start_date' => 'required',
            'project_end_date' => 'required',
            "card_size" => 'required',
            "image"     => 'required'
        ]);

        $name = $request->name;
        $card_title = $request->card_title;
        $card_size = $request->card_size;
        $project_start_date = $request->project_start_date;
        $project_end_date = $request->project_end_date;
        $fields           = $request->get("field");
        $uuid             = Uuid::generate();

        $project = new Project();
        $project->id = $uuid;
        $project->name = $name;
        $project->card_title = $card_title;
        $project->card_size = $card_size;
        $project->project_start_date = $project_start_date;
        $project->project_end_date = $project_end_date;

        $imageName = $uuid.'.'.$request->image->extension();
        $request->image->move(public_path('images'), $imageName);
        $project->image_path = $imageName;
        $project->fields = json_encode($fields);
        $project->save();

//        Project::create($request->all());
    }

    public function projectOfficers(){

        $project_officer_role = Role::where("name","LIKE","%Project Officer%")->first();
        $role_id = $project_officer_role ? $project_officer_role->id : null;
        $officers = User::leftJoin("model_has_roles","model_has_roles.model_id","=","users.id")
            ->where("model_has_roles.role_id",$role_id)->selectRaw("name,id")->get();
        return response()->json(["data" => $officers]) ;

    }

    public function postAddNewProjectOfficer(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'project_id' => 'required',
            'project_location' => 'required',
            'address' => 'required',
            'id_type' => 'required',
            'id_no' => 'required',
            'expiry_date' => 'required',
//            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        DB::transaction(function () use($request){
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);

            $project_officer = new ProjectOfficer();
            $project_officer->id = Uuid::generate();
            $project_officer->project_id = $request->project_id;
            $project_officer->full_name = $user->name;
            $project_officer->user_id=$user->id;
            $project_officer->address = $request->address;
            $project_officer->phone_number = $request->phone;
            $project_officer->id_type = $request->id_type;
            $project_officer->id_no = $request->id_no;
            $project_officer->project_location = $request->project_location;
            $project_officer->expiry_date = $request->expiry_date;
            $project_officer->added_by_user_id = Auth::user()->id;
            $project_officer->save();

            if ($request->has("image")) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('images'), $imageName);
                User::find($user->id)->update([
                    "profile_photo_path" => $imageName
                ]);
            }
        });
    }


    public function allProjects(){

        $projects = Project::all();

        return DataTables::of($projects)

            ->addColumn('project_name',function ($row){
                $text = $row->name;
                return $text;

            })->addColumn('project_no',function ($row){
                $text = 1 ;
                return $text;

            })->addColumn('officers_assigned',function ($row){
                $text = ProjectOfficer::whereProjectId($row->id)->count();
                return $text;

            })
            ->addColumn('created_at',function ($row){
                $text = $row->created_at;
                return $text;

            })->make(true);
    }


}
