<?php

namespace App\Http\Controllers;

use App\DTO\AccountDataFactory;
use App\Http\Requests\StoreAccountRequest;
use App\Models\ProjectOfficer;
use App\Models\User;
use App\Repositories\ProjectOfficer\ProjectOfficerRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Activitylog\Models\Activity;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Webpatser\Uuid\Uuid;
use Yajra\DataTables\DataTables;

class AccountController extends Controller
{
    private $user;
    private $projectOfficer;

    public function __construct(
        UserRepository $userRepository,
        ProjectOfficerRepository $projectOfficerRepository
    )
    {

//        $this->middleware('permission:databank', ['only' => ['getDataBank']]);
//        $this->middleware('permission:control-center', ['only' => ['getIndex']]);
//        $this->middleware('permission:print', ['only' => ['getPrinting']]);
//        $this->middleware('permission:management', ['only' => ['getManagement']]);
//        $this->middleware('permission:control-center-delivery', ['only' => ['getDelivery']]);
//        $this->middleware('permission:all', ['only' => ['getUsers',"postAddNewAccount","getAllUsersData",
//            "postChangeUserStatus","update","removeUser"]]);

        $this->user = $userRepository;
        $this->projectOfficer = $projectOfficerRepository;

    }

    public function getUsers(){
        return view("admin.users");
    }

    public function postAddNewAccount(StoreAccountRequest $request){



        DB::transaction(function () use($request){
            $user = $this->user->store($request);
            $role = Role::whereId($request->input("role"))->first();

            if ($request->has("image")) {
                $imageName = time().'.'.$request->image->extension();
                $this->user->saveImage($user,$imageName);
                $request->image->move(public_path('images'), $imageName);
            }

            $this->user->saveTheme($user, $role);

            $user->assignRole($request->input('role'));

            if ($role && $role->name == "Project Officer"){
                $this->projectOfficer->attachUserToProjectOfficer($user);
            }
        });
    }

    public function getAllUsersData(){
        $users = $this->user->all();

        return DataTables::of($users)
            ->addColumn('name',function ($row){
                $text = $row->name;
                return $text;
            })
            ->addColumn('email',function ($row){
                $text = $row->email;
                return $text;

            })->addColumn('role',function ($row){
                $role_ids = DB::table('model_has_roles')->where('model_id',$row->id)
                    ->pluck("role_id");
                $role = Role::whereIn("id",$role_ids)->first();
                $text = $role ? $role->name : ""  ;
                return $text;

            })->addColumn('role_id',function ($row){
                $role_ids = DB::table('model_has_roles')->where('model_id',$row->id)
                    ->pluck("role_id");
                $role = Role::whereIn("id",$role_ids)->first();
                $text = $role ? $role->id : ""  ;
                return $text;

            })->addColumn('access',function ($row){
                $role_ids    = DB::table('model_has_roles')->where('model_id',$row->id)->pluck("role_id");
                $permissions = DB::table("role_has_permissions")->whereIn("role_id",$role_ids)
                    ->selectRaw("permission_id")->pluck("permission_id");
                $permission_names = Permission::whereIn("id",$permissions)->pluck("other_name")->toArray();
                $text = $permission_names;
                return $text;

            })->addColumn('theme',function ($row){
                return $row->theme;

            })->addColumn('status',function ($row){
                $text = $row->active;
                return $text;

            })->addColumn('access_expiry',function ($row){
                $text = $row->access_expiry_time;
                return $text;

            })->make(true);
    }

    public function postChangeUserStatus($id){
        $user = $this->user->find($id);
        $status = $user->active === 1 ? 0 : 1;
        $user->update(["active" => $status]);
    }

    public function update(Request $request, $id)
    {
        $user = $this->user->find($id);
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$user->id
        ]);

//        $imageName = $request->has("image") ?  time().'.'.$request->image->extension() : "";
//        if ($request->has("image")) {
//            $request->image->move(public_path('images'), $imageName);
//        }

        if ($request->has("image")) {
            $imageName = time().'.'.$request->image->extension();
            $this->user->saveImage($user,$imageName);
            $request->image->move(public_path('images'), $imageName);
        }
        $role = Role::whereId($request->input("role"))->first();

        $this->user->updateUserWithPassword($request,$role,$imageName,$user);

        DB::table("model_has_roles")->where("model_id",$user->id)->delete();
        $user->assignRole($request->input('role'));

        return response()->json(["message" => "success"]);
    }

    public function removeUser($id){
        $user = $this->user->find($id);
        $this->user->destroy($user);
    }

    public function getUserLogs($userId){
        $activities = Activity::whereCauserId($userId)->get();

        return DataTables::of($activities)

            ->addColumn('action',function ($row){
                $text = $row->description." an account";
                return $text;

            })->addColumn('subject',function ($row){
                $user = User::find($row->subject_id);
                $text = $user? $user->name : "";
                return $text;

            })->addColumn('created_at',function ($row){
                $text = $row->created_at;
                return $text;

            })->make(true);
    }

}
