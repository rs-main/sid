<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *    title="SID ApplicationAPI",
 *    version="1.0.0",
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

//    public function __construct()
//    {
//        $this->middleware('permission:databank', ['only' => ['getDataBank']]);
//        $this->middleware('permission:control-center', ['only' => ['getIndex']]);
//        $this->middleware('permission:print', ['only' => ['getPrinting']]);
//        $this->middleware('permission:management', ['only' => ['getManagement']]);
//        $this->middleware('permission:control-center-delivery', ['only' => ['getDelivery']]);
//        $this->middleware('permission:all', ['only' => ['getUsers',"postAddNewAccount","getAllUsersData",
//            "postChangeUserStatus","update","removeUser"]]);

//    }
}
