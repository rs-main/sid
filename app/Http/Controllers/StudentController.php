<?php

namespace App\Http\Controllers;

use App\Models\PrintJob;
use App\Models\School;
use App\Models\Student;
use App\Models\User;
use App\Traits\ApiResponse;
use App\Traits\ConsumeExternalService;
use GraphQL\QueryBuilder\QueryBuilder;
use GraphQL\RawObject;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;


class StudentController extends Controller
{
    use ConsumeExternalService;
    use ApiResponse;
    public function __construct()
    {
        $this->endPoint = "https://staging.api.desafrica.com/v1";
    }


    public function index($school_code, $imei){

        $count = 0;
        $users = Student::whereHas("school",function ($q) use ($school_code){
            return $q->where("school_code",$school_code);
        })->whereImei($imei)->orderByDesc("created_at")->get();

        return DataTables::of($users)
            ->addColumn('serial_number',function ($row) use ($count) {
                $count++;
                $serial_number = $count;
                return $serial_number;
            })
            ->addColumn('card_id',function ($row){
                $text = $row->card_id;
                return $text;
            })

            ->addColumn('full_name',function ($row){
                $fullName = $row->first_name;
                return $fullName;
            })

            ->addColumn('school_name',function ($row){
                $school = School::find($row->school_id);
                return $school->school_name;

            })

            ->addColumn('class',function ($row){
                return "SHS ".$row->class;

            })->addColumn('photo',function ($row){
                return $row->photo;

            })->addColumn('qr_code',function ($row){
                $text = $row->qr_code;
                $image_url  = "/storage/$text";
                return $image_url;

            })->addColumn('bio',function ($row){
                $text = $row->bio ? "yes" : "no";
                return $text;

            })->addColumn('action',function ($row){
                $text = $row->bio ? "yes" : "no";
                return $text;

            })->make(true);
    }

    public function testEndPoint(){
        $collection = new Collection();
        PrintJob::chunk(5, function ($jobs) use ($collection) {
            foreach ($jobs as $job) {
                $builder = $this->getStudentsQuery();
                $school_code = School::find($job->school_id)? School::find($job->school_id)->first()->school_code : "";
                $variablesArray = ['schoolCode' => $school_code, 'imei' => $job->imei];
                 $collection->push($students = $this->graphqlQueryWithVariables($builder,$variablesArray)["data"]["getPrintJobDetails"]["students"]);
                Student::insertStudents($students);
            }
        });

        return $collection;
    }

    private function getStudentsQuery(){
        $builder = (new QueryBuilder('getPrintJobDetails'))
            ->setVariable('schoolCode', 'String',true)
            ->setVariable('imei', 'String',true)
            ->setArgument('input',
                new RawObject('{ schoolCode: $schoolCode , imei: $imei }'
                ))->selectField(
                (new QueryBuilder('students'))
                    ->selectField('firstName')
                    ->selectField('lastName')
                    ->selectField('cardId')
                    ->selectField('schoolName')
                    ->selectField('studentId')
                    ->selectField('class')
                    ->selectField('bio')
                    ->selectField('imei')
                    ->selectField('projectOfficerName')
                    ->selectField('date')
                    ->selectField('serialStart')
                    ->selectField('serialEnd')
            );

        return $builder;
    }

    public function addNewStudent(){

    }

}
