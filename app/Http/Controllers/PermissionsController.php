<?php

namespace App\Http\Controllers;

use App\Http\Resources\PermissionResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Spatie\Permission\Models\Permission;

class PermissionsController extends Controller
{

/**
 * @OA\Get(
* path="/api/all-permissions",
* summary="Retrieve All Permissions",
* description="Get all permissions available",
* operationId="profileShow",
* tags={"permissions"},
* security={ {"bearer": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success",
 * @OA\Response(
 *    response=401,
 *    description="User should be authorized to get permissions",
 * )
 * )
 * )
 */

    /**
     * @return AnonymousResourceCollection
     */
    public function getAllPermissions(): AnonymousResourceCollection
    {
        $permissions = Permission::all();
        return PermissionResource::collection($permissions);
    }

}
