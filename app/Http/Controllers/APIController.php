<?php

namespace App\Http\Controllers;

use App\Events\StudentRegistered;
use App\Http\Resources\SchoolSetUpResource;
use App\Http\Resources\StudentResource;
use App\Jobs\CaptureCardJob;
use App\Models\ContactPerson;
use App\Models\PrintJob;
use App\Models\Project;
use App\Models\ProjectOfficer;
use App\Models\School;
use App\Models\Student;
use App\Models\User;
use Carbon\Carbon;
use FontLib\TrueType\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Pusher\Pusher;
use Spatie\Permission\Models\Role;
use Webpatser\Uuid\Uuid;
use function Livewire\str;

class APIController extends Controller
{

    private const XTRA_CLASS_BASE_URL = "https://dextraclass.com/api/sid/schools/students/";
    private const XTRA_CLASS_TOKEN = "MnkxMGVaaUZJQktCdVM1U0V0Y2daMFlKQWV6UU42M3hjak9PbTlJSkJJOHBsb3h3MTBIZjgzMg==";

    public function submitStudent(Request $request)
    {

        if (empty($request->firstName) && empty($request->lastName)
            && empty($request->dob) && empty($request->gender) && empty($request->guardianPhone)
            && empty($request->profileImg) && empty($request->leftForeFingure) && empty($request->rightForeFingure)
            && empty($request->leftThumbFingure) && empty($request->rightThumbFingure) && empty($request->cardImg)
        )
            return response()->json(["message" => "some missing fields are required"], 400);

        if ($studentObject = Student::whereStudentId($request->studentId)->first()) {
            return response()->json(["data" => new StudentResource($studentObject)], 200);
        }

        $student = new Student();
         $school = $this->setUpStudentInfo($request, $student);

        $this->setUpStudentImages($student, $request);
        $studentArray["firstName"] = $student->first_name;
        $studentArray["lastName"] = $student->last_name;
        $studentArray["cardId"] = $student->card_id;
        $studentArray["schoolName"] = $school->school_name;
        $this->savePrintJob($request, $school);
        $qr_code_path = Student::saveStudentQRCode($studentArray);
        $student->qr_code = $qr_code_path;
        $school->update([
            "project_officer_id" => $request->projectOfficerId
        ]);
        if ($student->save()){
            CaptureCardJob::dispatch($studentArray["cardId"]);
//            $this->submitEvent();
        }

        return response()->json(["data" => new StudentResource($student)], 200);
    }

    public function setUpSchool(Request $request){

        $school_code = $request->schoolCode;
        $project_id = $request->projectId;
        $contact_person = $request->contactPerson;
        $contact_number = $request->contactNumber;
        $status = $request->status;

        $school_builder = School::with("contactPerson")->whereSchoolCode($school_code)->first();

        if ($school_builder && $school_builder->status == "started" ){

            return response()->json([
                "data" =>[
                    "id"                => $school_builder->id,
                    "schoolCode"        => $school_builder->school_code,
                    "contactPerson"     => $school_builder->contactPerson ? $school_builder->contactPerson->name : "",
                    "contactNumber"     => $school_builder->contactPerson? $school_builder->contactPerson->phone : "",
                    "status"            => $school_builder->status,
                    "card_count"        => 0,
                    "isSchoolSetupAlready" => true
                    ]
            ]);
        }


            $cp = ContactPerson::wherePhone($contact_number)->first();
            $contactPersonId = Uuid::generate();

             if (!$cp) {
                $contactPerson = new ContactPerson();
                $contactPerson->id = $contactPersonId;
                $contactPerson->name = $contact_person;
                $contactPerson->phone = $contact_number;
                $contactPerson->save();
             }
             $school_builder->update([
                "submit_date" => Carbon::now(),
                "project_id" => $project_id,
                "status"     => $status,
                "contact_person_id" => $cp ? $cp->id :  $contactPersonId
            ]);

        $this->changeSchoolStatus($school_code, $status);
        $school = School::with("contactPerson")->whereSchoolCode($school_code)->first();
        $this->submitEvent();
        return new SchoolSetUpResource($school);
    }

    public function projectOfficerLogin(Request $request){

        $user= User::wherePhone($request->phone)->first();
        $user_email = $user ? $user->email : null;

        $credentials = ["email" => $user_email, "password" => $request->password];
        if (!\Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Invalid login details'
            ], 401);
        }

        $token = $user->createToken('auth_token')->plainTextToken;

        $project_officer = ProjectOfficer::whereUserId($user->id)->first();
        $fullname =  explode(" ",$user->name);

        return response()->json([
            'token'              =>  $token,
            'project_id'         =>  $project_officer->project_id,
            'project_officer_id' =>  $project_officer->id,
            'firstname'          =>  isset($fullname) ? $fullname[0] : "",
            'lastname'           =>  isset($fullname[1]) ? $fullname[1] : "",
        ]);
    }





    public function assignProject(Request $request){
        $request->validate([
            'user_id' => 'required',
            'project_id' => 'required',
            'project_location' => 'required'
        ]);
        $project_officer = ProjectOfficer::whereUserId($request->user_id)->first();
        $project_officer->update($request->all());

    }

    public function getSchoolStudentsRegistered($school_code,$totalNumber){
        $school = School::whereSchoolCode($school_code)->first();
        $school_id = $school ? $school->id : null;
        $student_count = Student::whereSchoolId($school_id)->whereNotNull("status")->count();
        $percentage_completed = ($student_count/$totalNumber)*100;
        return response()->json(["data" => number_format((float)$percentage_completed, 2, '.', '')]);
    }

    public function getStudentsRegistered(): JsonResponse
    {
        $schools_collection = [];

        foreach (School::pluck("school_code") as $school_code) {
            $school = School::whereSchoolCode($school_code)->first();
            $school_id = $school ? $school->id : null;
            $student_count = Student::whereSchoolId($school_id)->whereNotNull("status")->count();
            $schools_collection[$school_code] = $student_count;
        }
        return response()->json(["data" => $schools_collection]);
    }

    /**
     * @param Request $request
     * @param string $image_name
     * @param string $field
     */
    private function saveImage(Request $request, string $image_name, $field): void
    {
        try {
            $request->$field->move(public_path('images'), $image_name);
        }catch (\Exception $exception){
            \Sentry::captureException($exception);
        }
    }

    /**
     * @param Request $request
     * @param Student $student
     * @return School|\Illuminate\Database\Eloquent\Builder|Model|object|null
     */
    private function setUpStudentInfo(Request $request, Student $student)
    {
        $school = School::whereSchoolCode($request->schoolCode)->first();

         $studentIdGenerated =  $this->studentIdIsEmpty($request) ?
            $this->generateStudentId($school->school_code,$request->classId) : ["id" => $request->studentId];

            $studentId                        = $studentIdGenerated["id"];
            $student->card_id                 = $studentId ;
            $student->imei                    = $request->imei;
            $student->project_officer_id      = $request->projectOfficerId;
            $student->project_id              = $request->projectId;
            $student->first_name              = $request->firstName;
            $student->last_name               = $request->lastName;
            $student->student_id              = $studentId;
            $student->dob                     = $request->dob;
            $student->gender                  = $request->gender;
            $student->guardian_contact_number = $request->guardianPhone;
            $student->status                  = $request->status;
            $student->school_id               = $school->id;
            $student->class_id                = $request->classId;
            $student->class                   = $request->classId;

            if ($school && $school->status != School::SUBMITTED ){
                $school->update(["status" => School::SUBMITTED]);
                $this->changeSchoolStatus($school->school_code,School::SUBMITTED);
            }

            if ($this->studentIdIsEmpty($request)){
                $student->new = true;
//                StudentRegistered::dispatch($studentIdGenerated,$student);
            }

        return $school;
    }

    /**
     * @param Student $student
     * @param Request $request
     */
    private function setUpStudentImages(Student $student, Request $request): void
    {
        // save profile image
        $profile_image_name = $student->student_id . '_profile' . '.' . $request->profile->extension();
        $this->saveImage($request, $profile_image_name, "profile");
        $student->profile_img_path = $profile_image_name;

        // save left fore image
        $left_fore_image_name = $student->student_id . '_left_fore' . '.' . $request->bioLeftFore->extension();
        $this->saveImage($request, $left_fore_image_name, "bioLeftFore");
        $student->bio_left_fore_img_path = $left_fore_image_name;

        // save right fore image
        $right_fore_image_name = $student->student_id . '_right_fore' . '.' . $request->bioRightFore->extension();
        $this->saveImage($request, $right_fore_image_name, "bioRightFore");
        $student->bio_right_fore_img_path = $right_fore_image_name;

        // save right thumb
        $right_right_thumb_image_name = $student->student_id . '_right_thumb' . '.' . $request->bioRightThumb->extension();
        $this->saveImage($request, $right_right_thumb_image_name, "bioRightThumb");
        $student->bio_right_thumb_img_path = $right_right_thumb_image_name;

        //save left thumb image
        $left_thumb_image_name = $student->student_id . '_left_thumb' . '.' . $request->bioLeftThumb->extension();
        $this->saveImage($request, $left_thumb_image_name, "bioLeftThumb");
        $student->bio_left_thumb_img_path = $left_thumb_image_name;

//        //save card image
//        $card_img_path = $student->student_id . '_card' . '.' . $request->card->extension();
//        $this->saveImage($request, $card_img_path, "card");
//        $student->card_img_path = $card_img_path;
    }

    /**
     * @param Request $request
     * @param Model|null $school
     */
    private function savePrintJob(Request $request,$school): void
    {
        if (!PrintJob::whereImei($request->imei)->whereSchoolId($school->id)->first()) {
            $printJob = new PrintJob();
            $printJob->imei = $request->imei;
            $printJob->school_id = $school->id;
            $printJob->pending = true;
            $printJob->completed = false;
            $printJob->save();
        }
    }

    /**
     * @param $school_code
     * @param $status
     */
    private function changeSchoolStatus($school_code, $status): void
    {
        $url = "https://dextraclass.com/api/sid/schools/update-school-details-by-code";
        \Http::withToken(self::XTRA_CLASS_TOKEN)
            ->post($url, ["school_code" => $school_code, "sid_status" => $status]);
    }

    /**
     * @param $school_code
     */
    public function generateStudentId($school_code,$class)
    {
        $defaultYear = 18;
        $year = Student::GetStudentEnrollmentYear($class);
        $old_id_string = $defaultYear.$school_code;
        $id_string = $year.$school_code;
        $school = School::whereSchoolCode($school_code)->first();
        if ($school && $school->last_student_id != null){
            $school_code_and_index = substr($school->last_student_id,strpos($school->last_student_id,$school_code));
            $id_part_to_increment = str_replace($school_code,"",$school_code_and_index);
            $new_id = $year . $school_code . (intval($id_part_to_increment) + 1);
            $school->update(["last_student_id" => $new_id]);
            return ["id"=> $new_id, "sample_object" => ""];
        }

        $url = APIController::XTRA_CLASS_BASE_URL."$school_code";
        $token = self::XTRA_CLASS_TOKEN;
        $response = \Http::withToken($token)->get($url);

        $students =  $response["data"];
        $studentIds = [];
        $studentCollectionObject = new \Illuminate\Support\Collection();

        foreach ($students as $key => $student){
            if(strpos($student["info"]["shs_number"],$school_code)){
                $studentIds[$key] = ($student["info"]["shs_number"]);
                $studentCollectionObject->push($student);
            }
        }
        $studentIdsReversed = array_reverse($studentIds);
        $new_id_number = ($studentIdsReversed[0]);

         $id_part_to_increment = intval(str_replace($old_id_string, "",$new_id_number));
         $generated = $id_string.($id_part_to_increment+1);
         $school->update(["last_student_id" => $generated]);

        return ["id"=> $generated, "sample_object" => $studentCollectionObject[0]];
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function studentIdIsEmpty(Request $request): bool
    {
        return empty($request->studentId);
    }

    public function submitEvent(){
        $options = array('cluster' => env('PUSHER_APP_CLUSTER'), 'encrypted' => true);
        $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), $options);
        $pusher->trigger('submission-channel', 'App\\Events\\NewSubmissionEvent',[]);
    }

    public function allSchools(){
        $schools = School::selectRaw("id,school_name")->orderBy("school_name","ASC")->get();
        return response()->json(["data" => $schools]);
    }

    public function updateStudent(Request $request, $student_id){

        Student::find($student_id)->update([
            "first_name" =>  $request->name,
            "last_name" =>  $request->name,
            "dob"   => $request->dob,
            "gender" => $request->sex,
            "guardian_contact_number" => $request->guardian_contact_number
        ]);

        $card_id = $request->card_id;
        \Http::get("https://sid.desafrica.com/image/$card_id");
    }

    public function schoolProjectOfficers($school_code){
        $officer = ProjectOfficer::whereHas("school",function ($q) use($school_code){
            $q->where("school_code",$school_code);
        })->first();
        return response()->json(["data" => $officer]);
    }
}
