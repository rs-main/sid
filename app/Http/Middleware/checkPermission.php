<?php

namespace App\Http\Middleware;

use App\Exceptions\ExpiredUserException;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class checkPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->user()->access_expiry_time != null){
            $user_access_date = Carbon::createFromFormat('Y-m-d H:i:s', $request->user()->access_expiry_time);
            $current_date_time = Carbon::now();
            $expired = $current_date_time->gt($user_access_date);
            if ($expired){
                throw ExpiredUserException::userExpired();
            }
        }

        if ($request->user()->active != 1){
                throw ExpiredUserException::userActive();
        }

        return $next( $request );
    }
}
