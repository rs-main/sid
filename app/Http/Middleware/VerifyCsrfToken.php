<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        '/api/add-new-account',
        '/api/add-new-project',
        '/api/update-user-status/*',
        '/api/update-student/*',
        '/api/update-user/*',
        '/api/remove-user/*',
        '/api/add-new-project-officer'
    ];
}
