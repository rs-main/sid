<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SchoolSetUpResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"            => $this->id,
            "schoolCode"    => $this->school_code,
            "contactPerson" => $this->contactPerson ? $this->contactPerson->name : "",
            "contactNumber" => $this->contactPerson? $this->contactPerson->phone : "",
            "status"        => $this->status,
            "card_count"        => 0,
            "isSchoolSetupAlready" => false
        ];
    }
}
