<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "cardId" => $this->card_id,
            "firstName" => $this->first_name,
            "lastName" => $this->last_name,
            "dob" => $this->dob,
            "gender" => $this->gender,
            "guardianPhone" => $this->guardian_contact_number,
            "profileImg" => $this->profile_img_path,
            "leftForeFingure" => $this->bio_left_fore_img_path,
            "rightForeFingure" => $this->bio_right_fore_img_path,
            "leftThumbFingure" => $this->bio_left_thumb_img_path,
            "rightThumbFingure" => $this->bio_right_thumb_img_path,
            "cardImg" => $this->card_img_path,
            "createdAt" => $this->created_at,
            "updatedAt" => $this->updated_at,
        ];
    }
}
