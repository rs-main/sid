<?php

namespace App\Traits;

use GraphQL\Exception\QueryError;
use GraphQL\QueryBuilder\QueryBuilder;
use GraphQL\RawObject;
use GuzzleHttp\Client;

trait ConsumeExternalService
{

    private $endPoint;

    private $graphql_query;

    private $baseUri;
    /**
     * Send request to any service
     * @param $method
     * @param $requestUrl
     * @param array $formParams
     * @param array $headers
     *
     */
    public function performRequest($method, $requestUrl, $formParams = [], $headers = [])
    {
        $client = new Client([
            'base_uri'  =>  "http://sid.desafrica.com/",
//            'base_uri'  =>  $this->baseUri,
        ]);


        $response = $client->request($method, $requestUrl, [
            'form_params' => $formParams,
            'headers'     => $headers,
        ]);

        return json_decode($response->getBody(), true);
    }

    public function performGraphQlRequest( array $variables = [], ?string $token = null)
    {
       try {
             return $this->graphqlQuery( $variables, $token);
        } catch (\ErrorException $e) {
            return response()->json(["message" => "failed!", $e->getTraceAsString()],500);
        }
    }

    private function graphqlQuery( array $variables = [], ?string $token = null): array
    {
        $headers = ['Content-Type: application/json'];
        if (null !== $token) {
            $headers[] = "Authorization: bearer $token";
        }

        if (false === $data = @file_get_contents($this->endPoint, false, stream_context_create([
                'http' => [
                    'method' => 'POST',
                    'header' => $headers,
                    'content' => json_encode(['query' => $this->graphql_query, 'variables' => $variables]),
                ]
            ]))) {
            $error = error_get_last();
            throw new \ErrorException($error['message'], $error['type']);
        }

        return json_decode($data, true);
    }

    public function graphqlQueryWithVariables($builder, $variablesArray){

        $client = new \GraphQL\Client(
            $this->endPoint,
            []  // Replace with array of extra headers to be sent with request for auth or other purposes
        );

        // Run query to get results
        try {

            $results = $client->runQuery($builder, true, $variablesArray);
        }
        catch (QueryError $exception) {

            // Catch query error and desplay error details
            print_r($exception->getErrorDetails());
            exit;
        }

        // Display original response from endpoint
        return json_decode($results->getResponseBody(),true);
    }
}
