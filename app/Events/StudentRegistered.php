<?php

namespace App\Events;

use App\Models\Student;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StudentRegistered implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $student_id_object;
    public $student_sample_object;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($student_sample_object,Student $student_id_object)
    {
        $this->student_id_object = $student_id_object;
        $this->student_sample_object = $student_sample_object;
    }


    public function broadcastOn()
    {
        return ["student-registered-channel"];

    }

    public function broadcastAs()
    {
        return 'student-registered';
    }
}
