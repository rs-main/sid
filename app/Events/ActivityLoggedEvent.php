<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class ActivityLoggedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $logMessage;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($logMessage)
    {
        $this->logMessage = $logMessage;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ["activity-logged-channel"];

    }

    public function broadcastWith()
    {
        return [
            'logMessage'    =>  $this->logMessage,
            'userId'      => Auth::id(),
        ];
    }

    public function broadcastAs()
    {
        return 'activity-logged';
    }
}
