<?php

namespace App\Repositories\ProjectOfficer;


use App\Models\ProjectOfficer;
use App\Models\User;
use Illuminate\Http\Request;
use App\Repositories\BaseRepository;
use App\Repositories\EloquentRepository;
use Webpatser\Uuid\Uuid;

class EloquentProjectOfficer extends EloquentRepository implements BaseRepository, ProjectOfficerRepository
{
	protected $model;

	public function __construct(ProjectOfficer $projectOfficer)
	{
		$this->model = $projectOfficer;
	}

	public function all()
	{
    		return $this->model->get();
	}

    public function store(Request $request)
    {
        return parent::store($request);
    }

    public function attachUserToProjectOfficer(User $user)
    {
        $this->model->save([
            "id" => Uuid::generate(),
            "project_id" => 1,
            "full_name" => $user->name,
            "user_id"=> $user->id
        ]);
    }

    public function update(Request $request, $id)
    {
        return parent::update($request, $id);
    }


	public function destroy($projectOfficer)
	{
        if(! $projectOfficer instanceof User)
            $projectOfficer = parent::findTrash($projectOfficer);

        return $projectOfficer->forceDelete();
	}

}
