<?php

namespace App\Repositories;

use Illuminate\Http\Request;

abstract class EloquentRepository
{

	public function all()
	{
		return $this->model->get();
	}


	public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function findBy($filed, $value)
	{
		return $this->model->where($filed, $value)->first();
	}


    public function store(Request $request)
    {
        // return $this->model->fill($request->all())->save();
        return $this->model->create($request->all());
    }

    public function update(Request $request, $id)
    {
        $model = $this->model->findOrFail($id);
        $model->update($request->all());
        return $model;
    }

	public function destroy($id)
	{
		return $this->model->onlyTrashed()->findOrFail($id)->forceDelete();
	}
}