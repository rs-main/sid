<?php

namespace App\Repositories\User;


use App\Models\User;
use Illuminate\Http\Request;
use App\Repositories\BaseRepository;
use App\Repositories\EloquentRepository;
use Illuminate\Support\Facades\Hash;

class EloquentUser extends EloquentRepository implements BaseRepository, UserRepository
{
	protected $model;

	public function __construct(User $user)
	{
		$this->model = $user;
	}

	public function all()
	{
    		return $this->model->get();
	}

    public function find($id): User
    {
        return $this->model->find($id);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        return $user;
    }

    public function update(Request $request, $id)
    {
        $user = parent::update($request, $id);
        return $user;
    }

    public function updateUserWithPassword($request, $role, $imageName, $user){
        $this->model->update([
            "name" => $request->name,
            "email" => $request->email,
            "password"   => $request->filled("password") ? Hash::make($request->password) : $user->password,
            "access_expiry_time"   => $request->filled("access_expiry_time")  ?  $request->access_expiry_time : null,
            "profile_photo_path" => $request->has("image") ? $imageName : "",
            "theme" => $role->name == "Super Admin" ? "Dark" : "Light"
        ]);
    }

    public function saveImage($user, $imageName){
        $this->model->find($user->id)->update([
            "profile_photo_path" => $imageName
        ]);
    }

    public function saveTheme($user,$role){
	    $this->model->find($user->id)->update([
            "theme" => $role->name == "Super Admin" ? "Dark" : "Light"
        ]);
    }

	public function destroy($user)
	{
        if(! $user instanceof User)
            $user = parent::findTrash($user);

        return $user->forceDelete();
	}

}
