<?php

namespace App\Repositories\Role;

use Illuminate\Http\Request;
use App\Repositories\BaseRepository;
use App\Repositories\EloquentRepository;
use Spatie\Permission\Models\Role;
use Webpatser\Uuid\Uuid;

class EloquentRole extends EloquentRepository implements BaseRepository, RoleRepository
{
	protected $model;

	public function __construct(Role $role)
	{
		$this->model = $role;
	}

	public function all()
	{
    		return $this->model->get();
	}

    public function store(Request $request)
    {
        return parent::store($request);
    }

    public function update(Request $request, $id)
    {
        return parent::update($request, $id);
    }


	public function destroy($role)
	{
        if(! $role instanceof Role)
            $role = parent::findTrash($role);

        return $role->forceDelete();
	}

}
