<?php

namespace App\Providers;

use App\Events\ActivityLoggedEvent;
use App\Events\PrintJobDownloadEvent;
use App\Events\StudentRegistered;
use App\Listeners\ActivityLoggedEventListener;
use App\Listeners\PrintDownloadedListener;
use App\Listeners\StudentRegisteredListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        ActivityLoggedEvent::class => [
            ActivityLoggedEventListener::class
        ],

        StudentRegistered::class => [
            StudentRegisteredListener::class
        ],

//        PrintJobDownloadEvent::class => [
//            PrintDownloadedListener::class
//        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
