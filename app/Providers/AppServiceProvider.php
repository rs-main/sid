<?php

namespace App\Providers;

use App\Events\PrintJobDownloadEvent;
use App\Jobs\InitializePrintJobsDataJob;
use App\Jobs\InitializeStudentsDataJob;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Queue::after(function (JobProcessed $event) {
//            PrintJobDownloadEvent::dispatch();
            // $event->connectionName
//             $event->job;
            // $event->job->payload()
        });
    }
}
