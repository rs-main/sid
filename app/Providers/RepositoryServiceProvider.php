<?php

namespace App\Providers;

use App\Repositories\ProjectOfficer\EloquentRole;
use App\Repositories\ProjectOfficer\ProjectOfficerRepository;
use App\Repositories\User\EloquentUser;
use App\Repositories\User\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    public function register()
    {
//        $this->app->singleton(
//            PositionRepository::class,
//            EloquentPosition::class
//        );

        $this->app->singleton(
            UserRepository::class,
            EloquentUser::class
        );

        $this->app->singleton(
            ProjectOfficerRepository::class,
            EloquentRole::class
        );
//
//        $this->app->singleton(
//            VoteRepository::class,
//            EloquentVote::class
//        );
//
//        $this->app->singleton(
//            UserVoteRepository::class,
//            EloquentUserVoteTracker::class
//        );

    }

}
