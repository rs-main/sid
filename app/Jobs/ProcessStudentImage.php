<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessStudentImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private  $request;
    private $image_name;
    private $field;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request, $image_name, $field)
    {
        $this->request = $request;
        $this->image_name = $image_name;
        $this->field = $field;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->saveImage($this->request,$this->image_name, $this->field);
    }

    /**
     * @param Request $request
     * @param string $image_name
     * @param string $field
     */
    private function saveImage(Request $request, string $image_name, $field): void
    {
        $request->$field->move(storage_path('images'), $image_name);
    }
}
