<?php

namespace App\Jobs;

use App\Models\Student;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CaptureCardJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $cardId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($card_id)
    {
        $this->cardId = $card_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Student::captureStudentCard($this->cardId);
    }
}
