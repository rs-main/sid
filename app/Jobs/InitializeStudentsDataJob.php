<?php

namespace App\Jobs;

use App\Models\PrintJob;
use App\Models\School;
use App\Models\Student;
use App\Traits\ConsumeExternalService;
use GraphQL\QueryBuilder\QueryBuilder;
use GraphQL\RawObject;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InitializeStudentsDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,ConsumeExternalService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->endPoint = "https://staging.api.desafrica.com/v1";
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->printJobStudents();
    }

    private function printJobStudents(){
        PrintJob::with("school")->chunk(10, function ($jobs) {
            foreach ($jobs as $job) {
                $builder = $this->getStudentsQuery();
                $school_code = $job->school->school_code;
                $variablesArray = ['schoolCode' => $school_code, 'imei' => $job->imei];
                $students = $this->graphqlQueryWithVariables($builder,$variablesArray)["data"]["getPrintJobDetails"]["students"];
                $student_object = new Student();
                $student_object->insertStudentsWithImage($students);
//                Student::insertStudents($students);
            }
        });
    }

    private function getStudentsQuery(){
        $builder = (new QueryBuilder('getPrintJobDetails'))
            ->setVariable('schoolCode', 'String',true)
            ->setVariable('imei', 'String',true)
            ->setArgument('input',
                new RawObject('{ schoolCode: $schoolCode , imei: $imei }'
                ))->selectField(
                (new QueryBuilder('students'))
                    ->selectField('firstName')
                    ->selectField('lastName')
                    ->selectField('cardId')
                    ->selectField('schoolName')
                    ->selectField('studentId')
                    ->selectField('class')
                    ->selectField('bio')
                    ->selectField('imei')
                    ->selectField('projectOfficerName')
                    ->selectField('date')
                    ->selectField('serialStart')
                    ->selectField('serialEnd')
                    ->selectField('photo')
                    ->selectField('guardianContactNumber')
                    ->selectField('gender')
            );


        return $builder;
    }

}
