<?php

namespace App\Jobs;

use App\Models\Student;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use PDF;

class GeneratePDFJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $imei;

    private $school_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($imei, $school_id)
    {
        $this->imei = $imei;
        $this->school_id = $school_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $collection = $this->generatePDFs($this->imei, $this->school_id);
        $this->zipGeneratedCards($this->imei, $collection);
    }

    /**
     * @param $imei
     * @param Collection $collection
     */
    public function zipGeneratedCards($imei, Collection $collection): void
    {
        $zip = new \ZipArchive();
        $school_id = $this->school_id;
        if ($zip->open("app/$imei"."_$school_id.zip", \ZipArchive::CREATE) === TRUE) {

            foreach ($collection as $file) {
                $zip->addFile("app/" . $file);
            }

            $zip->close();
        }

    }

    /**
     * @param $imei
     * @param $school_id
     * @return Collection
     */
    private function generatePDFs($imei, $school_id): Collection
    {
        $students = Student::whereImei($imei)->whereSchoolId($school_id)
            ->get();
        $collection = new Collection();

        foreach ($students as $student) {
//            $file_name = $student->card_id . "_" . time();
            $file_name = $student->card_id;
            $collection->push("$file_name.pdf");
            $customPaper = array(0,0,578,791);
//            $customPaper = array(0,0,638,791);
            $pdf = PDF::loadView("admin.card", compact('student'))
                ->setPaper($customPaper, 'landscape');
            $content = $pdf->download()->getOriginalContent();
            \Storage::disk('main_public')->put($file_name . '.pdf', $content);
            Student::whereId($student->id)->update([
                "generated" => true
            ]);
        }
        return $collection;
    }

    public function deleteFiles(){
        $file_pointer = "gfg.txt";
        unlink($file_pointer);
    }
}
