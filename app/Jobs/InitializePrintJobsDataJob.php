<?php

namespace App\Jobs;

use App\Models\PrintJob;
use App\Traits\ConsumeExternalService;
use GraphQL\QueryBuilder\QueryBuilder;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InitializePrintJobsDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,ConsumeExternalService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->endPoint = "https://staging.api.desafrica.com/v1";
        $this->graphql_query = "query {
                                  getPrintJobs {
                                    jobs {
                                      schoolCode
                                      schoolName
                                      submitDate
                                      pendingJobs {
                                        imei
                                      }
                                      completedJobs {
                                        imei
                                      }
                                    }
                                  }
                                }";
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        $jobs = $this->performGraphQlRequest();
//        $data =  $jobs["data"]["getPrintJobs"]["jobs"];
        $builder = (new QueryBuilder('getPrintJobs'))
            ->selectField(
                (new QueryBuilder('jobs'))
                    ->selectField('schoolCode')
                    ->selectField('schoolName')
                    ->selectField('submitDate')
                    ->selectField((new QueryBuilder("pendingJobs"))
                        ->selectField("imei"))
                    ->selectField((new QueryBuilder("completedJobs"))
                        ->selectField("imei"))
            );
        $data = $this->graphqlQueryWithVariables($builder,[])["data"]["getPrintJobs"]["jobs"];
        PrintJob::insertData($data);
    }
}
