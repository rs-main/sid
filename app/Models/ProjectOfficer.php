<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProjectOfficer
 *
 * @property int $id
 * @property string|null $full_name
 * @property string $project_id
 * @property string|null $project_location
 * @property string|null $phone_number
 * @property int $user_id
 * @property string|null $address
 * @property string|null $id_type
 * @property string|null $id_no
 * @property string|null $expiry_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer whereExpiryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer whereIdNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer whereIdType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer whereProjectLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $added_by_user_id
 * @property-read \App\Models\School $school
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectOfficer whereAddedByUserId($value)
 */
class ProjectOfficer extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        "id" => "string"
    ];

    public function school(){
        return $this->belongsTo(School::class);
    }
}
