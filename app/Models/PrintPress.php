<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PrintPress
 *
 * @property int $id
 * @property string $name
 * @property string $location
 * @property string $officer
 * @property string $officer_phone
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPress query()
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPress whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPress whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPress whereOfficer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPress whereOfficerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPress whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $press_report_id
 * @property-read \App\Models\PrintPressReport|null $report
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPress wherePressReportId($value)
 */
class PrintPress extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function report(){
        return $this->hasOne(PrintPressReport::class);
    }
}
