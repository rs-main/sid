<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ContactPerson
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson query()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $name
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson whereUpdatedAt($value)
 */
class ContactPerson extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = "contact_persons";

    protected $casts = ["id" => "string"];
}
