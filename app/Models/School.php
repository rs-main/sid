<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\School
 *
 * @property int $id
 * @property string $school_code
 * @property string $school_name
 * @property string|null $submit_date
 * @property string|null $press
 * @property string|null $quality_officer
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|School newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|School newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|School query()
 * @method static \Illuminate\Database\Eloquent\Builder|School whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|School whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|School wherePress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|School whereQualityOfficer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|School whereSchoolCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|School whereSchoolName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|School whereSubmitDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|School whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PrintJob[] $printJobs
 * @property-read int|null $print_jobs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $student
 * @property-read int|null $student_count
 * @property string|null $project_id
 * @property string|null $project_officer_id
 * @property string $contact_person_id
 * @method static \Illuminate\Database\Eloquent\Builder|School whereContactPersonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|School whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|School whereProjectOfficerId($value)
 * @property string $status
 * @property-read \App\Models\ContactPerson|null $contactPerson
 * @method static \Illuminate\Database\Eloquent\Builder|School whereStatus($value)
 * @property string|null $last_student_id
 * @property-read \App\Models\ProjectOfficer|null $projectOfficer
 * @method static \Illuminate\Database\Eloquent\Builder|School whereLastStudentId($value)
 */
class School extends Model
{
    use HasFactory;

    public const STARTED   = "started";
    public const SUBMITTED = "submitted";
    public const DELIVERED = "delivered";
    public const PRINTED   = "printed";

    protected $guarded = [];

    public function printJobs(){
        return $this->hasMany(PrintJob::class,'school_id');
    }

    public function student(){
        return $this->hasMany(Student::class);
    }

    public function contactPerson(){
        return $this->belongsTo(ContactPerson::class,"contact_person_id");
    }

    public function projectOfficer(){
        return $this->belongsTo(ProjectOfficer::class);
    }

    public static  function getSchoolByName($school_name){
        return $school = static::whereSchoolName($school_name)->first();
    }

    public static function GetId($code){
        $school = self::whereSchoolCode($code)->selectRaw("id")->first();
        return $school ? $school->id: null;
    }

    public static function SaveSchool($request){
//        self::create(
//            $request
//        )
    }
}
