<?php

namespace App\Models;

use App\Traits\ConsumeExternalService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Milon\Barcode\DNS2D;
use Spatie\Browsershot\Browsershot;
use Webpatser\Uuid\Uuid;

/**
 * App\Models\Student
 *
 * @property int $id
 * @property string $student_id
 * @property string $card_id
 * @property string $first_name
 * @property string $last_name
 * @property int $school_id
 * @property int $class
 * @property int $bio
 * @property string|null $qr_code
 * @property string|null $photo
 * @property string $imei
 * @property string|null $project_officer_name
 * @property string $date
 * @property string|null $serial_start
 * @property string|null $serial_end
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Student newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Student newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Student query()
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereBio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereImei($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereProjectOfficerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereQrCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereSchoolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereSerialEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereSerialStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\School $school
 * @property string|null $guardian_contact_number
 * @property string|null $issue_date
 * @property string|null $gender
 * @property int $generated
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereGenerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereGuardianContactNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereIssueDate($value)
 * @property string $project_officer_id
 * @property string $project_id
 * @property string $class_id
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereProjectOfficerId($value)
 * @property string|null $dob
 * @property string|null $status
 * @property string|null $bio_right_thumb_img_path
 * @property string|null $bio_left_thumb_img_path
 * @property string|null $bio_right_fore_img_path
 * @property string|null $bio_left_fore_img_path
 * @property string|null $profile_img_path
 * @property string|null $card_img_path
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereBioLeftForeImgPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereBioLeftThumbImgPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereBioRightForeImgPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereBioRightThumbImgPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereCardImgPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereDob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereProfileImgPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereStatus($value)
 * @property int $new
 * @property-read \App\Models\ProjectOfficer $officer
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereNew($value)
 */
class Student extends Model
{
    use HasFactory;


    protected $guarded =[];
    protected $table = "students";

    public function school(){
        return $this->belongsTo(School::class);
    }

    public function officer(){
        return $this->belongsTo(ProjectOfficer::class);
    }

    public static function getStudent($studentId){
        return static::whereStudentId($studentId)->first();
    }

    public function getTotalCount(){
        return static::count();
    }

    public static function createStudent($studentArray){
        Student::create([
            "student_id" => $studentArray["studentId"],
            "card_id" => $studentArray["cardId"],
            "first_name" => $studentArray["firstName"],
            "last_name" => $studentArray["lastName"],
            "school_id" => $studentArray["schoolId"],
            "class" => $studentArray["class"],
            "bio" => $studentArray["bio"],
            "imei" => $studentArray["imei"],
            "date" => $studentArray["date"],
            "gender" => $studentArray["gender"],
            "guardian_contact_number" => $studentArray["guardianContactNumber"],
            "project_officer_name" => $studentArray["projectOfficerName"],
            "serial_start" => $studentArray["serialStart"],
            "serial_end" => $studentArray["serialEnd"],
            "qr_code" => $studentArray["cardId"] . ".png",
            "photo" => $studentArray["photo"],
            "generated" => true
        ]);
    }

    public function insertStudentsWithImage($students_array){

        if (sizeof($students_array) != $this->getTotalCount()) {
            foreach ($students_array as $studentArray) {
                $student_object = self::getStudent($studentArray["studentId"]);
                $school = School::getSchoolByName($studentArray["schoolName"]);
                $studentArray["schoolId"] = $school? $school->id : 0;

                if (!$student_object) {
                    self::saveStudentQRCode($studentArray);
                    self::createStudent($studentArray);
                    self::captureStudentCard($studentArray["cardId"]);
                } else {
                if(!$student_object->generated) {
                    $student_object->update([
                        "photo" => $studentArray["photo"]
                    ]);
                    $this->captureStudentCard($studentArray["cardId"]);
                    }
                }
            }
        }
    }

    public static function insertStudents($students_array){

        foreach ($students_array as $studentArray) {
            $student_object = Student::whereStudentId($studentArray["studentId"])->first();
            if (!$student_object) {
                $student = new Student();
                $student->student_id = $studentArray["studentId"];
                $student->card_id = $studentArray["cardId"];
                $student->first_name = $studentArray["firstName"];
                $student->last_name = $studentArray["lastName"];
                $school = School::whereSchoolName($studentArray["schoolName"])->first();
                $student->school_id = $school ? $school->id : 0;
                $student->class = $studentArray["class"];
                $student->bio = $studentArray["bio"];
                $student->imei = $studentArray["imei"];
                $student->date = $studentArray["date"];
                $student->gender = $studentArray["gender"];
                $student->guardian_contact_number = $studentArray["guardianContactNumber"];
                $student->project_officer_name = $studentArray["projectOfficerName"];
                $student->serial_start = $studentArray["serialStart"];
                $student->serial_end = $studentArray["serialEnd"];
                $qr_code_string = $student->first_name."\n". $student->school->school_name."\n".$student->card_id;
                $dns2d = new DNS2D();
                \Storage::disk('public')->put($student->card_id . ".png", base64_decode($dns2d->getBarcodePNG($qr_code_string, 'QRCODE')));
                $student->qr_code = $student->card_id . ".png";
                $student->photo = $studentArray["photo"];

//                self::saveStudentImage($student->card_id,$studentArray["photo"]);
                $student->save();
            }else{
                $student_object->update([
                    "photo" => $studentArray["photo"]
                ]);
            }
        }
    }

    public static function getStudentExpiry($class){
        $year = 0;
        switch ($class){
            case 3:
                $year = Carbon::now()->year+1;
                break;
            case 2:
                $year = Carbon::now()->year+2;
                break;
            case 1:
                $year = Carbon::now()->year+3;
                break;
        }

        return $year;
    }

    public static function saveStudentImage($student_card_id,$url){
        $contents = file_get_contents($url);
        $name = $student_card_id;;
        \Storage::disk('public')->put($name, $contents);
    }

    public static function captureStudentCard($card_id){

        if (file_exists(public_path()."/$card_id.png")){
            $file_pointer = public_path()."/$card_id.png";
            unlink($file_pointer);
        }

        Browsershot::url("https://sid.desafrica.com/card/$card_id")
            ->windowSize(1050, 660)
            ->save("$card_id.png");
    }

    public static function saveStudentQRCode($studentArray){
        $qr_code_string = $studentArray["firstName"] . "\n" .
            $studentArray["schoolName"].
            "\n" . $studentArray["cardId"];
        $qr_code_name = $studentArray["cardId"] . "_qr_code.png";
        $dns2d = new DNS2D();
        \Storage::disk('public')->put($qr_code_name, base64_decode($dns2d->getBarcodePNG($qr_code_string, 'QRCODE')));
        return $qr_code_name;
    }

    public static function GetStudentEnrollmentYear($class): int
    {
        $year = 0;
        switch ($class){
            case "3":
                $year = 18;
                break;
            case "2":
                $year = 19;
                break;
            case "1":
                $year = 20;
                break;
        }

        return $year;
    }
}
