<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PrintPressReport
 *
 * @property int $id
 * @property int $print_press_id
 * @property int $jobs
 * @property int $downloaded
 * @property int $printed
 * @property int $delivered
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPressReport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPressReport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPressReport query()
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPressReport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPressReport whereDelivered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPressReport whereDownloaded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPressReport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPressReport whereJobs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPressReport wherePrintPressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPressReport wherePrinted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintPressReport whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PrintPressReport extends Model
{
    use HasFactory;

    protected $guarded = [];
}
