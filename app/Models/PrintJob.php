<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Webpatser\Uuid\Uuid;

/**
 * App\Models\PrintJob
 *
 * @property int $id
 * @property int $school_id
 * @property string $imei
 * @property int $pending
 * @property int $completed
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PrintJob newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PrintJob newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PrintJob query()
 * @method static \Illuminate\Database\Eloquent\Builder|PrintJob whereCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintJob whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintJob whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintJob whereImei($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintJob wherePending($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintJob whereSchoolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrintJob whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $generated
 * @property-read \App\Models\School $school
 * @method static \Illuminate\Database\Eloquent\Builder|PrintJob whereGenerated($value)
 * @property string|null $download_url
 * @method static \Illuminate\Database\Eloquent\Builder|PrintJob whereDownloadUrl($value)
 */
class PrintJob extends Model
{
    use HasFactory;

    protected $guarded = [];

    private static function AddToPressReport(): void
    {
        $print_press_id = PrintPress::first() ? PrintPress::first()->id : null;
        $report = PrintPressReport::wherePrintPressId($print_press_id)->first();
        $report->update([
            "jobs" => $report->jobs + 1
        ]);
    }

    public function school(){
        return $this->belongsTo(School::class);
    }

    public static function simulateJobData($array_data){

        $sample_schools_data = ["Achimota School", "St. John's School","Sammo School"];

        $generated_data = new Collection();

        foreach ($sample_schools_data as $school){

            $array_data["schoolCode"] = "000123";

            $array_data["schoolName"] = $school;

            $array_data["submitDate"] = "";

            $array_data["pendingJobs"] = [
//                ["imei" => Uuid::generate()->string],
                ["imei" => Uuid::generate()->string],
                ["imei" => Uuid::generate()->string]
            ];

            $array_data["completedJobs"] = [
                ["imei" => Uuid::generate()->string],
                ["imei" => Uuid::generate()->string],
                ["imei" => Uuid::generate()->string]
            ];

            $generated_data->push($array_data);
        }

        return $generated_data;
    }

    public static function insertData($array){
        $school_id = 0;
        $print_count = PrintJob::count();
        if (sizeof($array) != $print_count) {
            foreach ($array as $item) {
                $school_object = School::whereSchoolName($item["schoolName"])->first();
                $school_id = $school_object ? $school_object->id : "";
                if (!$school_object) {
                    $school = School::create([
                        "school_name" => $item["schoolName"],
                        "school_code" => $item["schoolCode"],
                        "submit_date" => $item["submitDate"],
                        "quality_officer" => "Mr. Baboni",
                        "press" => "R.S. Press",
                    ]);

                    $school_id = $school->id;
                }

                if (isset($item["pendingJobs"]) && sizeof($item["pendingJobs"]) != 0) {
                    $pending_job_builder = PrintJob::whereSchoolId($school_id)->wherePending(true);
                    $pending_job_counts = $pending_job_builder->count();
                    if (sizeof($item["pendingJobs"]) != $pending_job_counts) {
                        $pending_job_builder->delete();
                        foreach ($item["pendingJobs"] as $pendingJob) {
                            PrintJob::create([
                                "school_id" => $school_id,
                                "imei" => $pendingJob["imei"],
                                "pending" => true,
                                "completed" => false
                            ]);

                            self::AddToPressReport();
                        }
                    }
                }

                if (isset($item["completedJobs"]) && sizeof($item["completedJobs"]) != 0) {
                    $completed_job_builder = PrintJob::whereSchoolId($school_id)->whereCompleted(true);
                    $completed_job_counts = $completed_job_builder->count();
                    if (sizeof($item["completedJobs"]) != $completed_job_counts) {
                        foreach ($item["completedJobs"] as $completedJob) {
                            PrintJob::create([
                                "school_id" => $school_id,
                                "imei" => $completedJob["imei"],
                                "pending" => false,
                                "completed" => true
                            ]);
                            self::AddToPressReport();
                        }
                    }
                }
            }
        }
    }
}
