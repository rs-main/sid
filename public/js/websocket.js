

window.onload =  function() {

    let requestOptions = {
        method: 'GET',
        Accept: 'application/json',
    };

    loadSchools(requestOptions);

    let channel = pusher.subscribe('submission-channel');
    channel.bind('App\\Events\\NewSubmissionEvent', function(data) {
        loadSchools(requestOptions);
    });

    function loadSchools(requestOptions) {
        fetch('https://dextraclass.com/api/sid/schools/all-schools?list=all', requestOptions)
            .then(response => response.json())
            .then(function (school_data) {
                const controlCenterKeys = school_data.keys;
                const full = school_data.keys;

                const map_features = school_data.data.map((data) => ({
                    position: new google.maps.LatLng(data.lat, data.lng),
                    status: data.sid_status,
                    school_name: data.school_name,
                    population: {
                        "shsOne": data.form_one_count,
                        "shsTwo": data.form_two_count,
                        "shsThree": data.form_three_count
                    },
                    logo: data.school_logo,
                    locationName: data.location,
                    schoolCode: data.school_code
                }));

                initMap(map_features);

                controlCenterObj.setProgressKeys(controlCenterKeys, full);
                controlCenterObj.getSchools();
            });
    }
}
