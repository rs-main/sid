
let projectManagementClassObj = {
    jsId : {
        panelHtml             : '#panelHtml',
        ProjectsDiv           : '#ProjectsDiv',
        SchoolsDiv            : '#SchoolsDiv',
        PrintPressDiv         : '#PrintPressDiv',
        ProjectOfficersDiv    : '#ProjectOfficersDiv',
        QualityOfficersDiv    : '#QualityOfficersDiv',
        managementPageHeader  : '#management-page-header',

        //Add new Project
        newProjectName        : "#NewProjectName",
        newCardTitle          : "#NewCardTitle",
        newInstitutionType    : '#NewInstitutionType',
        newFirstName          : "#NewFirstName",
        newSecondName         : "#NewSecondName",
        newMiddleName         : '#NewMiddleName',
        newIssueDate          : "#NewIssueDate",
        newIdNo               : "#NewIdNo",
        newQrCode             : "#NewQrCode",
        newProjectDurationStartDate : '#NewProjectDurationStartDate',
        newProjectDurationEndDate : '#NewProjectDurationEndDate',
        newCardSize           : '#NewCardSize',

        //Add new Press
        newPressName          : "#NewPressName",
        newPressGpsCode       : "#NewPressGpsCode",
        newPressLocation      : "#NewPressLocation",
        newPressContactName   : "#NewPressContactName",
        newPressContactNo     : '#NewPressContactNo',


        //Assign Institution to Project Officer
        assignProjectOfficerSchoolNameHTML : '#AssignProjectOfficerSchoolNameHTML',
        assignProjectOfficerNameSelect     : '#AssignProjectOfficerNameSelect',
        assignProjectOfficerSchoolCode     : '#AssignProjectOfficerSchoolCode',

        //Add Task to Project Officer Name
        assignProjectOfficerInstSelect     : '#AssignProjectOfficerInstSelect',
        assignProjectOfficerStartDate      : '#AssignProjectOfficerStartDate',
        assignProjectOfficerEndDate        : '#AssignProjectOfficerEndDate',
        assignProjectOfficerId             : '#AssignProjectOfficerId',

        //Assign Print Officer to Press
        assignPrintOfficerPressNameHTML    : '#AssignPrintOfficerPressNameHTML',
        assignPrintOfficerNameSelect       : '#AssignPrintOfficerNameSelect',
        assignPrintOfficerPressId          : '#AssignPrintOfficerPressId',
        assignPrintOfficerNameSelect       : '#AssignPrintOfficerNameSelect',

        //Add new school
        newSchoolName         : '#NewSchoolName',
        newSchoolPopulation   : '#NewSchoolPopulation',

        //Add new Project Officer
        newProjectOfficerName                       : '#NewProjectOfficerName',
        newProjectOfficerAssignedProject            : '#NewProjectOfficerAssignedProject',
        newProjectOfficerAssignedProjectLocation    : '#NewProjectOfficerAssignedProjectLocation',
        newProjectOfficerAssignedProjectIdOne       : '#NewProjectOfficerAssignedProjectIdOne',
        newProjectOfficerAssignedProjectIdTwo       : '#NewProjectOfficerAssignedProjectIdTwo',
        newProjectOfficerMobileNo                   : '#NewProjectOfficerMobileNo',
        newProjectOfficerPassword                   : '#NewProjectOfficerPassword',
        newProjectOfficerConfirmPassword            : '#NewProjectOfficerConfirmPassword',
        newProjectOfficerResidentialAddress         : '#NewProjectOfficerResidentialAddress',
        newProjectOfficerIDType                     : '#NewProjectOfficerIDType',
        newProjectOfficerIDNo                       : '#NewProjectOfficerIDNo',
        newProjectOfficerIDExpiryDate               : '#NewProjectOfficerIDExpiryDate',
        newProjectOfficerIdCardImg                  : '#NewProjectOfficerIdCardImg'
    },
    jsData : {
        projects : 'Projects',
        schools : 'Schools',
        projectOfficers : 'Project Officers',
        printPress : 'Print Press',
        qualityOfficers : 'Quality Officers',
        bgImg : '',
        datePickerAssignProjectOfficerStartDate : '',
        datePickerAssignProjectOfficerEndDate : '',
        location : {
          lat : '',
          lng : ''
        }

    }
}

class projectManagementClass {

    constructor(external) {
        this.ext = external;

    }



    async assignOfficerToPress() {

      var ids = this.ext.jsId;

      var pressId = $(ids.assignPrintOfficerPressId).val();
      var printOfficerId = $(ids.assignPrintOfficerNameSelect).val();



      if(pressId != '' && printOfficerId != '') {

        const response = await fetch('https://staging.api.desafrica.com/v1', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ query: `
            mutation AssignOfficerToPress($input : AssignOfficerToPressInput!) {
                sid {
                  assignOfficerToPress(input : $input) {
                    id
                    pressId
                    printOfficerId
                  }
              }
            }
            ` ,
            variables : {
              "input": {
                "pressId": pressId,
                "printOfficerId": printOfficerId,
              }
            }
        }),

        });

        await response.json()
        .then(data => {
          console.log(data)
          if(data.data.sid != null) {
            toastr.success('Print Officer Assigned Successfully');
          }
          else {
            toastr.error('Error: Failed to Assign Print Officer');
          }

        });

     }





  }


    async addTaskToProjectOfficerName() {

      var ids = this.ext.jsId;

      var startDate = $(ids.assignProjectOfficerStartDate).val();
      var endDate = $(ids.assignProjectOfficerEndDate).val();
      var institutionId = $(ids.assignProjectOfficerInstSelect).val();
      var projectOfficerId = +($(ids.assignProjectOfficerId).val());


      if(startDate != '' && endDate != '' && institutionId != '') {

        const response = await fetch('https://staging.api.desafrica.com/v1', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ query: `
            mutation AssignInstitutionToPO($input : POIAssignmentInput!) {
              sid {
                assignInstitutionToPO(input : $input) {
                  id
                }
              }
            }
            ` ,
            variables : {
              "input": {
                "institutionId": institutionId,
                "projectOfficerId": projectOfficerId ,
                "endDate": endDate ,
                "startDate": startDate
              }
            }
        }),

        });

        await response.json()
        .then(data => {

          if(data.data.sid != null) {
            toastr.success('Project Officer Assigned Successfully');
          }
          else {
            toastr.error('Error: Failed to Assign Project Officer');
          }

        });

     }





  }

    async assignInstitutionToPO() {

      var ids = this.ext.jsId;

      var projectOfficerId = +($(ids.assignProjectOfficerNameSelect).val());
      var institutionId = $(ids.assignProjectOfficerSchoolCode).val();
      var startDate = '';
      var endDate = '';

      if(institutionId != '' && projectOfficerId != '') {
        const response = await fetch('https://staging.api.desafrica.com/v1', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ query: `
              mutation AssignInstitutionToPO($input : POIAssignmentInput!) {
                sid {
                  assignInstitutionToPO(input : $input) {
                    id
                  }
                }
              }
              ` ,
              variables : {
                "input": {
                  "institutionId": institutionId,
                  "projectOfficerId": projectOfficerId,
                  "endDate": "01/03/2020",
                  "startDate": "01/03/2020"
                }
              }
          }),
          });

          await response.json()
          .then(data => {
            console.log(data)
            if(data.data.sid != null) {
              toastr.success('Project Officer Assigned Successfully');
            }
            else {
              toastr.error('Error: Failed to Assign Project Officer');
            }

          });
      }




  }

    async getSchools() {

        const response = await fetch('https://staging.api.desafrica.com/v1', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ query: `
        query getSchools {
            getSchools(input: {
              perPage: 10,
              pageNumber: 1
            }){
              schools {
                schoolCode
                schoolName
                population
                projectOfficers {
                  fullName
                }

              }
              total
            }
          }
            ` ,
            variables : {

            }
        }),
        });

        return await response.json();

    }

    async getProjects() {


        const response = await fetch('https://staging.api.desafrica.com/v1', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ query: `
        query getProjects {
            getProjects (input: {
              perPage: 1
              pageNumber: 0
            }) {
              projects {
                id
                name
                startDate
              }
              total
            }
          }
            ` ,
            variables : {

            }
        }),
        });

        return await response.json();

    }

    async getProjectOfficers() {

        const response = await fetch('https://staging.api.desafrica.com/v1', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ query: `
        query getProjectOfficers($input : GetProjectOfficersInput!) {
            getProjectOfficers(input: $input){
              projectOfficers {
                id
                fullName
                phone
                projectId
                photoId
                photoIdNumber
                project {
                  name
                  startDate
                  endDate
                }

              }

            }
          }
            ` ,
            variables : {
                "input":  {
                    "perPage": 20,
                    "pageNumber": 0
                }
            }
        }),
        });

        return await response.json();


    }

    async getPrintOfficers() {

      const response = await fetch('https://staging.api.desafrica.com/v1', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query: `
          query getPrintOfficers($input : GetPrintOfficersInput!) {
            getPrintOfficers(input : $input) {
              printOfficers {
                id
                fullName
              }
            }
          }
          ` ,
          variables : {
              "input":  {
                  "perPage": 20,
                  "pageNumber": 0
              }
          }
      }),
      });

      return await response.json();


  }

    async getPresses() {

        const response = await fetch('https://staging.api.desafrica.com/v1', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ query: `
        query getPresses($input : GetPressesInput!) {
            getPresses(input: $input){
              presses {
                id
                name
                officers {
                  printOfficers {
                    fullName
                  }
                }
              }
            }
          }
            ` ,
            variables : {
                "input":  {
                    "perPage": 20,
                    "pageNumber": 0
                }
            }
        }),
        });

        return await response.json();

    }

    async addPress() {

        var ids = this.ext.jsId;
        var jsData = this.ext.jsData;

        var pressName = $(ids.newPressName).val();
        var gpsCode = $(ids.newPressGpsCode).val();

        var lat = jsData.location.lat;
        var lng = jsData.location.lng;

        var contactName = $(ids.newPressContactName).val();
        var contactNo = $(ids.newPressContactNo).val();

        if(pressName != '' && gpsCode != '' && lat != '' && lng != '' && contactName != '' && contactNo != '') {

            const response = await fetch('https://staging.api.desafrica.com/v1', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ query: `
                mutation AddPress($input :  AddPressInput!) {
                    sid {
                      addPress(input: $input) {
                        id

                      }
                    }
                  }
                    ` ,
                    variables : {
                        "input":  {
                            "name" : `${pressName}`,
                            "gpsCode" : `${gpsCode}`,
                            "location" :{
                                "lat" : `${lat}`,
                                "lng" : `${lng}`,
                            },
                            "contactName" : `${contactName}`,
                            "contactPhone" : `${contactNo}`,
                        },
                    }
                }),
                });

                await response.json()
                .then(data => {
                  console.log(data)
                  if(data.data.sid != null) {
                    toastr.success('Press Added Successfully');
                  }
                  else {
                    toastr.error('Error: Failed to Add Press');
                  }

                });

        }
        else {
            try {
                throw new Error('Check all fields are provided!')
              } catch (e) {
                console.error(e.name + ': ' + e.message)
              }

        }


    }

    async addProject() {

      // var ids = this.ext.jsId;
      // var jsData = this.ext.jsData;
      //
      // var projectName = $(ids.newProjectName).val();
      // var cardTitle = $(ids.newCardTitle).val();
      // var institutionType = $(ids.newInstitutionType).val();
      // var startDate = $(ids.newProjectDurationStartDate).val();
      // var endDate = $(ids.newProjectDurationEndDate).val();
      // var cardSize = $(ids.newProjectCardSize).val();
      // var firstName = $(ids.newFirstName).prop("checked");
      // var secondName = $(ids.newSecondName).prop("checked");
      // var middleName = $(ids.newMiddleName).prop("checked");
      // var issueDate = $(ids.newIssueDate).prop("checked");
      // var idNo = $(ids.newIdNo).prop("checked");
      // var qrCode = $(ids.newQrCode).prop("checked");
      //
      //
      // var bgImg = $(ids.newProjectCardBackImg).val();
      //
      //
      //
      // if(projectName != '' && cardTitle != '' && startDate != '' && endDate != '' && cardSize != '' && bgImg != '') {
      //
      //     const response = await fetch('https://staging.api.desafrica.com/v1', {
      //         method: 'POST',
      //         headers: {
      //           'Content-Type': 'application/json',
      //           'Accept' : 'application/json, text/plain, */*',
      //         },
      //         body: JSON.stringify({ query: `
      //         mutation AddProject($input :  AddProjectInput!) {
      //             sid {
      //               addProject(input: $input) {
      //                 id
      //                 name
      //                 cardTitle
      //                 fields{
      //                   firstName
      //                   lastName
      //                 }
      //                 startDate
      //                 endDate
      //                 cardSize
      //                 cardBgImg
      //                 initials
      //                 year
      //                 closure
      //               }
      //             }
      //           }
      //             ` ,
      //             variables : {
      //                 "input":  {
      //                     "name" : projectName,
      //                     "initials" : "AQ",
      //                     "year" : "2020",
      //                     "closure" : false,
      //                     "cardTitle" : cardTitle,
      //                     "fields" : {
      //                         "firstName" : firstName,
      //                         "lastName" : secondName,
      //                         "middleName" : middleName,
      //                         "issueDate" : issueDate,
      //                         "idNumber" : idNo,
      //                         "qrCode" : qrCode,
      //                     },
      //                     "startDate" : startDate,
      //                     "endDate" : endDate,
      //                     "cardSize" : cardSize,
      //                     "cardBgImg" : bgImg
      //                 }
      //             }
      //         }),
      //         });
      //
      //
      //         await response.json()
      //         .then(data => {
      //           console.log(data)
      //           if(data.data.sid != null) {
      //             toastr.success('Project Added Successfully');
      //           }
      //           else {
      //             toastr.error('Error: Failed to Add Project');
      //           }
      //
      //         });
      // }
      // else {
      //     try {
      //         throw new Error('Check all fields are provided!')
      //       } catch (e) {
      //         console.error(e.name + ': ' + e.message)
      //       }
      //
      // }


  }

  // async addProject() {
  //
  //     var ids = this.ext.jsId;
  //     var jsData = this.ext.jsData;
  //
  //     var projectName = $(ids.newProjectName).val();
  //     var cardTitle = $(ids.newCardTitle).val();
  //     var institutionType = $(ids.newInstitutionType).val();
  //     var startDate = $(ids.newProjectDurationStartDate).val();
  //     var endDate = $(ids.newProjectDurationEndDate).val();
  //     var cardSize = $(ids.newProjectCardSize).val();
  //     var firstName = $(ids.newFirstName).prop("checked");
  //     var secondName = $(ids.newSecondName).prop("checked");
  //     var middleName = $(ids.newMiddleName).prop("checked");
  //     var issueDate = $(ids.newIssueDate).prop("checked");
  //     var idNo = $(ids.newIdNo).prop("checked");
  //     var qrCode = $(ids.newQrCode).prop("checked");
  //
  //
  //     var bgImg = $(ids.newProjectCardBackImg).val();
  //
  //
  //
  //     if(projectName != '' && cardTitle != '' && startDate != '' && endDate != '' && cardSize != '' && bgImg != '') {
  //
  //         const response = await fetch('https://staging.api.desafrica.com/v1', {
  //             method: 'POST',
  //             headers: {
  //               'Content-Type': 'application/json',
  //               'Accept' : 'application/json, text/plain, */*',
  //             },
  //             body: JSON.stringify({ query: `
  //             mutation AddProject($input :  AddProjectInput!) {
  //                 sid {
  //                   addProject(input: $input) {
  //                     id
  //                     name
  //                     cardTitle
  //                     fields{
  //                       firstName
  //                       lastName
  //                     }
  //                     startDate
  //                     endDate
  //                     cardSize
  //                     cardBgImg
  //                     initials
  //                     year
  //                     closure
  //                   }
  //                 }
  //               }
  //                 ` ,
  //                 variables : {
  //                     "input":  {
  //                         "name" : projectName,
  //                         "initials" : "AQ",
  //                         "year" : "2020",
  //                         "closure" : false,
  //                         "cardTitle" : cardTitle,
  //                         "fields" : {
  //                             "firstName" : firstName,
  //                             "lastName" : secondName,
  //                             "middleName" : middleName,
  //                             "issueDate" : issueDate,
  //                             "idNumber" : idNo,
  //                             "qrCode" : qrCode,
  //                         },
  //                         "startDate" : startDate,
  //                         "endDate" : endDate,
  //                         "cardSize" : cardSize,
  //                         "cardBgImg" : bgImg
  //                     }
  //                 }
  //             }),
  //             });
  //
  //
  //             await response.json()
  //             .then(data => {
  //               console.log(data)
  //               if(data.data.sid != null) {
  //                 toastr.success('Project Added Successfully');
  //               }
  //               else {
  //                 toastr.error('Error: Failed to Add Project');
  //               }
  //
  //             });
  //     }
  //     else {
  //         try {
  //             new Error('Check all fields are provided!')
  //             toastr.error('Check all fields are provided!');
  //           } catch (e) {
  //             console.error(e.name + ': ' + e.message)
  //           }
  //
  //     }
  //
  //
  // }



    showHideDiv(div) {
        var jsId = this.ext.jsId;
        var jsData = this.ext.jsData;



        if(div == jsId.ProjectsDiv) {
          if(window.dataTables.projects) {
              $(jsId.ProjectsDiv).fadeIn(300);
              $(jsId.SchoolsDiv).fadeOut(300);
              $(jsId.PrintPressDiv).fadeOut(300);
              $(jsId.ProjectOfficersDiv).fadeOut(300);
              $(jsId.QualityOfficersDiv).fadeOut(300);
              commonObj.changeInnerText(jsId.managementPageHeader, jsData.projects);
          }else {
            projectManagementObj.getProjects()
            .then(data => {

                //console.log(data, "projects");

                if(data.data.getProjects == null) {
                    data.data.getProjects = {};

                    data.data.getProjects.projects = {}
                }

                $('#dataTable-projects').dataTable(
                    {
                        initComplete: function() {
                          window.dataTables.projects = true;


                          $(jsId.ProjectsDiv).fadeIn(300);
                          $(jsId.SchoolsDiv).fadeOut(300);
                          $(jsId.PrintPressDiv).fadeOut(300);
                          $(jsId.ProjectOfficersDiv).fadeOut(300);
                          $(jsId.QualityOfficersDiv).fadeOut(300);
                          commonObj.changeInnerText(jsId.managementPageHeader, jsData.projects);
                            var datatable_length_text = $('.dataTables_length label').contents().filter(function() {
                            if(this.nodeType === 3) {
                               if(this.data == ' entries') {
                                    return this.nodeType === 3;
                               }
                            }
                        }).wrap('<span style="display:none"></style>');

                            var dataTableSelectorProjects = `<div class="col-md-4 d-flex mb-3">
                                                <label class="form-label d-flex align-self-center whitespace-nowrap d-inline pr-3">Table for</label>
                                                <select id="dataTableSelector" class="custom-select form-control" onchange="projectManagementObj.showHideDiv($(this).val())">
                                                    <option selected="" value="#ProjectsDiv">Projects</option>
                                                    <option value="#SchoolsDiv">Schools</option>
                                                    <option value="#PrintPressDiv">Print press</option>
                                                    <option value="#ProjectOfficersDiv">Project Officers</option>
                                                    <option value="#QualityOfficersDiv">Quality Officers</option>

                                                </select>
                                            </div>`;

                            $($('#dataTable-projects_wrapper > div.row > div')[1]).removeClass('col-md-6').addClass('col-md-3  mb-3');
                            $($('#dataTable-projects_wrapper > div.row > div')[0]).removeClass('col-md-6').addClass('col-md-5  mb-3');
                            $($('#dataTable-projects_wrapper > div.row')[0]).prepend(dataTableSelectorProjects);
                        },
                        responsive: true,
                        data : data.data.getProjects.projects,
                        columns: [
                            { data: 'name' },
                            { data: 'id' },
                            { data: 'startDate' },
                            { data: null }
                        ],
                        columnDefs: [
                            {
                                targets: 0,
                                title: 'Project Name',
                                responsive: true,
                                orderable: false,
                                render: function(data, type, full, meta)
                                {
                                    return `${data}`;
                                },
                            },
                            {
                                targets: -1,
                                title: 'Actions',
                                responsive: true,
                                orderable: false,
                                render: function(data, type, full, meta)
                                {

                                    return "\n\t\t\t\t\t\t<div class='d-flex demo'>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-outline-success btn-inline-block mr-1' title='Delete Record'><i class=\"fal fa-eye\"></i></a>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-outline-warning btn-inline-block mr-1' title='Edit'><i class=\"fal fa-edit btn-white-icon\"></i></a>\n\t\t\t\t\t\t\t<div class='dropdown d-inline-block'>\n\t\t\t\t\t\t\t\t<a href='#' class='btn btn-sm btn-outline-danger' data-toggle='dropdown' aria-expanded='true' title='More options'><i class=\"fal fa-times\"></i></a>\n\t\t\t\t\t\t\t\t<div class='dropdown-menu'>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Change Status</a>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Generate Report</a>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>";
                                },
                            }],
                    });


            });

          }

        }

        if(div == jsId.SchoolsDiv) {
          if(window.dataTables.schools) {
            $(jsId.ProjectsDiv).fadeOut(300);
            $(jsId.SchoolsDiv).fadeIn(300);
            $(jsId.PrintPressDiv).fadeOut(300);
            $(jsId.ProjectOfficersDiv).fadeOut(300);
            $(jsId.QualityOfficersDiv).fadeOut(300);
            commonObj.changeInnerText(jsId.managementPageHeader, jsData.schools);
          }else {
            this.getSchools()
                .then(data => {
                    //console.log(data, "schools");

                    var schoolsDataTable = $('#dataTable-schools').DataTable(
                        {
                            initComplete: function() {
                              window.dataTables.schools = true;
                              $(jsId.ProjectsDiv).fadeOut(300);
                              $(jsId.SchoolsDiv).fadeIn(300);
                              $(jsId.PrintPressDiv).fadeOut(300);
                              $(jsId.ProjectOfficersDiv).fadeOut(300);
                              $(jsId.QualityOfficersDiv).fadeOut(300);
                              commonObj.changeInnerText(jsId.managementPageHeader, jsData.schools);

                                var datatable_length_text = $('.dataTables_length label').contents().filter(function() {
                                    if(this.nodeType === 3) {
                                        if(this.data == ' entries') {
                                                return this.nodeType === 3;
                                        }
                                    }
                                }).wrap('<span style="display:none"></style>');
                            },
                            createdRow: function (row, data, dataIndex) {
                                $(row).attr('data-school-code', data.schoolCode);
                            },
                            data: data.data.getSchools.schools,
                            columns: [
                                { data : 'schoolName' },
                                { data : 'population' },
                                { data : 'projectOfficers' },
                                { data : null },
                                { data : null},
                                { data : null}
                            ],
                            responsive: true,
                            columnDefs: [
                                {
                                    targets: 4,
                                    title: 'Completed',
                                    render: function(data, type, full, meta)
                                    {

                                        return "150/300";
                                    },
                                },
                                {
                                    targets: 3,
                                    title: 'Start date',
                                    render: function(data, type, full, meta)
                                    {

                                        return "01-03-2021<br><small class=\"text-muted\">0244115533</small>";
                                    },
                                },
                                {

                                    targets: 2,
                                    orderable: false,
                                    title: 'Assign Officer',
                                    render: function(data, type, full, meta)
                                    {

                                        var officers = '';

                                        $.each(data, function(key, data) {
                                            officers += `
                                                                    <div class="badge-pill-wrapper pb-1">
                                                                        <span class="badge badge-pill project-status job-pill started text-white">${data.fullName}</span>
                                                                    </div>

                                                                `
                                        });

                                        officers+=`<div class="badge-pill-wrapper pb-1">
                                                                        <a class="badge badge-pill btn btn-outline-primary"  data-toggle="modal" data-target="#assignProjectOfficerModal"><span class="icon-small"><i class="fal fa-plus"></i></span>Add</a>
                                                                    </div>`;

                                        return officers;
                                    },
                                },
                                {
                                    targets: 0,
                                    title: 'School Name',
                                    render: function(data, type, full, meta)
                                    {

                                        return data  + "<br><small class=\"text-muted\">0244115533</small>";
                                    },
                                },
                                {
                                    targets: -1,
                                    title: 'Actions',
                                    render: function(data, type, full, meta)
                                    {
                                        return "\n\t\t\t\t\t\t<div class='d-flex demo'>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-outline-warning btn-inline-block mr-1' title='Edit'><i class=\"fal fa-edit btn-white-icon\"></i></a>\n\t\t\t\t\t\t\t<div class='dropdown d-inline-block'>\n\t\t\t\t\t\t\t\t<a href='#' class='btn btn-sm btn-outline-danger' data-toggle='dropdown' aria-expanded='true' title='More options'><i class=\"fal fa-times\"></i></a>\n\t\t\t\t\t\t\t\t<div class='dropdown-menu'>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Change Status</a>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Generate Report</a>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>";
                                    },
                                }],
                        });

                    $('#dataTable-schools tbody').on('click', '.badge-pill-wrapper', function(){


                        var data_row = schoolsDataTable.row($(this).closest('tr')).data();
                        var dataAttr = commonObj.getDataAttr(schoolsDataTable.row($(this).closest('tr')).node(), 'schoolCode');

                        commonObj.setInputVal(projectManagementClassObj.jsId.assignProjectOfficerSchoolCode, dataAttr);

                        $(projectManagementClassObj.jsId.assignProjectOfficerSchoolNameHTML).html(`School name: ${data_row.schoolName}`);


                        projectManagementObj.getProjectOfficers()
                            .then(data => {
                                commonObj.createSelectOptionsFullName(projectManagementClassObj.jsId.assignProjectOfficerNameSelect , data.data.getProjectOfficers.projectOfficers)
                            });
                    });

                }).then(data => {
                var dataTableSelectorSchools = `<div class="col-md-4 d-flex mb-3">
                                                    <label class="form-label d-flex align-self-center whitespace-nowrap d-inline pr-3">Table for</label>
                                                    <select id="dataTableSelector" class="custom-select form-control" onchange="projectManagementObj.showHideDiv($(this).val())">
                                                        <option value="#ProjectsDiv">Projects</option>
                                                        <option selected="" value="#SchoolsDiv">Schools</option>
                                                        <option value="#PrintPressDiv">Print press</option>
                                                        <option value="#ProjectOfficersDiv">Project Officers</option>
                                                        <option value="#QualityOfficersDiv">Quality Officers</option>

                                                    </select>
                                                </div>`;

                $($('#dataTable-schools_wrapper > div.row > div')[1]).removeClass('col-md-6').addClass('col-md-3  mb-3');
                $($('#dataTable-schools_wrapper > div.row > div')[0]).removeClass('col-md-6').addClass('col-md-5  mb-3');
                $($('#dataTable-schools_wrapper > div.row')[0]).prepend(dataTableSelectorSchools);
            });
          }


        }

        if(div == jsId.PrintPressDiv) {


            if(window.dataTables.printPress) {
                $(jsId.ProjectsDiv).fadeOut(300);
                $(jsId.SchoolsDiv).fadeOut(300);
                $(jsId.PrintPressDiv).fadeIn(300);
                $(jsId.ProjectOfficersDiv).fadeOut(300);
                $(jsId.QualityOfficersDiv).fadeOut(300);
                commonObj.changeInnerText(jsId.managementPageHeader, jsData.printPress);
            }else {
              this.getPresses()
              .then(data => {
                  //console.log(data, "print press")
                  if(data.data.getPresses == null) {
                      data.data.getPresses = {};

                      data.data.getPresses.presses = {}
                  }

                  var printPressDataTable = $('#dataTable-print-press').DataTable(
                      {
                          initComplete: function() {
                            window.dataTables.printPress = true;

                            var datatable_length_text = $('.dataTables_length label').contents().filter(function() {
                              if(this.nodeType === 3) {
                                  if(this.data == ' entries') {
                                          return this.nodeType === 3;
                                  }
                              }
                          }).wrap('<span style="display:none"></style>');

                              $(jsId.ProjectsDiv).fadeOut(300);
                              $(jsId.SchoolsDiv).fadeOut(300);
                              $(jsId.PrintPressDiv).fadeIn(300);
                              $(jsId.ProjectOfficersDiv).fadeOut(300);
                              $(jsId.QualityOfficersDiv).fadeOut(300);
                              commonObj.changeInnerText(jsId.managementPageHeader, jsData.printPress);
                          },
                          createdRow: function (row, data, dataIndex) {
                              $(row).attr('data-press-id', data.id);
                          },
                          responsive: true,
                          data: data.data.getPresses.presses,
                          columns: [
                              { data: 'name' },
                              { data: 'officers' },
                              { data: null },
                              { data: null }
                          ],
                          columnDefs: [
                              {
                                  targets: 2,
                                  title: 'Date Added',
                                  orderable: true,
                                  render: function(data, type, full, meta)
                                  {
                                      return `01-03-2021`
                                  },
                              },
                              {
                                  targets: 1,
                                  title: 'Assigned Officer',
                                  orderable: true,
                                  render: function(data, type, full, meta)
                                  {
                                      console.log(data, 'press render')
                                      return `<a data-toggle="modal" data-target="#assignPrintOfficerModal"><button type="button" class="btn btn-outline-primary"><span class="icon-small"><i class="fal fa-plus"></i></span>  Assign Officer</button></a>`;
                                  },
                              },
                              {
                                  targets: 0,
                                  title: 'Press Name',
                                  orderable: true,
                                  render: function(data, type, full, meta)
                                  {
                                      return `${data}<br><small class="text-muted">Kokomlemle, Accra</small>`;
                                  },
                              },
                              {
                                  targets: -1,
                                  title: 'Actions',
                                  orderable: false,
                                  render: function(data, type, full, meta)
                                  {
                                      return "\n\t\t\t\t\t\t<div class='d-flex demo'>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-outline-success btn-icon btn-inline-block mr-1' title='Delete Record'><i class=\"fal fa-eye\"></i></a>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-outline-warning btn-icon btn-inline-block mr-1' title='Edit'><i class=\"fal fa-edit btn-white-icon\"></i></a>\n\t\t\t\t\t\t\t<div class='dropdown d-inline-block'>\n\t\t\t\t\t\t\t\t<a href='#' class='btn btn-sm btn-outline-danger btn-icon' data-toggle='dropdown' aria-expanded='true' title='More options'><i class=\"fal fa-times\"></i></a>\n\t\t\t\t\t\t\t\t<div class='dropdown-menu'>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Change Status</a>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Generate Report</a>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>";
                                  },
                              }],
                      });

                  $('#dataTable-print-press tbody').on('click', 'button', function(){

                      var data_row = printPressDataTable.row($(this).closest('tr')).data();
                      var dataAttr = commonObj.getDataAttr(printPressDataTable.row($(this).closest('tr')).node(), 'pressId');


                      commonObj.setInputVal(projectManagementClassObj.jsId.assignPrintOfficerPressId, dataAttr);


                      $(projectManagementClassObj.jsId.assignPrintOfficerPressNameHTML).html(`Press: ${data_row.name}`);



                      projectManagementObj.getPrintOfficers()
                          .then(data => {
                              commonObj.createSelectOptionsFullName(projectManagementClassObj.jsId.assignPrintOfficerNameSelect, data.data.getPrintOfficers.printOfficers);
                          });

                  });

              }).then(data => {
              var dataTableSelectorPrintPress = `<div class="col-md-4 d-flex mb-3">
                                                  <label class="form-label d-flex align-self-center whitespace-nowrap d-inline pr-3">Table for</label>
                                                  <select id="dataTableSelector" class="custom-select form-control" onchange="projectManagementObj.showHideDiv($(this).val())">
                                                      <option value="#ProjectsDiv">Projects</option>
                                                      <option value="#SchoolsDiv">Schools</option>
                                                      <option selected="" value="#PrintPressDiv">Print press</option>
                                                      <option value="#ProjectOfficersDiv">Project Officers</option>
                                                      <option value="#QualityOfficersDiv">Quality Officers</option>

                                                  </select>
                                              </div>`;





              $($('#dataTable-print-press_wrapper > div.row > div')[1]).removeClass('col-md-6').addClass('col-md-3  mb-3');
              $($('#dataTable-print-press_wrapper > div.row > div')[0]).removeClass('col-md-6').addClass('col-md-5  mb-3');
              $($('#dataTable-print-press_wrapper > div.row')[0]).prepend(dataTableSelectorPrintPress);
          });
            }

        }

        if(div == jsId.ProjectOfficersDiv) {


            this.getProjectOfficers()
                .then(data => {
                    console.log(data, "project officers");

                    if(data.data.getProjectOfficers == null) {
                        data.data.getProjectOfficers = {};

                        data.data.getProjectOfficers.projectOfficers = {};
                    }


                    if(window.dataTables.projectOfficers) {
                        $(jsId.ProjectsDiv).fadeOut(300);
                        $(jsId.SchoolsDiv).fadeOut(300);
                        $(jsId.PrintPressDiv).fadeOut(300);
                        $(jsId.ProjectOfficersDiv).fadeIn(300);
                        $(jsId.QualityOfficersDiv).fadeOut(300);
                        commonObj.changeInnerText(jsId.managementPageHeader, jsData.projectOfficers);
                    }
                    else {
                      var projectOfficersDataTable = $('#dataTable-project-officers').DataTable(
                        {
                            initComplete: function() {
                              window.dataTables.projectOfficers = true;

                              var datatable_length_text = $('.dataTables_length label').contents().filter(function() {
                                if(this.nodeType === 3) {
                                    if(this.data == ' entries') {
                                            return this.nodeType === 3;
                                    }
                                }
                            }).wrap('<span style="display:none"></style>');

                                $(jsId.ProjectsDiv).fadeOut(300);
                                $(jsId.SchoolsDiv).fadeOut(300);
                                $(jsId.PrintPressDiv).fadeOut(300);
                                $(jsId.ProjectOfficersDiv).fadeIn(300);
                                $(jsId.QualityOfficersDiv).fadeOut(300);
                                commonObj.changeInnerText(jsId.managementPageHeader, jsData.projectOfficers);

                                var dataTableSelectorProjectOfficers = `<div class="col-md-4 d-flex mb-3">
                                                    <label class="form-label d-flex align-self-center whitespace-nowrap d-inline pr-3">Table for</label>
                                                    <select id="dataTableSelector" class="custom-select form-control" onchange="projectManagementObj.showHideDiv($(this).val())">
                                                        <option value="#ProjectsDiv">Projects</option>
                                                        <option value="#SchoolsDiv">Schools</option>
                                                        <option value="#PrintPressDiv">Print press</option>
                                                        <option value="#ProjectOfficersDiv">Project Officers</option>
                                                        <option value="#QualityOfficersDiv">Quality Officers</option>

                                                    </select>
                                                </div>`;



                                $($('#dataTable-project-officers_wrapper > div.row > div')[1]).removeClass('col-md-6').addClass('col-md-3  mb-3');
                                $($('#dataTable-project-officers_wrapper > div.row > div')[0]).removeClass('col-md-6').addClass('col-md-5  mb-3');
                                $($('#dataTable-project-officers_wrapper > div.row')[0]).prepend(dataTableSelectorProjectOfficers);
                            },
                            createdRow: function (row, data, dataIndex) {
                                $(row).attr('data-project-officer-id', data.id);
                            },
                            data: data.data.getProjectOfficers.projectOfficers,
                            columns: [
                                { data: 'fullName' },
                                { data: 'projectId' },
                                { data: null },
                                { data: null },
                                { data: null },
                                { data: null },
                                { data: null },
                                { data: null },
                                { data: 'photoId' },
                                { data: 'photoIdNumber' },
                                { data: null }
                            ],
                            responsive: true,
                            columnDefs: [
                                {
                                    targets: 9,
                                    title: 'ID No.',
                                    orderable: false,
                                    render: function(data, type, full, meta)
                                    {
                                        return `${data}`
                                    },
                                },
                                {
                                    targets: 8,
                                    title: 'ID Type',
                                    orderable: false,
                                    render: function(data, type, full, meta)
                                    {
                                        return `${data}`
                                    },
                                },
                                {
                                    targets: 7,
                                    title: 'Date Added',
                                    orderable: true,
                                    render: function(data, type, full, meta)
                                    {
                                        return `01-03-2021`
                                    },
                                },
                                {
                                    targets: 6,
                                    title: 'Task Duration',
                                    orderable: true,
                                    render: function(data, type, full, meta)
                                    {
                                        return `12 months`
                                    },
                                },
                                {
                                    targets: 5,
                                    title: 'Total Submits',
                                    orderable: true,
                                    render: function(data, type, full, meta)
                                    {
                                        return `008`
                                    },
                                },
                                {
                                    targets: 4,
                                    title: 'Inst. Engaged',
                                    orderable: true,
                                    render: function(data, type, full, meta)
                                    {
                                        return `008`
                                    },
                                },
                                {
                                    targets: 3,
                                    title: 'Start date',
                                    orderable: true,
                                    render: function(data, type, full, meta)
                                    {
                                        return `${full.project.startDate}<br><small class="text-muted">End date ${full.project.endDate} </small>`
                                    },
                                },
                                {
                                    targets: 2,
                                    orderable : false,
                                    render: function(data, type, full, meta) {
                                        var assignSchools = '';

                                        if(!data) {
                                            $.each(full, function(key, data) {
                                                assignSchools += `
                                                                            <div class="badge-pill-wrapper pb-1">
                                                                                <span class="badge badge-pill project-status job-pill started text-white">${data}</span>
                                                                            </div>

                                                                        `
                                            });
                                        }


                                        assignSchools+=`<div class="badge-pill-wrapper pb-1">
                                                                                <a class="badge badge-pill btn btn-outline-primary"  data-toggle="modal" data-target="#addTaskToProjectOfficerNameModal"><span class="icon-small"><i class="fal fa-plus"></i></span>Add</a>
                                                                            </div>`;

                                        return assignSchools;
                                    }
                                },
                                {
                                    targets: 1,
                                    title: 'Assign Project ID',
                                    orderable: true,
                                    render: function(data, type, full, meta)
                                    {
                                        return `${data}<br><small class="text-muted">${full.project.name}</small>`
                                    },
                                },
                                {
                                    targets: 0,
                                    title: 'Project Officer name',
                                    orderable: false,
                                    render: function(data, type, full, meta)
                                    {
                                        return `${data}<br><small class="text-muted">${full.phone}</small>`
                                    },
                                },
                                {
                                    targets: -1,
                                    title: 'Actions',
                                    orderable: false,
                                    render: function(data, type, full, meta)
                                    {
                                        return "\n\t\t\t\t\t\t<div class='d-flex demo'>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-outline-success btn-inline-block mr-1' title='Delete Record'><i class=\"fal fa-eye\"></i></a>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-outline-warning btn-inline-block mr-1' title='Edit'><i class=\"fal fa-edit btn-white-icon\"></i></a>\n\t\t\t\t\t\t\t<div class='dropdown d-inline-block'>\n\t\t\t\t\t\t\t\t<a href='#' class='btn btn-sm btn-outline-danger' data-toggle='dropdown' aria-expanded='true' title='More options'><i class=\"fal fa-times\"></i></a>\n\t\t\t\t\t\t\t\t<div class='dropdown-menu'>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Change Status</a>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Generate Report</a>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>";
                                    },
                                }
                            ]
                        }
                    );
                    }

                    $('#dataTable-project-officers tbody').on('click', '.badge-pill-wrapper', function(){


                        var data_row = projectOfficersDataTable.row($(this).closest('tr')).data();
                        var dataAttr = commonObj.getDataAttr(projectOfficersDataTable.row($(this).closest('tr')).node(), 'projectOfficerId');


                        commonObj.setInputVal(projectManagementClassObj.jsId.assignProjectOfficerId, dataAttr);



                        projectManagementObj.getSchools()
                            .then(data => {
                                commonObj.createSelectOptionsSchool(projectManagementClassObj.jsId.assignProjectOfficerInstSelect, data.data.getSchools.schools);
                            });
                    });


                });

        }

        if(div == jsId.QualityOfficersDiv) {

          this.getProjectOfficers()
          .then(data => {
              console.log(data, "project officers");

              if(data.data.getProjectOfficers == null) {
                  data.data.getProjectOfficers = {};

                  data.data.getProjectOfficers.projectOfficers = {};
              }


              if(window.dataTables.qualityOfficers) {

                  $(jsId.ProjectsDiv).fadeOut(300);
                  $(jsId.SchoolsDiv).fadeOut(300);
                  $(jsId.PrintPressDiv).fadeOut(300);
                  $(jsId.ProjectOfficersDiv).fadeOut(300);
                  $(jsId.QualityOfficersDiv).fadeIn(300);
                  commonObj.changeInnerText(jsId.managementPageHeader, jsData.qualityOfficers);

              }else {
                var projectOfficersDataTable = $('#dataTable-quality-officers').DataTable(
                  {
                      initComplete: function() {

                          window.dataTables.qualityOfficers = true;
                          $(jsId.ProjectsDiv).fadeOut(300);
                          $(jsId.SchoolsDiv).fadeOut(300);
                          $(jsId.PrintPressDiv).fadeOut(300);
                          $(jsId.ProjectOfficersDiv).fadeOut(300);
                          $(jsId.QualityOfficersDiv).fadeIn(300);
                          commonObj.changeInnerText(jsId.managementPageHeader, jsData.qualityOfficers);

                          var datatable_length_text = $('.dataTables_length label').contents().filter(function() {
                            if(this.nodeType === 3) {
                                if(this.data == ' entries') {
                                        return this.nodeType === 3;
                                }
                            }
                        }).wrap('<span style="display:none"></style>');

                        var dataTableSelectorProjectOfficers = `<div class="col-md-4 d-flex mb-3">
                                              <label class="form-label d-flex align-self-center whitespace-nowrap d-inline pr-3">Table for</label>
                                              <select id="dataTableSelector" class="custom-select form-control" onchange="projectManagementObj.showHideDiv($(this).val())">
                                                  <option value="#ProjectsDiv">Projects</option>
                                                  <option value="#SchoolsDiv">Schools</option>
                                                  <option value="#PrintPressDiv">Print press</option>
                                                  <option value="#ProjectOfficersDiv">Project Officers</option>
                                                  <option value="#QualityOfficersDiv">Quality Officers</option>

                                              </select>
                                          </div>`;



              $($('#dataTable-quality-officers_wrapper > div.row > div')[1]).removeClass('col-md-6').addClass('col-md-3  mb-3');
              $($('#dataTable-quality-officers_wrapper > div.row > div')[0]).removeClass('col-md-6').addClass('col-md-5  mb-3');
              $($('#dataTable-quality-officers_wrapper > div.row')[0]).prepend(dataTableSelectorProjectOfficers);
                      },
                      createdRow: function (row, data, dataIndex) {
                          $(row).attr('data-project-officer-id', data.id);
                      },
                      data: data.data.getProjectOfficers.projectOfficers,
                      columns: [
                          { data: 'fullName' },
                          { data: 'projectId' },
                          { data: null },
                          { data: null },
                          { data: null },
                          { data: null },
                          { data: null },
                          { data: null },
                          { data: 'photoId' },
                          { data: 'photoIdNumber' },
                          { data: null }
                      ],
                      responsive: true,
                      columnDefs: [
                          {
                              targets: 9,
                              title: 'ID No.',
                              orderable: false,
                              render: function(data, type, full, meta)
                              {
                                  return `${data}`
                              },
                          },
                          {
                              targets: 8,
                              title: 'ID Type',
                              orderable: false,
                              render: function(data, type, full, meta)
                              {
                                  return `${data}`
                              },
                          },
                          {
                              targets: 7,
                              title: 'Date Added',
                              orderable: false,
                              render: function(data, type, full, meta)
                              {
                                  return `01-03-2021`
                              },
                          },
                          {
                              targets: 6,
                              title: 'Task Duration',
                              orderable: false,
                              render: function(data, type, full, meta)
                              {
                                  return `12 months`
                              },
                          },
                          {
                              targets: 5,
                              title: 'Total Submits',
                              orderable: false,
                              render: function(data, type, full, meta)
                              {
                                  return `008`
                              },
                          },
                          {
                              targets: 4,
                              title: 'Inst. Engaged',
                              orderable: false,
                              render: function(data, type, full, meta)
                              {
                                  return `008`
                              },
                          },
                          {
                              targets: 3,
                              title: 'Start date',
                              orderable: false,
                              render: function(data, type, full, meta)
                              {
                                  return `${full.project.startDate}<br><small class="text-muted">End date ${full.project.endDate} </small>`
                              },
                          },
                          {
                              targets: 2,
                              render: function(data, type, full, meta) {
                                  var assignSchools = '';

                                  if(!data) {
                                      $.each(full, function(key, data) {
                                          assignSchools += `
                                                                      <div class="badge-pill-wrapper pb-1">
                                                                          <span class="badge badge-pill project-status job-pill started text-white">${data}</span>
                                                                      </div>

                                                                  `
                                      });
                                  }


                                  assignSchools+=`<div class="badge-pill-wrapper pb-1">
                                                                          <a class="badge badge-pill btn btn-outline-primary"  data-toggle="modal" data-target="#addTaskToProjectOfficerNameModal"><span class="icon-small"><i class="fal fa-plus"></i></span>Add</a>
                                                                      </div>`;

                                  return assignSchools;
                              }
                          },
                          {
                              targets: 1,
                              title: 'Assign Project ID',
                              orderable: false,
                              render: function(data, type, full, meta)
                              {
                                  return `${data}<br><small class="text-muted">${full.project.name}</small>`
                              },
                          },
                          {
                              targets: 0,
                              title: 'Project Officer name',
                              orderable: false,
                              render: function(data, type, full, meta)
                              {
                                  return `${data}<br><small class="text-muted">${full.phone}</small>`
                              },
                          },
                          {
                              targets: -1,
                              title: 'Actions',
                              orderable: false,
                              render: function(data, type, full, meta)
                              {
                                  return "\n\t\t\t\t\t\t<div class='d-flex demo'>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-outline-success btn-inline-block mr-1' title='Delete Record'><i class=\"fal fa-eye\"></i></a>\n\t\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-outline-warning btn-inline-block mr-1' title='Edit'><i class=\"fal fa-edit btn-white-icon\"></i></a>\n\t\t\t\t\t\t\t<div class='dropdown d-inline-block'>\n\t\t\t\t\t\t\t\t<a href='#' class='btn btn-sm btn-outline-danger' data-toggle='dropdown' aria-expanded='true' title='More options'><i class=\"fal fa-times\"></i></a>\n\t\t\t\t\t\t\t\t<div class='dropdown-menu'>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Change Status</a>\n\t\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Generate Report</a>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>";
                              },
                          }
                      ]
                  }
              );
              }


              $('#dataTable-project-officers tbody').on('click', '.badge-pill-wrapper', function(){


                  var data_row = projectOfficersDataTable.row($(this).closest('tr')).data();
                  var dataAttr = commonObj.getDataAttr(projectOfficersDataTable.row($(this).closest('tr')).node(), 'projectOfficerId');


                  commonObj.setInputVal(projectManagementClassObj.jsId.assignProjectOfficerId, dataAttr);



                  projectManagementObj.getSchools()
                      .then(data => {
                          commonObj.createSelectOptionsSchool(projectManagementClassObj.jsId.assignProjectOfficerInstSelect, data.data.getSchools.schools);
                      });
              });


          });


        }


    }
}

var projectManagementObj = new projectManagementClass(projectManagementClassObj)
