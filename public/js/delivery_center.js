class deliveryCenter {
    updateDeliveryStatus({updateFrom, updateTo, event}) {
        
        if(updateFrom == 'pending' && updateTo == 'in-transit') {
            $(event.target).closest('div').siblings('button:first').removeClass('btn-primary').addClass('btn-warning').html('In-transit');

            $(event.target).closest('div').siblings('button:last').removeClass('btn-primary').addClass('btn-warning');
        }

        if(updateFrom == 'in-transit' && updateTo == 'pending') {
            $(event.target).closest('div').siblings('button:first').removeClass(['btn-warning', 'btn-success']).addClass(['btn-primary']).html('Pending');
            $(event.target).closest('div').siblings('button:last').removeClass(['btn-warning', 'btn-success']).addClass('btn-primary');
        }

        if(updateFrom == 'pending' && updateTo == 'delivered') {
            $(event.target).closest('div').siblings('button:first').removeClass(['btn-primary', 'btn-warning']).addClass('btn-success').html('Delivered');
            $(event.target).closest('div').siblings('button:last').removeClass(['btn-primary', 'btn-warning']).addClass('btn-success');
        }

        if(updateFrom == 'delivered' && updateTo == 'pending') {
            $(event.target).closest('div').siblings('button:first').removeClass(['btn-success', 'btn-warning']).addClass('btn-primary').html('Pending');
            $(event.target).closest('div').siblings('button:last').removeClass(['btn-success', 'btn-warning']).addClass('btn-primary');
        }

        if(updateFrom == 'in-transit' && updateTo == 'delivered') {
            $(event.target).closest('div').siblings('button:first').removeClass('btn-warning').addClass('btn-success').html('Delivered');
            $(event.target).closest('div').siblings('button:last').removeClass('btn-warning').addClass('btn-success');
        }

        if(updateFrom == 'delivered' && updateTo == 'in-transit') {
            $(event.target).closest('div').siblings('button:first').removeClass(['btn-success', 'btn-primary']).addClass('btn-warning').html('In-transit');
            $(event.target).closest('div').siblings('button:last').removeClass(['btn-success', 'btn-primary']).addClass('btn-warning');
        }
    }
}

var deliveryCenterObj = new deliveryCenter();